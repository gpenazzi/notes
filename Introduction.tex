\chapter{Introduction}



These note will be written during the course \emph{Electronic Transport at Nanoscale} at the department of Physics of Universit\"{a}t Bremen, Academic Year 2013/2014. The aim of these notes is to introduce the student to general concepts and techniques routinely used in the study of organic and inorganic confined system, ranging from electronic devices to experimental setups. During the course some numerical implementation will be shown. Hopefully at the end of the course the student will have acquired the proper language, a decent set of theoretical tools and a feeling of the challenges in nano-device modelling. 



\section{Submicrometer systems}

Generally speaking, a nanostructure is any system or device with one or more characteristic length in the nanometer length scale. In order to understand why the nanometer length scale is particularly important, I will introduce some charge transport  characteristic length scales. It is important to keep in mind that despite the word 'Nanotechnology' is having quite an impact the modern society, technological control of electronic transport at nanometer scale is possible since almost 40 years. Some of the most important technological milestones are the experimental realization of the first Resonant Tunneling Diode by Sollner in 1983 \cite{Sollner1983} and the invention of Scanning Tunneling Microscope by Binning and Rohrer in 1982 \cite{Binnig1982}. In the same decade, the advances in Molecular Beam Epitaxy allowed for high mobility III-V alloys (GaAs, AlGaAs) and layered quantum well systems, giving the possiblity to explore the wide field of 2D Electron Gas (2DEG) physics. This technology was used by van Wees and colaborators in 1988 \cite{vanWees1988} to observe for the first time quantized conductance in a quantum point contact. As we will see during the course, this discovery connect directly the macroscopic conductance with transmission probability of wave functions, leading to an insight in the quantum foundation of electrical transport. All this field of research was triggered not only by curiosity, but also by the impressive economic interests surrounding Silicon CMOS technology. 


Up to the end of last century, semiconductor nanostructures were the king of the field, and many textbooks still refer to those and start from 2DEG in semiconductor heterostructures. Today, the situation is quite different and there is a whole plethora of novel nanoscale electronic systems, being carbon allotropes the most notable example.   



\section{Characteristic length scales}

Why do we need a different course for electron transport in \emph{Nanoscale} systems? The most intuitive reason, is that the nanometer scale is the domain where quantum mechanical phenomena are relevant. Sometimes the expression "Quantum transport" is used for the same reason, however you should be aware that different communities refer to something very different when they speak about "quantum transport". Some refer to topics close to the problem of quantum computing, some to the Quantum mechanics foundation of charge transport theory, referring to the topics faced for example in the courses of  Quantum condensed matter theory. The second big change in nanoscale system is that the concept of bulk material itself can become meaningless: the bulk description per se is possible when the size of the system is much larger than relevant characteristic lengths. 

We will therefore dedicate this introductory chapter to the introduction of such length scale; their relevance and some derivation will follow in the next chapter. Afterwards, we recall some basics quantum mechanic theory of single particle confinement and we will introduce a first "trick" to tune a system dimensionality through magnetic control. An introduction to characteristic length scales can also be found in the first chapter of ref. \cite{Ferry2009, Datta1997}. 

The most important lengthscales are given the following:

\begin{description}

\item[De Broglie Wavelength] $\lambda$: charge carriers are often described as free or quasi free particles with associated momentum $\pvec=\hbar \kvec$. This picture is still meaningful, i.e. the wavevector is still a "good" quantum number when the carriers can freely travel along a distance $L$ such that the De Broglie wavelength $\lambda=2\pi / k$ is much smaller than $L$, or equivalently $kL \gg 1$.
When the sample size is in the order of De Broglie wavelength, confinement may occur and the particle will get localized in space. 

\item[Mean free path $l_{m}$]: this is the average distance a particle can travel before it experiences an elastic scattering event which change its initial momentum. The mean free path is related to the momentum relaxation time $\tau_{m}$ simply by $l_{m}=v\tau_{m}$ where $v$ is the carrier velocity (the group velocity of the wavepacket in the quantum mechanical picture). A related quantity, the transport momentum relaxation length, is defined as the average distance a particle can travel before its initial momentum is randomized. The two definition are equivalent if the scattering mechanism is isotropic. We may define an \emph{anisotropy coefficient} $g$ and define the transport momentum relaxation length as 

\begin{equation}
l_{tr}=\frac{l_{m}}{1-g}
\end{equation}

whre $g=0$ represent an isotropic scattering and $g=1$ a completely anisotropic scattering (meaning that it will never provide full momentum randomization). The relation between the two quantities will be explained later in the text, however it is important to keep in mind their difference as sometimes in literature the two terms are exchanged, within the implicit assumption of isotropic scattering. 

On distances much longer than the transport relaxation length particles will move according to a diffusive (or Brownian) motion, with $l^2=Dt$ where $l$ is the travelled distance, $t$ the time and $D$ the diffusion coefficient. For shorter distances the particles will travel according to the ballistic expression $l=vt$ with $v$ group velocity.


\item[Inelastic mean free path]: the inelastic mean free path $l_{in}$, or energy relaxation length, is the average distance a particle will travel before it experiences an inelastic scattering event which will change its energy. Clearly, not necessarily a sequence of inelastic scattering events will provide energy relaxation, and the idea of energy relaxation itself assumes that the particle may relax to an equilibrium distribution, which is not always true. Therefore there is not an exact definition of the inelastic mean free path, and different author may apply different definitions. 

In general, a particle may travel in diffusive or ballistic regime between inelastic scattering events. Therefore $l_{in}$ is well defined in two limiting cases:

        \begin{align}
        l_{in} & = v \tau_{\phi} & l_{in} \leq l_{m} \label{eq1:phase1} \\
      l_{in}^{2}& = \frac{1}{3}v^{2}\tau_{in}\tau_{m}=D\tau_{in} & l_{in} \gg l_{m} \label{eq1:phase2}
        \end{align}

The idea of energy relaxation itself is possible if we assume a distinction between two subsystem: a single particle and an energy bath. It is well known from statistical physics that this distinction leads to irreversible dynamics. 

\item[Phase relaxation length $l_{\phi}$]: the phase relaxation length is defined as the average distance a particle can travel before the correlation between its initial phase and its current phase is lost. This quantity is very closely related to the  inelastic mean free path, but with the striking difference of being a purely not classical (in the sense of quantum-mechanics) quantity. 

 We can explain this quantity by providing an example. Imagine to have a single particle travelling in an environment given by other particles which may interact with it. We assume that at an initial time we can described the particle and the environment by a product state $\Ket{\varphi}=\Ket{\varphi_{p}}\Ket{\varphi_{env}}$, solution of the Schr\"{o}dinger equation for an Hamiltonian $\opham=\opham _{p}+\opham _{env}$ such that $\opham_{p} \Ket{\varphi_{env}}=0 $ and $\opham_{env} \Ket{\varphi_{p}}=0 $. This means that the environment and the particle are non-interacting systems, and the wavefunction $\Ket{\varphi}$ will evolve in time as a product state, i.e. (assuming a time independent hamiltonian). 
 
 \begin{equation}
 \Ket{\varphi, t} = e^{-j\opham t} \Ket{\varphi, t_{0}}=e^{-j\opham_{p} t} \Ket{\varphi_{p}, t_{0}}e^{-j\opham_{env} t} \Ket{\varphi_{env}, t_{0}}=\Ket{\varphi_{p},t}\Ket{\varphi_{env},t}
\end{equation}

The phase of the particle is evolving independently, according to its single particle Hamiltonian. 

If the particle and the environment are interacting, the total Hamiltonian becomes $H=H_{p}+H_{env}+H_{p-env}$ and the equation above is not valid anymore, i.e. the state vector will not evolve as a product vector and the particle wavefunction will gather a phase contribution given by the environment (and viceversa). This disturbance in the single particle phase is what can be identified with \emph{phase distruction}, and the phase relaxation length measures the average distance a particle is allowed to travel before its phase is destroyed.  Phase breaking mechanism are often given by inelastic process, therefore the two quantities are often used as synonym or appear to be numerically identical.

\end{description}

When the system size is much larger than these length scales, classical theory of electron transport works well, Ohm's law holds and the total resistance of the sample is given by the expression $R=\frac{L}{A\sigma}$ where $\sigma$, the conductance, can be measured or theoretically predicted by semi-classical models. On the other hand, if one of the characteristic length (by characteristic length we mean lateral size or typical size of geometric features such as the width of a layer in an heterostructure) is shorter than one or more characteristic lengths, novel electron properties and transport mechanisms may occur. A table of characteristic lengths for Silicon and Gallium Arsenide at low temperature can be found in ref. \cite{Ferry2009}. De Broglie wavelength is usually in the order of 1-100 nm. The mean free path may depend or not on temperature, but typical  values could range from some nanometers to several microns. Phase relaxation length depends on temperature and can also span values similar to the mean free path. Therefore it is clear that the nanometer scale is critical because this is the scale where different transport regimes may exist and where quantum-mechanical effects may become crucial.




\section{Confined 2D, 1D and 0D systems}

In the following paragraphs we will recall the basic behaviour of a non-relativistic single particle constrained by a confinement potential. 

From solid state physics course, we learned that an electron moving in conduction band (or a hole in valence band) close to the band edge can be described by an \emph{effective mass equation} which is formally equivalent to the time independent Schr\"{o}dinger equation of a free electron:

\begin{equation}
\left[ E_{c} + \frac{\opp^{2}}{2m*} + U(\rvec)\right]\Psi(\rvec)=\varepsilon\Psi(\rvec) \label{eq1:ema1}
\end{equation}

where $E_{c}$ is the conduction band edge, $m*$ the effective mass and $U(\rvec)$ an external potential. Note that this equation holds for nearly parabolic dispersion (massive fermions), therefore it works quite good in semiconductors and in some metals \footnote{It does not work in semi-metals and, in general, in systems with a linear dispersion (massless fermions) such as graphene or topological insulators. This systems are rarely cited in the dedicated books for a very simple reasons: they have been discovered only recently (direct evidence of massless electrons in graphene has been published in 2004). Nevertheless they are systems of outstanding importance; while we will focus on massive fermions for didactic reasons, some of the techniques shown during the course apply to massless fermions as well.}. Let's now assume that $U(\rvec)$ is a confining potential, i.e. it has the purpose to localize the electron in a specific region in space. The easiest case known from Quantum Mechanics course is the infinite well:

\begin{equation}
U(z) =
  \begin{cases}
    0 &  \text{if $0<z<L_{z}$}\\
    \infty &  \text{otherwise}
  \end{cases} 
\end{equation}

Assuming no magnetic field and $E_{c}=0$ for convenience, the solution to eq. ~\ref{eq1:ema1} is found simply by decoupling the wavefunction $\Psi(\rvec) = \chi(\rvec_{\parallel})\varphi(z)$ such that $\chi(\rvec_{\parallel})$ is the free electron solution with $\opp = j\hbar\nabla_{\rvec_{\parallel}}$ in the $x,y$ direction and $\varphi(z)$ satisfies:

\begin{equation}
\left[ \frac{\hbar^{2}}{2m*}\frac{\partial^{2}}{\partial z^{2}} + U(z)\right]\varphi(z)=E\varphi(z) \label{eq1:ema2}
\end{equation}

The solutions to the above equation are well known from basic quantum wave mechanics and given by:

\begin{gather}
\varphi_{n}(z)=\sqrt{\frac{2}{L_{z}}}sin\left(nk_{n} \right) \quad \text{with $k_{n}=\frac{n\pi} {L_{z}}$, n = 1,2...} \\
\varepsilon_{n}=\frac{\hbar^{2}k_{n}^{2}}{2m*}
\end{gather}

The total dispersion relation is given by the free electron energy in the $x,y$ direction plus the discrete levels of the particle in the box:

\begin{equation}
  \varepsilon_{2D} = \frac{\hbar^{2}k_{\parallel}^{2}}{2m*}+\frac{\hbar^{2}k_{n}^{2}}{2m*} \label{eq1:disp1}
\end{equation}

with $k_{\parallel} = |k_{x}\xvec + k_{y}\yvec |$. The dispersion relation above describes a set of $n$ sub-bands in the reciprocal two dimensional (x,y) space with a relative offset $\varepsilon_{n}$. If the confinement is strong enough, a single band $n=1$ is allowed (in the realistic case with finite barrier the higher energy band offsets will be higher than the barrier itself or than the material work function). We define such a system a 2DEG (2-dimensional electron gas). Similarly, if we have a confinement potential $U(y,z)$ along two directions the corresponding dispersion relation will describe a free electron in $x$ direction. The solutions to the Schr\"{o}dinger equations will be:

\begin{gather}
\Psi(\rvec)=\chi(x)\varphi_{n}(z)\varphi_{m}(y) \\
\varepsilon_{1D}=\varepsilon_{n,m}+\frac{\hbar^{2}k_{\parallel}^{2}}{2m*} \\
\varepsilon_{n,m}=\frac{\hbar^{2}k^{2}_{n,m}}{2m*} \quad \text{with $\kvec _{n,m}=k_{n}\xver+k_{m}\yver$ and  $k_{m}=\frac{m\pi}{L_{y}}$}
\end{gather}

In this case we have a set of parabolic sub-bands with an offset given by the confinement energies $E_{n,m}$. In analogy to the solution of wave equation for optical wave guides, this sub-bands are often called \emph{electronic modes} (or just modes). Similarly, a confinement in all the directions (particle in 3D box) will correspond to a purely discrete energy spectra $E_{m,n,l}$ with no dispersion; such structures are called \emph{quantum dots}.

Before considering the physical implications of confinement, we should have clear that the previous equations are all valid for a confined free electron gas (or Fermi gas), i.e. a gas of \emph{non-interacting} electrons. The student familiar with many body physics will know that this is a very rough approximation and that, in line of principle, the full wave function for the $N$ particles in the system should be considered. Luckily, a system of interacting particle can be be, in line of principle, described by an equivalent system of non-interacting Kohn-Sham particles by means of Density Functional Theory or additional interactions terms can be included fairly easily in a free electron gas approximation by solving the system of coupled single particle equations:

\begin{gather}
\left[ -\frac{\hbar^{2}}{2m*}\nabla^{2}_{\rvec} + U_{ext}(\rvec)+ v_{eff}(\rvec) \right]\psi_{i}(\rvec) = \varepsilon_{i}\psi_{i}(\rvec) \label{eq1:many1}\\
v_{eff}(\rvec) = v_{Hartree} + v_{xc} \label{eq1:many2} \\
v_{Hartree} = \sum_{i\neq j}\int d\rvec ' \frac{| \psi_{j}^{2}(\rvec ') |^{2}}{|\rvec - \rvec '|} \label{eq1:many3}
\end{gather}

where $v_{eff}$ is an effective mean field local potential which contains electrostatic interaction via Hartree approximation (eq. ~\ref{eq1:many3}) and exchange-correlation effects. We will ignore at first the complication given by the exchange-correlation term; even in this limit it is clear that local confinement may induce local charging effects affecting the Hartree potential. In fact, eq.~\ref{eq1:many1} and eq.~\ref{eq1:many2} needs usually to be solved numerically in a self-consistent scheme coupling a Poisson solver and an Eigenvalue solver. Moreover, we considered an isotropic effective mass $m*$ constant on the whole sample. 

\begin{figure}[h]
\centering
 \includegraphics[width=0.6\textwidth,keepaspectratio]{graph/hetero}
 \caption{An AlAs/GaAs quantum well and corresponding conduction band energy profile.}
 \label{fig1:hetero}
 \end{figure}
 
 If we consider a more realistic case as depicted in Fig.~\ref{fig1:hetero}, we should take into account at least two additional facts. First, the barrier is not well approximated by an infinite barrier as the barrier height is close to level spacing. Second, the effective mass in GaAs and AlAs differs. In such cases, a more realistic approximation is given by the envelope equation shown by Ferry in ref.~\cite{Ferry2009}, pag.35:
 
 \begin{equation}
 \left[\frac{\hbar^2}{2} \frac{\partial}{\partial z}\frac{1}{m^{*}(z)}\frac{\partial}{\partial z} + \frac{\hbar^{2}}{2m^{*}_{\parallel}}\nabla_{\rvec_{\parallel}}^{2} + U(z) + v_{eff}(z) \right]\Psi(\rvec_{\parallel}, z)=\varepsilon \Psi(\rvec_{\parallel}, z)
 \end{equation}
 
 where $\Psi(\rvec_{\parallel})$ is the envelope of Bloch wave functions and $U(z)$ a finite height barrier. A rigorous derivation of this equation and a review of Envelope Function Approximation theory can be found in a review by Burt ~\cite{Burt1999}. In the following we will still consider solutions for a free electron, as a more accurate description, needed for quantitative predictions, does not change substantially the qualitative picture in the cases of interest. The main take home message is that confinement will reduce the degrees of freedom of the electron gas and generate a discrete number of sub-bands (modes).
 
 \begin{ExerciseList}
\Exercise[label=ex1:harmonic]
When the confinement potential is weak, the infinite barrier model is not physical any more. This is for example the case of electrostatic confinement by top-gate or chemical etching, where the resulting interface between two materials is not abrupt. In this cases we can approximate $U(z)$ with an harmonic potential $U(z)=\frac{1}{2}m^{*}\omega_{0}^{2}z^{2}$. What is the solution of the Schr\"{o}dinger equation? How is the spacing between sub-bands? (hint: the equation should already look familiar) 
\Exercise
The effective mass in conduction band in Gallium Arsenide is about $0.07 m_{e}$. Consider the well in fig. ~\ref{fig1:hetero} in the approximation of infinite barrier and find the energy position of the first 4 sub-bands. Do the same using the longitudinal effective mass of Silicon in conduction band ($\sim 1.0 m_{e}$) 
 \end{ExerciseList} 
 
 \subsection{Density of states}
 
The density of states (DOS) is defined as the number of states per unit length and energy interval $\varepsilon, \varepsilon + d\varepsilon$:

\begin{equation}
g(\varepsilon) = \sum_{i}\delta(\varepsilon-\varepsilon_{i}) \label{eq1:dos1}
\end{equation}

where $i$ is a good quantum number characterizing the state. In our descriptions, it will include crystal momentum, sub-band index, spin index, valley degeneracy and so on. The calculation of $g(\varepsilon)$ in a free electron gas is explained in any solid state theory textbook (for example C. Kittel Introduction to Solid State Physics, chapter 6). We will extend the 3D results to 2D and 1D systems. In 0D systems, the states obey a discrete distribution, therefore the density of states is simply given directly by eq. ~\ref{eq1:dos1}, i.e. $g_{0}(\varepsilon)$ is a set of delta functions peaked on the corresponding eigenvalues $\varepsilon_{i}$.

In the other cases, we will follow a procedure similar to the bulk isotropic case. In a 3D electron gas, the constant energy surface corresponds to a sphere in the reciprocal space. The volume occupied by the spherical shell with internal diameter $k$ and external diameter $k+dk$ is given by $V_{dk}=4\pi k^{2}dk + \order (dk^{2})$. In a sample of size $L$ the k points are equally spaced on a grid given by $k_{x,y,z}=\frac{2\pi n_{x,y,z}}{L}$, therefore the volume occupied by a single state in the reciprocal space is $V_{state}=\frac{L^{3}}{(2\pi)^{3}}$. Note that in general we have $V_{state}=\frac{L^{d}}{(2\pi)^{d}}$ where $d$ is the dimensionality of the system. The density of states in k-space per unit volume is then given by:

\begin{equation}
g_{3}(k)=\frac{dn}{dk}=\frac{V_{dk}}{dk V_{state}}=\frac{k^{2}}{2\pi^{2}}
\end{equation} 

by similar consideration, we have that low dimensionality systems are described by:

\begin{gather}
g_{2}(k)=\frac{2\pi k}{4\pi^{2}}=\frac{k}{2\pi} \\
g_{1}(k) = \frac{1}{2\pi}
\end{gather}

These relations are always valid as they only depend on the dimensionality of the systems and on periodic boundary conditions, i.e. they are meaningful as long as the definition of the reciprocal space is. Let's assume an isotropic dispersion relation (or isotropic effective mass) such that we can write $g(\varepsilon)=g(k)\frac{dk}{d\varepsilon}=g(k)\frac{1}{\hbar v}=g(k)\frac{m}{\hbar\sqrt{2m\varepsilon}}$. Therefore:

\begin{gather}
g_{3}(\varepsilon)= \frac{\sqrt{2m^{3}\varepsilon}}{2\pi^{2}\hbar^{3}} \\
g_{2}(\varepsilon)= \frac{m}{2\pi \hbar^{2}}  \\
g_{1}(\varepsilon)= \frac{\sqrt{m}}{\pi \hbar \sqrt{8 \varepsilon}}
\end{gather}

The expressions above are valid for a single band, without considering any additional degeneracy. A factor of 2 should be multiplied to take into account spin degree of freedom. Whenever an additional index referring to multiple sub-bands exists, the corresponding single bands DOS multiplied by a proper step function can be added. As an example, the DOS of a 2DEG with two propagating modes with effective masses $m_{1}$ and $m_{2}$ will be given by:

\begin{equation}
g(\varepsilon) = \frac{1}{2\pi\hbar^{2}}\left[ m_{1}\theta(\varepsilon-\varepsilon_{1}) + m_{2}\theta(\varepsilon-\varepsilon_{2}) \right]
\end{equation} 

The characteristic shape of density of states in 3D, 2D, 1D and 0D systems is shown in Fig. ~\ref{fig1:dos}


\begin{figure}[H]
\centering
 \includegraphics[width=1.0\textwidth,keepaspectratio]{graph/dos}
 \caption{Qualitative plot of the density of states in low dimensionality systems.}
 \label{fig1:dos}
 \end{figure}


\section{Magnetic confinement}

\subsection{Landau levels}

As seen in the previous section, the introduction of a confinement potential along one or more directions produces a discretization of electronic states and allows for the creation of sub-bands of nearly free electrons which behaves like free electrons in a system with reduced dimensionality. A confinement potential can be realized either by geometrical features (nanowires, heterostructure quantum wells), by band bending (2DEG at the interface of an heterostructure or the inversion layer in a MOSFET) or by electrostatic gating. In this section, we will see that a strong magnetic field can also introduce confinement and quantization. 

In the exercise \ref{ex1:harmonic} the student should have recognized that the solution of the Schr\"{o}dinger equation for the potential $U(z)=\frac{1}{2}m\omega^{2}_{0}z^{2}$ is just the well known quantum harmonic oscillator, where the solution is given by:

\begin{gather}
\varepsilon_{n}=\left[n+\frac{1}{2} \right]\hbar\omega_{0} \\
\varphi(z)= \sqrt{\frac{1}{2^{n}n!}}\left(\frac{m\omega_{0}}{\pi\hbar}  \right)^{1/4} \exp \left( -\frac{m\omega_{0}z^{2}}{2\hbar} \right)H_{n}\left(\sqrt{\frac{m\omega_{0}}{\hbar}}z \right)
\end{gather}

where $H_{n}$ are Hermite polynomials. Note that for an infinite wall potential (strong confinement) the energies are distributed according to a geometric progression, while for the harmonic potential (weak confinement) the energies are distributed according to an arithmetic progression; therefore in general a weak confinement potential allows for more sub-bands within a given energy window, as expected. For this reason, 2DEG with an additional electrostatic confinement (well approximated by an harmonic potential) usually exhibit a large number of \emph{transverse modes}. A system with strong confinement in one direction (2DEG) and weak confinement in another direction is often called \emph{quasi-1D}, to remember that the distance between sub-bands is rather small and several sub-bands are likely to be populated.

Assume now that we can apply a strong static magnetic field $\mathbf{B}=B\zver$ perpendicular to the confinment direction of a 2DEG. We will consider a strongly confined 2DEG, such that a single sub-band can be considered. The magnetic field can be included in the Hamiltonian by a \emph{Peierl's substitution} $\opp^{2} \Lra \left(\opp - q\Avec \right)^{2}$, where $\Avec$ is the vector potential. The single particle Schr\"{o}dinger equation will be (note: in the following I will identify with $m$ the effective mass of an isotropic medium, dropping the $m^{*}$ notation for readability):

\begin{equation}
\left[\frac{1}{2m} \left(-j\hbar \nabla_{\rvec} -q\Avec \right)^{2} + U(z) \right]\Psi(\rvec)=\varepsilon\Psi(\rvec)
\end{equation}


The choice of $A$ is not unique and will be valid as long it is ensured that $\nabla \times \Avec = \mathbf{B}$. In this case we choose the \emph{Landau gauge} $\Avec = Bx\yver$ (verify that this gauge is a valid one). As the vector potential is independent of $z$ and has no component along $z$ direction, we can still separate the solution in the form $\Psi(\rvec)=\chi(x,y)\varphi(z)$ where $\varphi(z)$ is the confined wavefunction with known solution. Therefore we solve:

\begin{equation}
\left[\frac{-\hbar^{2}}{2m}\frac{\partial^{2}}{\partial x^{2}}+ \frac{1}{2m} \left(-\jmath\hbar \frac{\partial}{\partial y}-qBx \right)^{2} \right]\chi(x,y)=\varepsilon\chi(x,y)
\end{equation}

The quadratic term of the Hamiltonian can be written as:

\begin{equation}
\frac{1}{2m} \left(-\jmath\hbar \frac{\partial}{\partial y}-qBx \right)^{2} = 
\frac{q^{2}b^{2}}{2m}\left(x + \frac{\jmath \hbar}{qB} \right)^{2}=\frac{m\omega_{c}^{2}}{2}\left(x+x_{0} \right)^{2}
\label{eq1:landau1}
\end{equation}

where we consider an electron $q=-e$ and define the \emph{center coordinate} $x_{0}=\frac{-j\hbar}{eB}\frac{\partial}{\partial y}$. If we assume a free electron solution in the $y$ direction, such that $\chi(x,y)=\chi(x)e^{jk_{y}y}$ the center coordinate is simply a scalar  $x_{0}=\frac{\hbar k_{y}}{eB}$ and the quadratic term in the Hamiltonian corresponds to an harmonic potential centered in $-x_{0}$. As the eigenenergies do not change for rigid translation of the harmonic potential, the dispersion relation is given by the discrete solutions of the oscillator:

\begin{equation}
\varepsilon_{n}=\left[n+\frac{1}{2} \right]\hbar \omega_{c}
\label{eq1:landau2}
\end{equation}
 
as in the case of the harmonic confining potential. Note that even though the Hamiltonian still depends on $k_{y}$, the dispersion relation does not and the density of states is 'quasi-0D', meaning that it will be given by $n$ discrete delta functions centered on $\varepsilon_{n}$. For the same reason, the group velocity in $y$ direction is zero. In analogy to a classical charged particle subject to Lorentz force, we can think that the electrons are now forced to run along circular orbits. By plugging the classical expression for the cyclotron velocity $v=r\omega_{c}$ in the kinetic energy $\frac{1}{2}mv^{2}$ and comparing with the spectrum in ~\ref{eq1:landau2}, we obtain the quantized cyclotron radius:

\begin{equation}
r_{n}=\left[\frac{2\hbar}{m\omega_{c}}\left(n+\frac{1}{2} \right) \right]^{1/2}=\left[\frac{2\hbar}{eB}\left(n+\frac{1}{2} \right) \right]^{1/2}
\end{equation}  

The minimum orbit $r_{0}=\left( \frac{\hbar}{eB}\right)^{1/2}$ corresponding to the zero point energy is called \emph{cyclotron length} or \emph{magnetic length} $l_{c}$ and is an additional characteristic length scale of low dimensional systems.

\vspace{1cm}

\begin{ExerciseList}
\Exercise The total number of states in te 2DEG does not change when we apply a magnetic field in the direction of confinement. It follows that the Landau levels must be strongly degenerate. Can you calculate the level degeneracy as a function of the magnetic field for a 2DEG? (consider a strongly confined system with one sub-band).
\Exercise What changes if we consider a bulk 3D system instead of a 2DEG? How does the density of states look like?
\end{ExerciseList}

\subsection{Edge states}

We consider the same system described in the previous subsection, i.e. a strongly confined 2DEG under the influence of a static magnetic field along the confinement direction. Additionally, we consider a weakly confining potential in the $x$ direction modeled as an harmonic potential $U(x)=\frac{1}{2}m\omega_{0}^{2}x^{2}$. As no additional terms in $z$ appear, the wavefunction can still be decomposed as $\Psi(\rvec)=\varphi(z)\chi(x,y)$. Following the samre procedure we can easily write the Schr\"{o}dinger equation in the $x,y$ directions:

\begin{equation}
\left[\frac{-\hbar^{2}}{2m}\frac{\partial^{2}}{\partial x^{2}}+ \frac{m\omega_{c}^{2}}{2}\left(x+x_{0} \right)^{2} + U(x) \right]\chi(x,y)=\varepsilon\chi(x,y)
\label{eq1:edge1}
\end{equation}

where $x_{0}$ is the center coordinate $x_{0}=\frac{\hbar k_{y}}{eB}$, defined in the previous section. The Hamiltonian in eq.~\ref{eq1:edge1} can be rewritten as

\begin{equation}
\opham = \frac{-\hbar^{2}}{2m}\frac{\partial^{2}}{\partial x^{2}}
+ \frac{mx^2}{2}\left(\omega_{c}^2+\omega_{0}^2 \right)+m\omega_{c}^2x_{0}x+\frac{m\omega_{0}^2 x_{0}^2}{2}
\end{equation}

We define a new center coordinate $x'_{0}=x_{0}\frac{\omega_{c}^2}{\omega^2}$ where $\omega^2=\omega_{c}^2+\omega_{0}^2$ such that $\omega^2 x'_{0}=\omega_{c}^2 x_{0}$. The Hamiltonian can be rewritten:

\begin{equation}
\begin{aligned}
\opham &= \frac{-\hbar^{2}}{2m}\frac{\partial^{2}}{\partial x^{2}} +
\frac{m \omega^2 x^2}{2}+mx'_{0}\omega^2 x + \frac{m{x'}_{0}^{2} \omega^4}{2\omega_{c}^2} = \\
& = \frac{-\hbar^{2}}{2m}\frac{\partial^{2}}{\partial x^{2}} +
\frac{m \omega^2}{2}\left(x+{x'}_{0} \right)^2 - \frac{m\omega^2 {x'}_{0}^2}{2}+\frac{m{x'}_{0}^2 \omega^{4}}{2 \omega_{c}^2}
\end{aligned}
\label{eq1:edge2}
\end{equation}

It is convenient to write the last two terms of eq.~\ref{eq1:edge2} as a function of $x_{0}$:

\begin{equation}
\begin{aligned}
& - \frac{m\omega^2 {x'}_{0}^2}{2}+\frac{m{x'}_{0}^2 \omega^{4}}{2 \omega_{c}^2} = - \frac{m\omega_{c}^4 x_{0}^2}{2\omega_{2}}+\frac{mx_{0}^2 \omega_{c}^{2}}{2} = \\
= & \frac{mx_{0}^2\omega_{c}^2\omega_{0}^2}{\omega^2}=\frac{m^{2}\omega_{c}^2x_{0}^2}{2M}
\end{aligned}
\label{eq1:edge3}
\end{equation}

where we have defined $M=m \frac{\omega^2}{\omega_{0}^2}$. By recalling that $\omega_{c}=\frac{eB}{m}$ and $x_{0}=\frac{\hbar k_{y}}{eB}$ we can write the total Hamiltonian as:

\begin{equation}
\opham=\frac{-\hbar^{2}}{2m}\frac{\partial^{2}}{\partial x^{2}} +
\frac{m \omega^2}{2}\left(x+{x'}_{0} \right)^2+\frac{\hbar^2 k_{y}^2}{2M}
\end{equation}

which is the Hamiltonian of an harmonic oscillator centered in $-{x'}_{0}$ plus an additional free-electron like term in $y$ direction. As the oscillator has no components in $y$ direction, the solution is simply given by:

\begin{equation}
\varepsilon_{n}=\left(n+\frac{1}{2} \right)\hbar \omega+\frac{\hbar^2 k_{y}^2}{2M} 
\label{eq1:edge4}
\end{equation}

Note that in the limiting case $\omega_{c}\gg \omega_{0}$ the second term in eq.~\ref{eq1:edge4} vanishes and we recover the discrete dispersion characteristic of discrete Landau level, while in the case $\omega_{0}\gg \omega_{c}$ the free-electron like dispersion dominates. This means that if the confinement is strong enough, the cyclotron orbit cannot form and the propagation is still close to an ideal 1D system. 

\begin{figure}
\centering
\begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth]{graph/edge1}
        \end{subfigure}
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth]{graph/edge2}
        \end{subfigure} 
\begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth]{graph/edge3}
        \end{subfigure} 
\caption{Dispersion relation for three different values of $\omega_{0}$}
\label{fig1:edge1}
\end{figure}

The script used to generate the plots in fig.~\ref{fig1:edge1} have been generated with the following script (you can fined the sources of all scripts in the git repository):

\small
\begin{verbatim}
import numpy
import matplotlib.pyplot as plt
from matplotlib import rc

#SI constants
me = 9.110e-31
hbar = 1.055e-34
e = 1.602e-19
#Define the physical parameters
#Effective mass (electron mass units, isotropic)
m=0.07
#Magnetic field (Tesla)
B=5.0
#Confinement potential (in unit of cyclotron frequency omegac)
omega0=10.0
#Center coordinate range (in nm)
x0_range = 50.0
#Number of coordinate steps
x0_n = 500
#Number of sub-bands
subbands = 5
#Calculation
x0 = 1e-9*numpy.linspace(-x0_range, x0_range, x0_n)
omegac = e*B/(me*m)
bigm = me*m*(1.0+numpy.power(1.0/omega0,2.0))

energy=numpy.zeros((subbands,x0.size))
for nn in range(subbands):
    energy[nn,:] = ((nn+0.5)*hbar*omegac +\
            (numpy.power(x0*e*B,2.0)/(2.0*bigm)))/e

for nn in range(subbands):
    plt.plot(x0*1e9, energy[nn,:])
plt.xlabel(r'Center coordinate [nm]')
plt.ylabel(r'Energy [eV]')
plt.title(r'$\omega_{0}=10.0\omega_{c}$')
plt.show()
\end{verbatim}
\normalsize

The harmonic potential case is solvable analytically, however the dispersion looks more interesting if we assume a strong confinement in the $x$ direction, as we would get from an abrupt interface (an edge). The resulting dispersion can be calculated numerically; an exact expression of the wavefunction is in ref. \cite{Ferry2009} at pag. 73. 

 \begin{figure}[h]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/ferry_edge}
 \caption{Reproduced from ref. \cite{Ferry2009}}
 \end{figure}
 
 
When $x_{0}$ is small the dispersion is almost flat while a parabolic dispersion is recovered for $x_{0}$ sufficiently high. Physically, this means that the free-electron like states are confined near the edges of the structure, while close to the center the group velocity goes to zero. Therefore these states are called \emph{edge states}.
An intuitive representation of the edge states can be given in analogy to classical skipping orbits, as depicted in Fig.~\ref{fig1:edge4}.

 \begin{figure}[h]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/edge4}
 \caption{Cartoon representation of edge states and cyclotron orbits in a 2DEG}
 \label{fig1:edge4}
 \end{figure}
 

This is of course a simplified picture, remember that a different choice of gauge should not affect the value of the observables, but it could change the functional form of the wavefunction, therefore the connection between the analytical solution and a 'pictorial' intuitive image is not that straight. You can find a different treatment (symmetric gauge) in ref. \cite{Piroth2008}. In the following paragraphs, I will introduce two important concepts which can be observed in 2DEG magnetically confined systems: DOS broadening and resistance quantization. In order to understand the physical meaning of such phenomena we will need to wait for the following chapters, as we still didn't introduce any theory of charge transport, therefore I will only give a quick overview.


\subsection{Level broadening}

In the previous section we verified that a strong magnetic field can modify the density of states of the 2DEG turning it into a set of discrete levels with high degeneracy. Therefore in an ideal system of non-interacting particles the DOS is given by:

\begin{equation}
g(\varepsilon) = \sum_{n} \varepsilon_{n}\delta (\varepsilon - \varepsilon_{n})
\end{equation}

In real systems, a certain amount of impurities is unavoidable (lattice defects, charged or neutral impurities, interface roughness etc.). As a result of nonidealities, the DOS exhibit a finite  broadening. The exact shape of the broadening is hard to determine, a comprehensive study of the phenomena can be found in a review by Ando \cite{Ando1982}, where an analytical expression is given in the limit of low Landau index (gaussian form) and high Landau index (semi-elliptic form, cited also by Ferry \cite{Ferry2009}). Fig.~\ref{fig1:edge5} shows the shape of the DOS for different Landau levels. 

 \begin{figure}[h]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/ando}
 \caption{Reproduced from ref. \cite{Ando1982}}
 \label{fig1:edge5}
 \end{figure}

Besides the exact shape of the DOS, the important point is that the broadening is controlled by a single parameter $\Gamma$. In the limit of short range impurities, i.e. with a potential range $d<l_{c}/(2n+1)^{1/2}$, the broadening factor is given by:

\begin{equation}
\Gamma ^{2}=\frac{2}{\pi}\hbar \omega_{c} \frac{\hbar}{\tau}
\end{equation}

where $\tau$ is the scattering time for the considered impurity. $\Gamma$ is dimensionally an energy, as well as $\hbar / \tau$. The relation between scattering phenomena and DOS broadening is not limited to this case, and is connected to the time-energy uncertainty principle $\Delta T \Delta E \geq \hbar/2$. In fact, we will see the connection etween scattering, broadening and particle lifetime again during the lectures.


