\chapter{Green's Function}

The Green's function method offers a powerful analytical and numerical tool to calculate currents and transmission curves in nanostructures, with some peculiar advantages over the transfer and scattering matrix method. It can easily incorporate open boundary conditions at a perturbative and exact level; it is closely related to time evolution propagator therefore can be expressed in a perturbation form to include many body interactions and it allows for a rigorous generalization to non-equilibrium systems. 

In this chapter I will give an overview of the single particle equilibrium method. The non-equilibrium Keldysh formalism is out of the scope of the notes, however we show how the current can be written in the Keldysh form, more closely related to semi-classical description.  


\section{Schr\"{o}dinger equation Green's function}

Green's functions are a powerful tool to solve inhomogeneous diffeerntial equations, and are often approached first during Electrodynamics courses to solve the Poisson equation for a point charge and recover the expression of the Coulomb force. In this section we briefly remind the definition and will derive an operator expression for the Green's function associated with the Schr\"{o}dinger equation. 

Given a differential linear operator $\diffop$, the associate Green's function is defined as 

\begin{equation}
\diffop \green(x, s) = \delta(x-s)
\end{equation}

The Green's function is simply the solution of the general inhomogeneous differential equation 

\begin{equation}
\diffop u(x) = f(x)
\label{eq:green1}
\end{equation}

when $f(x)$ is an impulse. Once the Green's function is known, the  eq.~\ref{eq:green1} can be solved for any given inhomogeneous term $f(x)$. In fact we have 

\begin{equation}
f(x) = \int \delta(x-s) f(s) \dif s = \int \diffop \green(x,s)f(s) \dif s 
\end{equation}

therefore we can write

\begin{equation}
\diffop u(x) = f(x) = \int \diffop \green(x,s)f(s)\dif s
\end{equation}

$\diffop$ is by definition a linear operator acting on $x$, therefore we can bring it out and apply the inverse operator on both side, and we are left with

\begin{equation}
u(x)=\int \green(x,s)f(s) \dif s
\end{equation}

The equation above allows to calculate the solution of the inhomogeneous differential equation for any excitation $f(x)$. 

We can now apply this definition to find the Green's function associated to a single particle Schr\"{o}dinger equation

\begin{equation}
\left[\jmath \hbar \pd{}{t} - \ham_{0}(\rvec) \right]\green(\rvec, \rvec '; t, t') = \delta(\rvec - \rvec ')\delta(t - t')
\end{equation}

where $\ham_{0}$ is the hamiltonian of the unperturbed system. We use the familiar wave equation, but the results are not dependent on the basis we choose. Let's now assume that we are not interested in time-dependent solution and that  we can postulate that the system will reach a \emph{steady state} solution (i.e. the state only depends on time differences); in that case we can substitute $t-t'$ with $\tau$ and Fourier transform the whole equation \footnote{The Fourier transform is ill defined in $t=t'$. I will ignore it as the introduction of retarded and advanced Green's function, following shortly, removes this singularity.}:

\begin{equation}
F\left[ \left( \jmath \hbar \pd{}{t} - \ham_{0}(\rvec)   \right) \green(\rvec, \rvec '; \tau)\right] = \delta(\rvec - \rvec ')
\label{eq:green3}
\end{equation}

We use the convention to work in energy rather than in frequency, therefore we apply the Planck relation $\varepsilon = \hbar \omega$ \footnote{This notation is more popular among the device  community, being more intuitive when applied to transport, while theoretical physicists prefer to work with frequency.}. The equation
~\ref{eq:green3} becomes 

\begin{equation}
\left[\varepsilon - \ham_{0}(\rvec) \right]\green(\rvec, \rvec '; \varepsilon) = \delta(\rvec - \rvec ')
\end{equation}

We defined the equation in a real space representation to give a more familiar expression of the differential operators. However, the derivation is still valid if we don't explicitly fix the base, and we can write the equation above in a more convenient general form

\begin{equation}
\left[\varepsilon - \ham_{0} \right]\green(\varepsilon) = 1
\end{equation}

When we work in a localized tight binding bases, common in computater modellin, the operators are writte in a matrix representation and the calculation of the Green's function corresponds to an inversion operation

\begin{equation}
\matgreen(\varepsilon) = \frac{1}{\varepsilon - \matham_{0}}
\label{eq:green5b}
\end{equation}

\begin{ExerciseList}
\Exercise Demonstrate that in case of non-orthogonal basis, the equation above becomes
\begin{equation}
\matgreen(\varepsilon) = \frac{1}{\varepsilon \matoverlap - \matham_{0}}
\end{equation}
where $S$ is the overlap matrix $\matoverlap _{ij}=\Braket{i \vert j}$
\end{ExerciseList}

Eq. ~\ref{eq:green5b} is still ill-defined when $\varepsilon = \varepsilon_{0}$ where $\varepsilon_{0}$ are the eigenvalues of $\matham_{0}$, as the inverse operator is singular in these points. To avoid singularity, we have to introduce a small imaginary part which lifts the poles of the Green's function from the real axis into the upper and lower complex plane:

 \begin{equation}
\matgreen^{r,a}(\varepsilon) = \frac{1}{\varepsilon - \matham_{0} \pm \jmath \eta}
\label{eq:green5}
\end{equation}

where $\eta \rightarrow 0$ is an arbitrary small real number, 'r' stads for retarded, corresponding to the addition of $+\jmath \eta$, and 'a'for advanced, corresponding to $-\jmath \eta$ \footnote{This way of introducing the imaginary term is similar to Sakurai, Modern Quantum Mechanics ch.7.}.

Now that we have derived a matrix expression for the Green's function, we will try to understand why is it useful at all. First, we will see that the single particle Green's function allows to describe exactly the particle spectrum of a non-interacting open quantum systems. 


\section{Spectral function}

The Green's function has a number of interesting properties. The first one, already clear after the derivation, is that its poles contain the quasi-particle energies. Moreover, it contains information on the density of states and on the solutions to the Schr\"{o}dinger equation. In a complete orthogonal basis, we may expand the definition ~\ref{eq:green5} by using the completeness relation. This is the so called eigenbasis expansion.  

\begin{equation}
\matgreen^{r,a} = \sum_{i}\frac{\Ket{i}\Bra{i}}{\varepsilon -\varepsilon_{i}\pm \jmath \eta}
\end{equation}

We define the \emph{spectral function} as 

\begin{equation}
\matspectral = \jmath \left(\matgreen^{r} - \matgreen^{a} \right)
\end{equation}

In the eigenbasis expansion

\begin{equation}
\matspectral = \jmath \sum_{i} \Ket{i}\Bra{i} \left[ \frac{1}{\varepsilon - \varepsilon_{i} +\jmath \eta} - \frac{1}{\varepsilon - \varepsilon_{i} -\jmath \eta} \right] = \sum_{i}\Ket{i}\Bra{i} \frac{2\eta}{(\varepsilon-\varepsilon_{i})^2 + \eta ^2}
\end{equation}

The second term is a Lorentzian centred on the eigenvalue $\varepsilon_{i}$. Therefore the limit when $\eta \rightarrow 0$ the expression above is given by

\begin{equation}
\matspectral(\varepsilon) = 2\pi \sum_{i}\delta(\varepsilon - \varepsilon_{i})\Ket{i}\Bra{i}
\end{equation}

From the definition of density of states it follows that the density of state is defined by

\begin{equation}
D(\varepsilon) = \frac{1}{2\pi} \Tr \left[\matspectral(\varepsilon) \right]
\end{equation}


The spectral function contains all the information about the density of states, i.e. the energy spectrum of the system described by the associated differential operator. Noticeably, while a direct diagonalization is not always possible even in the case of single-particle hamiltonian, the spectral function can usually be calculated and this is only one of the powerful features of the Green's function formalism. We will explout this property to calculate the spectrum of a system with open boundary conditions.



\section{Open quantum systems}

Imagine a setup similar to the previously introduced Landaer setup, where a central scattering region is described at equilibrium by a given single-particle hamiltonian $\matham_{D}$. The device is slowly connected to a semi-infinite left and a right electrode, described by the hamiltonian $\matham_{L}, \matham_{R}$. When the systems are connected, a non-zero interaction described by the hopping matrices $\matham_{LD}, \matham_{DL}, \matham_{RD}, \matham_{DR}$ arises. We assume also that we work in a local basis and that the Hilbert spaces of electrodes and device are initially independent. After connecting the system, the total Hilbert space is the direct sum of all the initial Hilbert spaces. In matrix form \footnote{we assume orthogonal basis for simplicity} the total system is described (in matrix representation) by

\begin{equation}
\begin{bmatrix}
\matham_{L} & \matham_{LD}& 0 \\
\matham_{DL} & \matham_{D} & \matham_{DR} \\
0 & \matham_{RD} & \matham_{R}
\end{bmatrix}
\begin{bmatrix}
\varphi_{L} \\ \varphi_{D} \\ \varphi_{R}
\end{bmatrix}= \varepsilon
\begin{bmatrix}
\varphi_{L} \\ \varphi_{D} \\ \varphi_{R}
\end{bmatrix}
\end{equation}

The contacts Hamiltonian is still semi-infinite, therefore we can not solve this problem by direct diagonalization. From the definition of Green's function we may write

\begin{equation}
\begin{bmatrix}
\varepsilon - \matham_{L} & -\matham_{LD}& 0 \\
-\matham_{DL} & \varepsilon - \matham_{D} & -\matham_{DR} \\
0 & - \matham_{RD} & \varepsilon - \matham_{R}
\end{bmatrix}
\begin{bmatrix}
\matgreen_{L} & \matgreen_{LD}& \matgreen_{LR} \\
\matgreen_{DL} & \matgreen_{D} & \matgreen_{DR} \\
\matgreen_{RL} & \matgreen_{RD} & \matgreen_{R}
\end{bmatrix}=I
\end{equation}

Fro this equation we can write 

\begin{eqnarray}
\matgreen_{LD}=g_{L} \matham_{LD}\matgreen_{D} \\
\matgreen_{RD}=g_{R} \matham_{RD}\matgreen_{D} \
\end{eqnarray}

where $g_{L,R}^{r,a}=\left(\varepsilon - \matham_{L,D} \pm \jmath \eta \right)^{-1}$
 is called \emph{surface Green's function} of the left (right) electrode, an represent the Green's function of the isolated contact. Combining these expressions with the relation
 
 \begin{equation}
 -\matham_{DL}\matgreen_{LD} + (\varepsilon - \matham_{D})\matgreen_{D}- -\matham_{DR}\matgreen_{RD} = I
 \end{equation}
 
 we obtain 
 
 \begin{equation}
 \matgreen_{D} = \frac{1}{\varepsilon - \matham_{D} - \matselfener_{L} - \matselfener_{R}} 
 \end{equation}
 
 where 
 
 \begin{equation}
 \matselfener_{L} = \matham_{DL}g_{L}\matham_{LD} 
 \end{equation}
 
is called the electrode \emph{Self Energy}. Note that the Self-energy of the electrode is symmetric but not Hermitian, and moreover is non-local (in matrix representation, the inverse of a sparse matrix is dense therefore $g_{L,R}$ are filled with off-diagonal terms). The inclusion of the electrodes is to practical extent including this additional non-local non-hermitian potential correction to the device hamiltonian:

\begin{equation}
H_{D} \rightarrow H_{D}+\Sigma
\end{equation}

As a first striking consequence we find that the coupling with the electrodes can be represented within a closed finite region by a correction term, at the price of breaking hermitianicity. 
 
Note that even though $g_{L}$ is infinite, the self energy has finite size because the interaction blocks $\matham_{LD, RD}$ have finite size. If we are able to calculate $g_{L,R}$, we are indeed able to calculate the \emph{exact} Green's function for the device connected to the semi-infinite electrodes.

\vspace{1cm}
 
\begin{ExerciseList}
\Exercise Demonstrate that the spectral function of a system with electrodes can be written as 
\begin{equation}
\matspectral(\varepsilon) = \matgreen^{r}\Gamma\matgreen^{a}
\end{equation}
where $\Gamma = \jmath \left[\matselfener^{r} - \matselfener^{a} \right]$ and $\matselfener^{r,a}=\matselfener^{r,a}_{L} + \matselfener^{r,a}_{R}$
\end{ExerciseList}


\subsection{Connection to Dyson equation}

In the previous section we derived the device Green's function directly from the full hamiltonian. It can be shown that the resulting expression is a particular case of the \emph{Dyson equation}, well known in time-dependent perturbation theory. Consider an equilibrium hamiltonian to which a perturbation term is added

\begin{equation}
\matham = \matham_{0} + V
\end{equation}

where $V$ is a generic perturbing potential. By applying the following operator identity

\begin{equation}
\frac{1}{A+B}=\frac{1}{A}\left(1- \frac{B}{A+B} \right)
\end{equation}

we can write

\begin{equation}
\matgreen = \frac{1}{\varepsilon - \matham_{0} - V}=\frac{1}{\varepsilon - \matham_{0}}\left(1+\frac{V}{\varepsilon - \matham} \right)=\matgreen_{0}1+ \matgreen_{0}V\matgreen 
\end{equation}

We can substitute iteratively $G$ on the right hand side and obtain an infinite serie

\begin{equation}
G=G_{0}+G_{0}VG_{0}+G_{0}VG_{0}VG_{0}+G_{0}VG_{0}VG_{0}VG_{0}+ \ldots
\end{equation}

This form resemble the Dyson serie for the time propagation operator, and the two pictures are indeed intimately connected. The Self energy can be therefore used to represent perturbation expansion in a compact way.



\subsection{Physical meaning of electrode Self Energy}

Consider a single-site tight binding system with a given eigenenergy $\varepsilon_{0}$. As we saw, when we adiabatically connect it with an electrode, we can equivalently think to switch on a perturbation potential such that the total energy is now given by $\varepsilon = \varepsilon_{0} + v$. The perturbing potential is in general complex: the real part provides a shift of the energy level and is commonly due to hybridization of orbitals between the site and the lead; the imaginary part provides a characteristic time for charge injection and extraction. If we consider a purely imaginary $v=  \jmath \gamma$ where $\gamma$ is a positive real number,  we can write $\varepsilon = \varepsilon_{0} + \jmath \gamma$. It is straight-forward to demonstrate that the time-dependent solution can be written in the same form we use for self adjoint Hamiltonian:

\begin{equation}
\Ket{i; t} = \Ket{i}e^{-(\jmath \varepsilon t)/\hbar}e^{-(\gamma t)/\hbar}
\end{equation}

The presence of the self-energy $\gamma$ induce an exponential decay of the state $\Ket{i}$: this is the probability to be adsorbed from the electrodes. In fact, a strong coupling means a short lifetime, and vice versa. 

This condition is reflected in the density of states. If we include the self-energy in the expression for the density of states, we can easily see (left as exercise) that the density of states is still given by a superposition of Lorentzian with shifted energies and finite broadening. Reminding that the Lorentzian distribution in energy is the Fourier transform of an exponentially decaying process in time, a physical consequence is that coupling with electrode will determine a broadening of states proportional to the strength of the coupling  and the particle lifetime. This relation allows to shred new light on the Breit-Wigner formula we derived using the transmission matrix: the $\Gamma$ parameters we used are indeed quantum bath terms due to a non zero coupling of a bound state with an electron reservoir.  


\subsection{How to calculate the contact self-energy}

In order to calculate the self-energy of the electrodes, we need to know the surface Green's function $g$. A first method is simply 'we do not calculate it'. Assume that we our electrode consists in a semi-infinite bulk of a given material, and that we know the density of states of that material. If the device region is weakly coupled, we can imagine that the energy shift can be negligible (hybridization between surface orbitals of electrode and frontier orbitals of the system is small), therefore the self energy is purely imaginary. We also assume that the self-energy can be assumed to be local (i.e. diagonal in matrix representation) and that the density of states of the system is nearly constant in the energy interval of interest. 

By applying the definition of density of states to the surface of the electrode, we can find that under the assumption specified above the surface green's function can be simply written as

\begin{equation}
\Im{g^{r}_{s}}=-\frac{1}{2}a
\end{equation}

where $a$ is the spectral function of the electrode and can be derived easily, if we consider an homogeneous systems where all the atoms contribute equally to the density of states, from the condition

\begin{equation}
\Tr[a] = \frac{1}{2\pi} \tilde{D}
\end{equation}

where $\tilde{D}$ is the average density of states.As an alternative, the total self-energy can be specified as a phenomenological injection rate parameter following the arguments of the previous section. This method is called \emph{Wide Band Approximation} and is still very popular because the atomistic details of the electrode-device interface are not always known and the coupling is often set as an empirical parameter.

Nevertheless, when microscopic details of the system are known, the Green's function method allows to calculate exactly $g_{s}$. There are several methods based on expansion of Dyson equation. Here I mention only the continued fraction method as it is probably the simplest and it's the basis for other more efficient iterative algorithms. If the hamiltonian of the electrode can be partitioned in sub-blocks, such that a sub block is only interacting with nearest neighbours sub blocks in the following way

\begin{equation}
\matham_{L}  =
\begin{bmatrix}
\ddots & &    \\
\matham_{12} & \matham_{11} & \matham_{10} \\
0 & \matham_{01} & \matham_{00} 
\end{bmatrix}
\end{equation}

then we can iteratively apply the Dyson equation on each sublayer and write the surface Green function $g_{s}=\matgreen_{00}$ as


\begin{equation}
  \matgreen_{00} = \cfrac{1}{\varepsilon - \matham_{00}
          - \cfrac{\matham_{01}\matham_{10}}{\varepsilon - \matham_{11}  - \cfrac{\matham_{12}\matham_{21}}{\varepsilon - \matham_{22} - \cdots}}}
\end{equation}

This quantity will eventually converge to a finite value with arbitrary precision. 
Other methods are based on a direct evaluation of the leads Green's function from a known Bloch states solution and application of the eigenvector expansion of the Green's operator. 
A comprehensive review of different methods can be found in ref. \cite{Velev2004}.

\section{Response to excitation}

In this section we calculate the response of an electrode/device setup to an incoming wave originated in one of the electrodes. The result will be then used in order to calculate the density matrix and the tunneling current in the open system. To this end, we follow a procedure similar to Todorov \cite{Todorov2002} with some simplification inspired by a paper from Paulsson \cite{Paulsson2008}. 

We define first a \emph{projection operator} $\opP_{i} = \Ket{i}\Bra{i},\opP_{ji} = \Ket{i}\Bra{j} $. The projection operator applied to a generic ket is simply selecting the component of the ket along the state vector $\ket{i}$. Given an Hilbert subspace described by the states $\ket \in A$, we can similarly define a projection operator on the given subspace $\opP_{A}=\sum_{i \in A}\Ket{i}\Bra{i}$, and an operator between two subspaces $\opP_{AB}=\sum_{i \in A; j \in B}\Ket{i}\Bra{j}$ In matrix notation, the projection operator is selecting some specific sub-blocks. We define then an excitation $\Ket{n,L0}$ given by the $nth$ solution to the Schr\"{o}dinger equation in the left electrode, when it is disconnected from the rest of the system. When the electrode is adiabatically connected to the device, the full solution will be given by a response $\Ket{n}$ of the system to the excitation $\Ket{n,L0}$, plus the excitation itself. In fact, this can be easily shown by writing down the full Schr\"{o}dinger equation.

\begin{equation}
\opham \left(\Ket{n} + \Ket{n,L0} \right) = \varepsilon \left(\Ket{n} + \Ket{n,L0} \right)
\end{equation}

We use the labels $L,R,D$ to represent the basis set localized on the left and right electrode and on the device, similarly to what we did in matrix notation. From the closure relation it follows that $\opP_{L} + \opP_{D} + \opP_{R} = 1$, and therefore $\opham \ket{n,L0} = \opham_{L} \ket{n,L0} + \opham_{DL} \ket{n,L0}$ where $\opham_{DL} = \opP_{D}\opham \opP_{L}$ \footnote{Note that this is all quite obvious in matrix representation.}. By definition $\opham_{L}\Ket{n,L0}=\varepsilon \Ket{n,L0}$, therefore we can finally write

\begin{equation}
\opham\Ket{n}= \varepsilon\Ket{n}-\opham_{DL}\Ket{n,L0}
\end{equation} 

This is exactly the inhomogeneous Schr\"{o}dinger equation with an excitation term $-\opham_{DL}\Ket{n,L0}$ and the solution can be written as

\begin{equation}
\Ket{n}= \green \opham_{DL}\Ket{n,L0}
\end{equation}

We indicate as $\Ket{n,D}=\opP_{D}\Ket{n}$  the response in the device region only and as $\Ket{n,L}, \Ket{n,R}$ the response in the left and right lead. By the same argument the student can verify that the following identities are verified

\begin{eqnarray}
\Ket{n,D} = \green_{D}\opham_{DL}\Ket{n,L0} \\
\Ket{n,R} = g_{R}\opham_{RD}\green_{D}\opham_{DL}\Ket{n,L0} \nonumber \\
\Ket{n,L} = (g_{L}\opham_{LD}\green_{D}\opham_{DL} - 1)\Ket{n,L0} \nonumber 
\end{eqnarray}

These expressions will be useful in the following sections, where we calculate the density matrix and the coherent current in the open system.


\section{Density matrix}

The Green function allows also for a definition of the density matrix by correctly taking into account the injection of carriers from the electrodes. 
By definition the density matrix is given by the matrix element of the corresponding density operator

\begin{equation}
\sum_{i} p_{i}\Ket{i}\Bra{i}
\end{equation}

where $p_{i}$ is the probability to find the quantum system in the state $i$. In a grand canonical ensemble of fermions with chemical potential $\mu$, $p_{i}$  is simply given by the Fermi-Dirac distribution 

\begin{equation}
\sum_{i} f(\varepsilon_{i}, \mu) \Ket{i}\Bra{i} \delta(\varepsilon - \varepsilon_{i})
\label{eq:green7}
\end{equation}

We consider the density matrix in the device region due to all the incoming waves from the left electrode, as calculated in the previous section. 
The density matrix in the device region due to states injected by the left contact is

\begin{eqnarray}
\rho_{D,L} = \int_{-\infty}^{+\infty}\sum_{n}f(\varepsilon, \mu_{L})\delta(\varepsilon - \varepsilon_{i})\Ket{n,D}\Bra{n,D} \dif \varepsilon = \\
= \int_{-\infty}^{+\infty}\sum_{n} f(\varepsilon, \mu_{L})\delta(\varepsilon - \varepsilon_{i})\matgreen_{D}\matham_{DL}\Ket{n,L0}\Bra{n,L0}\matham_{LD}\matgreen_{D}^{\dagger} \dif \varepsilon \nonumber
\end{eqnarray}

The quantity $\delta(\varepsilon - \varepsilon_{n})\Ket{n,L0}\Bra{n,L0}$ is equal, aside a factor, to the spectral density of the isolated contact, i.e. 

\begin{equation}
a(\varepsilon) = 2\pi \delta(\varepsilon - \varepsilon_{i})\Ket{n,L0}\Bra{n,L0}
\end{equation}

It can be easily shown (compare the definition of spectral function and the definition of Self Energy) that \footnote{when the retarded, advanced indexes are not specified, we implicitly assume to work with the retarded response}

\begin{equation}
\Gamma_{L} = \matham_{DL}a_{L}\matham_{LD} = \jmath \left[ \matselfener_{L} - \matselfener_{L}^{\dagger} \right]
\end{equation}

Therefore we get the final expression

\begin{equation}
\rho_{D,L} = \frac{1}{2\pi}\int_{-\infty}^{+\infty}f(\varepsilon, \mu_{n}) \matgreen_{D}^{r}\Gamma_{L}\matgreen_{D}^{a} \dif \varepsilon
\end{equation}

In case of $N$ contacts, the total density matrix is simply given by the some of the response to all the incoming waves

\begin{equation}
\rho_{D} = \sum_{n=1}^{N} \frac{1}{2\pi}\int_{-\infty}^{+\infty}f(\varepsilon, \mu_{L}) \matgreen_{D}^{r}\Gamma_{n}\matgreen_{D}^{a} \dif \varepsilon
\end{equation}


\section{Landauer-Caroli current formula}

We now demonstrate that the we can evaluate the current across an electrode and recover a Landauer-like form. In a tight binding basis we can not directly apply the probability current definition. In fact the basis is not complete in $\rvec$ and if we attempt to directly evaluate the terms $\opp \Ket{i}$, where $\opp$ is the momentum operator, by using the real space representation of $\opp$ we may get to a wrong result and break conservation rules. We start therefore from the definition of electric current $I=\od{n}{t}$ where $n$ is the charge density. 

The charge density on a basis $i$ is the expectation value of the projection operator $\opP_{i}$, therefore we may evaluate the currents starting from the equation of motion of the projection operator in Heisenberg representation

\begin{equation}
\od{n_{i}}{t}=\frac{1}{\jmath \hbar}\left[\opP_{i},\opham \right]=\opJ
\end{equation}

where we just defined a site current operator $\opJ$. It is easy to demonstrate that 

\begin{equation}
\opJ_{i}=\sum_{j \neq i} \opJ_{ji} = \frac{1}{\jmath \hbar}\left[\opP_{i}\opham \opP_{j} -  \opP_{j}\opham \opP_{i} \right]
\end{equation}

This can be demonstrated by plugging as usual the identity operator in front of the operators and remembering that given a local basis the matrix element $\Braket{i| \opham|j}$ is non zero only for a subset of states $i,j$.
We are now ready to evaluate the current across a surface $S$. Consider a surface dividing a quasi one-dimensional system in a left and a right section (in our case, we can consider the surface separating the right electrode from the device plus left electrode). If we label as $1,2$ the states on the left and on the right of the surface, we can write the current flowing through as

\begin{eqnarray}
\opJ_{S}= \frac{1}{\jmath \hbar}\sum_{i \in 2, j \in 1} \opJ_{ji} = \frac{1}{\jmath \hbar}\left[\opP_{2}\opham\opP_{1}-\opP_{1}\opham\opP_{2} \right] = \\
=\frac{1}{\jmath \hbar}\left[\opP_{2}\opham-\opham\opP_{2} \right] 
=- \frac{1}{\jmath \hbar}\left[\opP_{1}\opham-\opham\opP_{1} \right] \nonumber
\end{eqnarray}

These relations are easily demonstrated by noting that $\opP_{1}+\opP_{2}=1$. The target is to evaluate the current at the right electrode by a procedure similar to what we did for the density matrix. In the following I will label as usual $D,L,R$ as device, left electrode and right electrode subspaces. We consider the response to an incoming wave from the left contact, as previously. The expectation value of the surface current is given by 

\begin{equation}
<\opJ_{S}>=\Tr[{\rho \opJ_{S}}]
\end{equation}

where $\rho=\sum_{n}p_{n}\Ket{n}\Bra{n}$ is the density matrix of the whole system. 
Therefore

\begin{equation}
<\opJ_{S}>=\frac{1}{\jmath\hbar}\Tr\left[ \sum_{n}p_{n} \Ket{n}\Bra{n}\left(\opP_{R}\opham - \opham \opP_{R} \right) \right]
\end{equation}

Remember that $\opP _{r}\Ket{n}=\Ket{n,R}=g_{R}\opham_{RD}\green_{D}\opham_{DL}\Ket{n,L0}$. If we expand the terms $\Braket{n|\opham \opP_{R}|n}$, $\Braket{n|\opP_{R}\opham|n}$ the only non cancelling terms are $\Braket{n,D|\opham_{DR}|n,R}$, $\Braket{n,R|\opham_{RD}|n,D}$. Reminding that $\Gamma_{R}=\jmath \left(\matselfener_{R} - \matselfener_{R}^{\dagger} \right) = \jmath \opham_{DR}\left(g_{R}-g_{R}^{\dagger} \right)\opham_{RD}$ we end with the following expression:

\begin{equation}
<\opJ_{S}>=\frac{1}{\hbar}\Tr\left[ \sum_{n}p_{n} \Ket{n}\Bra{n,L0}\opham_{LD}\green_{D}^{\dagger}\Gamma_{R}\green_{D}\opham_{DL}
\Ket{n,L0}\Bra{n}\right]
\end{equation}

We plug an identity operator in order to move around some terms

\begin{equation}
<\opJ_{S}>=\frac{1}{\hbar}\Tr\left[ \sum_{n,m}p_{n} \Ket{n}\Bra{n,L0}\opham_{LD}\green_{D}^{\dagger}\Gamma_{R}\green_{D}\Ket{m}\Bra{m}\opham_{DL}
\Ket{n,L0}\Bra{n}\right]
\end{equation}

and after rearranging we can notice that a term $\sum_{n}p_{n}\Ket{n,L0}\Bra{n},L0$ appears. These are equilibrium solution in the left contact, which is populated according to a Fermi distribution $f(\varepsilon_{n}, \mu_{L})$. We can therefore use the same trick we used to calculate the density matrix and we end up with 

\begin{equation}
<\opJ_{S}>=\frac{1}{h}\int_{-\infty}^{+\infty} \dif \varepsilon f(\varepsilon, \mu_{L})\Tr \left[\Gamma_{L}\green_{D}^{\dagger}\Gamma_{R}\green_{D} \right]
\end{equation}

which gives the particle current across the surface $S$ given by the states injected from the left electrode. If we consider similarly the states injected from the right electrode and multiply for $e$ to get the electric current, we get the following formula, called \emph{Landauer-Caroli}

\begin{equation}
I=\frac{2e}{h}\int_{-\infty}^{+\infty}\dif \varepsilon \left[f(\varepsilon, \mu_{L})-f(\varepsilon, \mu_{R}) \right] \Tr \left[\Gamma_{L}\green_{D}^{\dagger}\Gamma_{R}\green_{D} \right]
\end{equation}

where we added $2$ for spin degeneracy. This is analogous to the Landauer formula with 

\begin{equation}
T(E)=\Tr \left[\Gamma_{L}\green_{D}^{\dagger}\Gamma_{R}\green_{D} \right]
\end{equation}


\subsection{Breit-Wigner formula from Green's functions}

After introducing the Landauer Caroli, we calculate the current thorough a device region described by a single state weakly coupled to electrodes. The device Hamiltonian is simply given by the single state energy $H=\varepsilon_{0}$.
After coupling with two electrodes with a given Self Energy $\Sigma=\Sigma_{L}+\Sigma_{R}$ we have

\begin{align*}
\green^{r} &=\frac{1}{\varepsilon - \varepsilon_{0} - \Sigma^{r}} \nonumber \\
T(\varepsilon) &= \frac{\Gamma_{L}\Gamma_{R}}{(\varepsilon - \varepsilon_{0} - \Sigma^{r})(\varepsilon - \varepsilon_{0} - \Sigma^{a})} = \nonumber \\
&= \frac{\Gamma_{L}\Gamma_{R}}{(\varepsilon - \varepsilon_{0})^{2} + \Sigma^{r}\Sigma^{a} + (\varepsilon - \varepsilon_{0})(\Sigma^{r}+\Sigma^{a})}
\end{align*}

If the state is weakly coupled to the electrodes, it is reasonable to assume that the shift in the eigenenergy will be small. You can also consider a possible shift as the renormalization of the eigenenergy of a bound state after couling with free electron states through finite potential barriers.Therefore $\Re(\Sigma^{r})=0$ and $\Gamma=2 \Sigma^{r}$ where $\Gamma=\Gamma_{L}+\Gamma_{R}$ is the sum of left and right lead contribution. Given the property $\Sigma^{r}=\Sigma^{a\dagger}$ the term $\Sigma^{r}+\Sigma^{a}$ is zero. The transmission can be therefore written as

\begin{equation}
T(\varepsilon)=\frac{\Gamma_{L}\Gamma_{R}}{(\varepsilon - \varepsilon_{0})^{2}-\left(\frac{\Gamma_{L}+\Gamma_{R}}{2
} \right)^{2}}
\end{equation}

This is exactly the Breit-Wigner formula already introduced in the previous chapter. We reach the same formula easily as the Green's function machinery takes into account for all the multiple reflection paths and their interference. We also see that the $\Gamma$ functions we introduced are related to the electrodes Self Energies. 


\subsection{Inscattering/Outscattering form}

Even though a more comprehensive transport theory goes well beyond single particle  equilibrium Green's function, in this section I show that you can cast the Landauer-Caroli in a form which is physically more intuitive and allows for an intuitive connection with a semi-classical picture. On a more rigorous ground, this formulation is more fondamental, holds in a much larger number of cases in presence of particle-particle interactions and falls within the non equilibrium Keldysh Green's Function theory. Therefore the usual procedure is the opposite: to demonstrate that the Landauer Caroli can be derived from a more general many body formulation in the limit of elastic scattering. The notation is also a bit different, here we follow an "intuitive" notation by Datta \cite{Datta1997} while the original formulation derives from the second quantization formulation.

We start defining a \emph{inscattering} and \emph{outscattering} Self energies.

\begin{align*}
\Sigma^{in}_{l}=f(\varepsilon)\Gamma_{l}(\varepsilon, \mu_{l}) \nonumber \\
\Sigma^{out}_{l}=(1-f(\varepsilon, \mu_{l}))\Gamma_{l}(\varepsilon)
\end{align*}

where $f$ is the equilibrium distribution and $l$ an electrode index.  Trivially $\Gamma=\Sigma^{in}+\Sigma^{out}$ with $\Sigma^{in,out}=\sum_{l}\Sigma_{l}^{in,out}$. 

We define then an \emph{electron} and \emph{hole} Green's function defined as

\begin{equation}
\green^{n,p}=\green^{r}\Sigma^{in,out}\green^{a}
\end{equation} 

If all the leads have the same chemical potential, hence the same distribution, we have simply

\begin{equation}
\green^{n}(\varepsilon)=f(\varepsilon, \mu)\green^{r}(\varepsilon)\Gamma(\varepsilon)\green^{a}(\varepsilon) = f(\varepsilon, \mu)A(\varepsilon)
\end{equation}

where $A(\varepsilon)$ is the spectral function. The density matrix is then given by

\begin{equation}
\rho = \frac{1}{2\pi}\int \green^{n}(\varepsilon)\dif \varepsilon
\end{equation}

Given these definitions it is easy to demonstrate that the total current flowing through an electrode can be written in the following form

\begin{equation}
I=\frac{2e}{h}\int_{-\infty}^{+\infty}\dif \varepsilon  \Tr \left[\Sigma^{in}_{l}G^{p}-\Sigma^{out}_{l}G^{n} \right]
\label{eq:green8}
\end{equation}

Equation ~\ref{eq:green8} is has a more intuitive physical meaning with respect to the Landauer-Caroli. $\Sigma^{in, out}$ can be interpreted as scattering distribution giving the probability to inject or extract states at a given energy, similar to in and out collision terms in Boltzmann equation. $G^{n,p}$ contain information on the spectrum and occupied and unoccupied states, therefore could be considered a generalization of a distribution function. Indeed, the connection between Green's formalism and Boltzmann equation is more profound and particle-particle interaction like electron-phonon can be taken into account as well. The theoretical tools needed to achieve this result are well beyond the scope of these lectures and usually studied in many body quantum condensed matter physics. The interested reader can have a very basic introduction in the last chapter of ref. \cite{Datta1997} and a (much) more rigorous  approach in ref. ~\cite{Haug2010}.