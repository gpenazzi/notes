\newcommand{\partialt}[1]{\frac{\partial #1}{\partial t}}

\chapter{Coherent transport: Transfer matrix method}

\section{Probability Density Current}

In the following we will define the current density for quantum particles, by
similarity with respect to classical particles. The starting point is the
classical continuity equation, which is derived from Gauss' Law and Ampere's Law:

\begin{equation}\label{Coherent:eq00}
  \partialt{\rho (\rvec, t)} = - \nabla \textbf{J}(\rvec , t)
\end{equation} 

We substitute the particle density with the probability density $\rho(\rvec, t)
= \Psi(\rvec, t)\Psi^{*}(\rvec, t)$ such
that:

\begin{equation}\label{Coherent:eq10}
\partialt {\rho (\rvec , t)} =  \partialt{}
\left[\Psi(\rvec, t) \Psi^{*}(\rvec, t) \right] = \left[\Psi(\rvec, t)
  \partialt{\Psi^{*}(\rvec, t)} + \Psi^{*}(\rvec, t) \partialt{\Psi(\rvec, t)}   \right]
\end{equation}

Eq. ~\ref{Coherent:eq20} is evaluated by writing the time dependent Schr\"odinger equation
and its complex conjugate for $\Psi$. 

\begin{align}\label{Coherent:eq20}
  -\frac{\hbar^{2}}{2m}\nabla^{2}\Psi(\rvec, t)) + V(\rvec, t)\Psi(\rvec, t) & = \jmath \hbar \partialt{\Psi(\rvec, t)} \\ 
  -\frac{\hbar^{2}}{2m}\nabla^{2}\Psi^{*}(\rvec, t)) + V(\rvec, t)\Psi^{*}(\rvec, t) & = -\jmath \hbar \partialt{\Psi^{*}(\rvec, t)} 
\end{align}

By substituting eq. ~\ref{Coherent:eq20} in eq. ~\ref{Coherent:eq10} and noting that $\Psi \nabla^{2} \Psi^{*} - \Psi^{*}\nabla^{2}\Psi = \nabla \left[\Psi \nabla \Psi^{*} - \Psi^{*}\nabla \Psi \right]$, it is straightforward to show that

\begin{equation}\label{Coherent:eq30}
  \partialt \rho{\rvec, t} = \frac{-\jmath \hbar}{2m}\nabla \left[\Psi(\rvec, t)\nabla\Psi^{*}(\rvec, t) - \Psi^{*}\nabla\Psi(\rvec, t) \right]
\end{equation}

This equation is identical to eq. ~\ref{Coherent:eq00} if we define a Probability Density Current, given by:

\begin{equation}\label{Coherent:eq40}
\textbf{J}(\rvec, t) = \frac{\jmath \hbar}{2m} \left[\Psi(\rvec, t)\nabla\Psi^{*}(\rvec, t) - \Psi^{*}\nabla\Psi(\rvec, t) \right] 
\end{equation}

Note that in going from ~\ref{Coherent:eq20} to ~\ref{Coherent:eq30} we need to assume the external potential to be real, otherwise the terms $V\Psi\Psi^{*}$ do not delete each other. If we want to keep the definition of eq. ~\ref{Coherent:eq40}, then a complex potential determines violation ox the continuity equation. In other words, complex potential corresponds to creation/destruction of particles.

Consider now the situation pictured in fig ~\ref{fig:coh5}, where an electronic states is decomposed in an incoming, a reflected and a transmitted contribution.


\begin{figure}[h]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/tun}
 \caption{Schematic of tunneling barrier.}
 \label{fig:coh1}
 \end{figure}
 
 The wavefunction far from the scattering potential can be written as 
 
 \begin{eqnarray}
 \varphi(x\rightarrow +\infty)=B^{+}e^{\jmath kx } \\
  \varphi(x\rightarrow -\infty)=A^{+}e^{\jmath kx } + A^{-}e^{\jmath kx } 
 \end{eqnarray}
 
We are interested in steady state property, therefore the continuity equation reduces to $\nabla \Jvec = 0$, which can be written (by applying Gauss theorem) 

\begin{equation}
\oint_{S} \Jvec \dif \mathbf{S} = 0
\end{equation} 

where S is a surface which enclose the scattering potential. It follows that $J_{inc} = J_{trans} - J_{ref}$ where $J_{inc}, J_{tr}$ and $ J_{ref}$  are the prbability current density contribution of the incoming, transmitted and reflected wave. Far enough from S, $J_{inc}$ can be written as

\begin{equation}
J_{inc} = \frac{\jmath \hbar}{2m} \left[2A^{+}A^{+*}e^{\jmath k_{A}x}e^{-\jmath k_{A}x}(\jmath k_{A}) \right]=\frac{\hbar k_{A}}{m}\lvert A^{+} \rvert ^ 2 = v_{A} \lvert A^{+} \rvert ^ 2 
\end{equation}

Similarly the reflected and transmitted currents are 

\begin{eqnarray}
J_{ref}=v_{A} \lvert A^{-} \rvert ^ 2 \\
J_{tr}=v_{B} \lvert B^{+} \rvert ^ 2
\end{eqnarray}

The \emph{transmission coefficient} is defined as 

\begin{equation}
T=\frac{J_{tr}}{J_{inc}}=\frac{v_{B} \lvert B^{+} \rvert ^ 2}{v_{A} \lvert A^{+} \rvert ^ 2 }
\end{equation}

The \emph{reflection coefficient} is defined as

\begin{equation}
R=\frac{J_{ref}}{J_{inc}}=\frac{\lvert A^{-} \rvert ^ 2}{\lvert A^{+} \rvert ^ 2}
\end{equation}

Gauss theorem ensures that $T+R=1$. If the potential energy is the same on both sides of the scattering potential, energy conservation implies $k_{A}=k_{B}$ and the transmission coefficient is simply $T=\frac{\lvert B^{+} \rvert ^ 2}{\lvert A^{+} \rvert ^ 2}$. Transmission and reflection can be also defined in terms of transmittance and reflectance

\begin{eqnarray}
T = tt^* \mbox{ with } t=\sqrt{\frac{v_{b}}{v_A}}\frac{B^{+}}{A^{+}} \\
R = rr^* \mbox{ with } r=\frac{A^{-}}{A^{+}} \\
\end{eqnarray}


\section{Transfer matrix method}

The transmittance and reflectance amplitudes can be enconded in a very efficient and powerful mathematical formulation: the scattering and transfer matrix method. The application of such methods is, in the way we present them, bounded to the definition of scattering states which are easily written in terms of plane waves or, similarly, Bloch waves. However, the scattering theory is a more complete and popular theory in quantum dynamics. We will here present in a form which is successfully in semiconductor modelling in the last 30 years.last 20 years. 

We start with  a case similar to fig.~\ref{fig:coh1}, with the difference that we will add for generality transmitted and reflected states on both sides of the barrier, as in fig.~\ref{fig:coh2} 

\begin{figure}[h]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/tun2}
 \caption{Schematic of transfer/scattering matrix setup.}
 \label{fig:coh2}
\end{figure}
 
We define a \emph{transfer} and a \emph{scattering matrix} satisfying the following relations

\begin{equation}
\begin{bmatrix} B^{+}  \\ B^{-}  \end{bmatrix} = 
\mattransfer \begin{bmatrix} A^{+}  \\ A^{-} \end{bmatrix}
\end{equation}

\begin{equation}
\begin{bmatrix} A^{-}  \\ B^{+}  \end{bmatrix} = 
\matscattering \begin{bmatrix} A^{+}  \\ B^{-} \end{bmatrix}
\label{eq:coh13}
\end{equation}
 
The transfer matrix is relating wave function components on the left and on the side of a potential barrier. The scattering matrix is relating wave function components travelling in the direction of the barrier and components going in the direction opposite to the barrier \footnote{Disclaimer: the ordering of the rows in the definition of S,M or the definitio of $t,r$ referred as "left to right" or "right to left" tunnelling is  arbitrary, therefore you may find some small notation differences in literature depending on the author.}. 

By comparing the two expression, it is easy to verify that the following relations between the scattering and transfer matrix hold

\begin{eqnarray}
M = 
\begin{bmatrix}
S_{21}-\frac{S_{22}S_{11}}{S_{21}} & \frac{S_{22}}{S_{12}} \\[0.5em]
- \frac{S_{11}}{S_{12}} & \frac{1}{S_{12}} 
\end{bmatrix} \label{eq:coh15} \\
S=
\begin{bmatrix}
-\frac{M_{21}}{M_{22}} & \frac{1}{M_{22}} \\[0.5em]
M_{11}-\frac{M_{12}M_{21}}{M_{22}} & \frac{M_{12}}{M_{22}}
\end{bmatrix}
\end{eqnarray}

These expressions are completely general, when additional constrain are considered they simplify as not all the coefficients in the scattering of transfer matrix are anymore independent.

\subsection{Current conservation}

If we impose current conservation on the system (i.e. we don't have any generation/recombination phenomena) and assume that the velocity of states $\Psi_{A}^{+,-}, \Psi_{B}^{+,-}$ are the same, we must obey the constrain 
 
 \begin{equation}
 v_{A}\lvert A^{+} \rvert ^ {2} - v_{A}\lvert A^{-} \rvert ^2 = v_{B}\lvert B^{+} \rvert ^ {2} - v_{B}\lvert B^{-} \rvert
 \label{eq:coh16}
 \end{equation}

 I would like to avoid to carry velocities all around, therefore I will resort to "velocity normalized" states $\Phi^{+}_{A}=\frac{A^{+}}{\sqrt{v_{A}}}$ by implicitely incorporating the velocity term in the amplitude with the substitution $\frac{A^{+}}{\sqrt{v_{A}}} \rightarrow A^{+}$. Alternatively, we obtain the same result if we work with a modified scattering matrix such that $S'_{ij}=\sqrt{\frac{v_{j}}{v_{i}}}S_{ij}$.
 
With this semplification eq.~\ref{eq:coh16} becomes

 \begin{equation}
 \lvert A^{+} \rvert ^ {2} -\lvert A^{-} \rvert ^2 = \lvert B^{+} \rvert ^ {2} -\lvert B^{-} \rvert
 \label{eq:coh14}
 \end{equation}
 
 
 with some basic manipulation the equation may be rewritten as 
 
 \begin{equation}
 \begin{bmatrix}
 {A}^{+*} & {B}^{-*}
 \end{bmatrix}
 S^{\dagger}S
  \begin{bmatrix}
 A^{+} \\ B^{-}
 \end{bmatrix}=
  \begin{bmatrix}
 {A}^{+*} & {B}^{-*}
 \end{bmatrix}
  \begin{bmatrix}
 A^{+} \\ B^{-}
 \end{bmatrix}
 \end{equation}
 
 which implies the following
 
 \begin{gather}
 S^{\dagger}S = 1 \\
 \lvert \det S \rvert = 1 \nonumber \\
 \lvert S_{11} \rvert ^2 + \lvert S_{12} \rvert ^2 = 1 \nonumber \\
  \lvert S_{21} \rvert ^2 + \lvert S_{22} \rvert ^2 = 1  \nonumber
\end{gather} 

We see that once we get rid of velocities, the scattering operator is \emph{unitary}, as the condition $S^{\dagger}S=I$ must be fulfilled.

Similarly it can be demonstrated (left as an exercise) that the transfer matrix must obey the following

\begin{equation}
M^{\dagger}
\begin{bmatrix}
1 & 0 \\
0 & -1
\end{bmatrix}
M=
\begin{bmatrix}
1 & 0 \\
0 & -1
\end{bmatrix}
\label{eq:coh12}
\end{equation}
 
and

\begin{gather}
\lvert M_{11} \rvert ^{2}-\lvert M_{21}\rvert ^{2}=1 \\
\lvert M_{22} \rvert ^{2}-\lvert M_{12}\rvert ^{2}=1 \nonumber \\
M_{11}^{*}M_{12}-M_{21}^{*}M_{22}=0 \nonumber \\
M_{12}^{*}M_{11}-M_{22}^{*}M_{21}=0
\end{gather}

Note that the scattering matrix is nothing but the matrix  representation of a self-adjoint operator, while the transfer matrix is not. Also remember that we assumed that the velocities must be equal (i.e., the left and right contact must have the same dispersion), if this is not true eq.~\ref{eq:coh13} is not true anymore and the scattering matrix is not self-adjoint. However, this issue can be solved by defining the scattering matrix not in terms of wave amplitudes  but in terms of current amplitudes (i.e. amplitude coefficients only, see ~\cite{Datta1997} pag.121). If defined in this way, the scattering matrix is always self-adjoint.

\subsection{Time reversal symmetry}

When time reversal symmetry holds, if $\Psi$ is a valid solution, then also the anti propagating wave function $\Psi^{*}$ must be a solution. We can rewrite eq.~\ref{eq:coh13} as 

\begin{equation}
\begin{bmatrix} {A}^{+*}  \\ {B}^{-*}  \end{bmatrix} = 
\matscattering \begin{bmatrix} {A}^{-*}  \\ {B}^{+*} \end{bmatrix}
\end{equation}

By comparison with the complex conjugate of  eq.~\ref{eq:coh12} we obtain the following

\begin{gather}
SS^{*}=S^{*}S=1 \\
\lvert S_{11} \rvert ^2 + S_{12}^{*}S_{21}=1 \nonumber \\
\lvert S_{22} \rvert ^2 + S_{21}^{*}S_{12}=1 \nonumber \\
S_{11}^{*}S_{12} + S_{21}^{*}S_{22}=0 \nonumber \\
S_{21}^{*}S_{11} + S_{22}^{*}S_{21}=0
\end{gather}

If we assume both time reversal symmetry and current conservation, then $S_{12}=S_{21}$ and the scattering matrix is symmetric. Similarly, it can be demonstrated (left as exercise) that the transfer matrix must obey the following relation

\begin{equation}
\begin{bmatrix} 0 & 1 \\
1 & 0
\end{bmatrix}
M
\begin{bmatrix} 0 & 1 \\
1 & 0
\end{bmatrix}
= M^{*}
\end{equation}

meaning that in system with time reversal symmetry $M$ must have the form

\begin{equation}
M=
\begin{bmatrix}
M_{11} & M_{12} \\
M_{12}^{*} & M_{11}^{*}
\end{bmatrix}
\end{equation}

therefore $\Tr [M]$ is a real number and (compare eq~\ref{eq:coh15}) $\det [M] = 1$.


\section{Composition rules}

The transfer and scattering matrix can be written in terms of reflectance and transmittance.
If we call $t,r$ the transmittance and reflectance on the left side of the barrier and $t',r'$ the transmittance and reflectance on the right side of the barrier, the scattering matrix can be written as

\begin{equation}
S=
\begin{bmatrix}
r & t' \\
t & r'
\end{bmatrix}
\end{equation}

In time reversal symmetry we can apply the relation $S_{11}^{*}S_{12}+S_{21}^{*}S_{22}=0$ to derive the expression $r^{*}/t'^{*}=-r'/t$. Substituting in eq.~\ref{eq:coh15} $M_{11}=t-\frac{r'r}{t'}=\frac{1}{t*}$ we obtain the expression for the transfer matrix

\begin{equation}
M=
\begin{bmatrix}
\frac{1}{t^{*}} & \frac{r'}{t} \\[0.5em]
-\frac{r}{t'} & \frac{1}{t'}
\end{bmatrix}
\end{equation}

By the definition of the transfer matrix, it is straightforward to notice that if we consider $N$ regions and define for each of them a transfer matrix $M_{i}$, then the total transfer matrix is simply given by the product of the constituent transfer matrices, ordered from last to first

\begin{equation}
M_{tot}= M_{N}M_{N-1}...M_{0}
\end{equation}

The total transmittance is the given by $1/t'=\left[ M_{tot} \right]_{22}$ \footnote{We can choose $t$ or $t'$ without distinction, as at the end we are interested in the coefficient $tt*$}. Clearly, this is the powerful property of the transfer matrix: once we know how to calculate the transfer matrix for some basic structure, we can represent more complex systems by simple matrix products. 

Consider a double barrier as depicted in fig~\ref{fig:coh9}, where $M_{A}$ and $M_{B}$ are the transfer matrix associated with the first barrier and the second barrier. \footnote{an additional transfer matrix for the free layer in between is also needed, as we'll see shortly.}

\begin{figure}[h]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/double}
 \caption{Schematic of a double barrier.}
 \label{fig:coh9}
\end{figure}
 
 The total transmission is simply given by 
 
 \begin{equation}
 t_{AB}=\left[ M_{B}M_{A} \right]_{22}^{-1}=t_{A}\left[1-r'_{A}r_{B} \right]^{-1}t_{B}
 \end{equation}
 
 The serier expansion of this expression ($1-r'_{A}r_{B}$ is smaller than one) is
 
 \begin{equation}
 t_{AB}=t_{A}t_{B}+t_{A}r'_{A}r_{B}t_{B}+t_{A}r'_{A}r_{B}r'_{A}r_{B}t_{B}+ \cdots
 \end{equation}
 
The physical meaning of this expression is clear. $\Psi_{C}^{+}$ is the sum of the contribution of multiple reflection paths in between the two barriers. 
 
If we represent $r'_{A},r_{B}$ as $|r'_{A}|e^{j\theta_{A}}, |r_{B}|e^{j\theta_{B}}$, the associated transmission is given by  

\begin{equation}
T_{AB}=t_{AB}t_{AB}^{*}=\frac{T_{A}T_{B}}{1-2\sqrt{R_{A}R_{B}}\cos \theta + R_{A}R_{B}}
\label{eq:coh15b}
\end{equation}

where $\theta=\theta_{A}+\theta_{B}$ is the total phase acquired during a double reflection. 
This result is only valid whenever phase coherency is preserved during the tunnelling process. If this is not the case, the transmittance as we defined it is meaningless and we need to rather combine directly the particle (and not the wavefunction) transmission and reflection.

By applying similar arguments, the total transmission is then given by the sum of all the possible \emph{classical} multiple reflection paths (see also \cite{Datta1997}):

\begin{align}
T_{AB} & = T_{A}T_{B}+T_{A}R_{A}R_{B}T_{B} + T_{A}R_{A}R_{B}R_{A}R_{B}T_{B} + \cdots = \\
& = \frac{T_{A}T_{B}}{1-R_{1}R_{2}} \nonumber
\end{align}

The two expressions are clearly different, the first one represent a purely coherent quantum process, while the second one represents a \emph{sequential tunneling} process, i.e. the incoming particle can tunnel through a classically forbidden energy barrier, but then loses phase coherence and may tunnel through the second barrier independently from the first process. In this transport regime, which occurs when the phase relaxation time is shorter than the tunnelling time, ohmic behaviour is recovered. In fact, we can write

\begin{equation}
\frac{1-T_{AB}}{T_{AB}}=\frac{1-T_{A}}{T_{A}}+\frac{1-T_{B}}{T_{B}}
\end{equation}

which can just be read as: the 4 probe resistance due to $N$ scatterer is the sum of the $N$ 4 probe resistances of every single scatterer, when phase coherence is not preserved between scattering events. 

This definition can also be used to derive an expression of mean free path in terms of transmission coefficient. In a system with $N$ identical scatterers the total transmission obey the relation

\begin{equation}
\frac{1-T(N)}{T(N)}=N \frac{1-T}{T}
\end{equation}

If we define the linear scatterer density $n_{i}$, then $N=n_{i}L$ where $L$ is the length of the sample. The transmission can be then be written as

\begin{equation}
T(L)=\frac{T}{n_{i}L(1-T)+T}=\frac{L_{m}}{L+L_{m}}
\end{equation}

where $L_{m}=\frac{T}{(1-T)n_{i}}$. It easy to verify that $L_{m}$ satisfy the relation

\begin{equation}
R=\frac{1}{G_{0}}=R_{0}+R{0}\frac{L}{L_{m}}
\end{equation}

This means that the conductance of the sample depends linearly on $L_{m}$, as it depends linearly on the mean free path. Moreover, the mean free path is defined (in 1D systems) as the average distance a carrier can travel between back-scattering events. The back-scattering probability is the reflection coefficient and $l_{m}\approx \frac{1}{n_{i}R}$. If the transmission is close to one (weak scattering), then 

\begin{equation}
L_{m}\approx \frac{1}{n_{i}(1-T)}\approx L_{m}
\end{equation}

It follows that the transmission coefficient can be also used to evaluate the mean free path related to some specific scatterer. You can see an application example of such concept for characterization of silicon nanowires in ref.~\cite{Markussen2006}.


\section{Breit-Wigner formula}

The transmission in a double barrier exhibit another fundamental difference whether we consider or not quantum coherence: the phenomena of \emph{resonant tunnelling}. In the equation ~\ref{eq:coh15b} we didn't derive an explicit formula for the phase term $\theta$. In general, the accumulated phase is a function of energy itself. I consider two small barriers, i.e. I will assume that the transmission across a single barrier is small enough to have that $R_{A}R_{B}\approx 1$. By means of Taylor expansion of the terms $R_{A}R_{B}$ around $1$ we may write the expression as

\begin{equation}
T=\frac{T_{A}T_{B}}{\left[ 1 - \sqrt{R_{A}R_{B}} \right]^{2}+2\sqrt{R_{A}R_{B}}\left(1-\cos \theta(\varepsilon) \right)}\approx \frac{T_{A}T_{B}}{\left[\frac{T_{A}+T_{B}}{2} \right]^2 + 2\left(1-\cos \theta(\varepsilon) \right)}
\label{eq:coh17}
\end{equation} 

We can notice that even when $T_{A}, T_{B}$ are close to $0$, the transmission as described in  eq.~\ref{eq:coh17} may be close to $1$ when the cumulated phase is a integer multiple of $2\pi$. When such condition is fulfilled, we call is \emph{resonant tunneling}. Physically, this happens at the energies of quasi-bound states in the double barrier. We can expand the trigonometric term close to resonance energies $\varepsilon_{n}$ (i.e., around $\theta=2\pi n$)

\begin{equation}
1- \cos \theta \approx \frac{\theta(\varepsilon)^2}{2}
\end{equation}

We assume also the linear expansion for $\theta$ (we said that $\theta(\varepsilon _{n})=0$) around $\varepsilon _{n}$



\begin{equation}
\theta (\varepsilon) \approx \od{\theta}{\varepsilon} (\varepsilon - \varepsilon_{n})
\end{equation}

The total transmission can be then written as

\begin{equation}
T\approx \frac{\Gamma_{A}\Gamma_{B}}{(\varepsilon - \varepsilon_{n})^{2} + \left(\frac{\Gamma_{A}+\Gamma_{B}}{2} \right)^{2}}
\end{equation}

where 

\begin{equation}
\Gamma_{A,B}=\od{\varepsilon}{\theta}T_{A,B}
\end{equation}

This expression can be written in a more common form

\begin{equation}
T_{E}=\frac{\Gamma_{A}\Gamma_{B}}{\Gamma_{A}+\Gamma_{B}}\frac{\Gamma}{(\varepsilon-\varepsilon_{n})^2+\left( \frac{\Gamma}{2} \right)^{2}}
\end{equation}

with $\Gamma=\Gamma_{A}+\Gamma_{B}$. The second term is a Cauchy (or Lorentzian) distribution, and this formula is called \emph{Breit-Wigner formula}, and it is often used to model in a simple way resonant tunneling or to extract physical parameters ($\Gamma, \varepsilon_{n}$) from experimental measurements. 


\section{Discontinuity transfer matrix}

Based on the composition rules, we can apply the transfer matrix as a method to solve the single particle Schr\"{o}dinger equation with any given local potential by approximation the potential with a sum of known systems with known transfer matrix. The simplest approximation is the stepwise approximation, based on the transfer matrix for the discontinuity potential and constant potential. Additionally, I show the transfer matrix for the single and double delta potential.


\subsection{Discontinuity transfer matrix}

Thanks to the composition rules, the transfer matrix is a powerful tool to calculate the solution of systems with an arbitrary external potential. A very limited set of building blocks can be used to represent a wide number of systems. In the following paragraph we will calculate the transfer matrix for a potential step, a constant potential and a delta potential. In the following I will follow closely the derivation shown by Walker and Gathright \cite{Walker1993}.
We include for sake of generality an arbitrary energy reference $V_{0}$ and define the wave vector as 

\begin{equation}
k = \frac{\sqrt{2m(\varepsilon - V_{0})}}{\hbar }=\frac{\sqrt{2mV_{0}}}{\hbar }\sqrt{\tilde{\varepsilon}-1}
\end{equation}

where $\tilde{\varepsilon=\varepsilon / V_{0}}$ is a dimensionless parameter.

We consider first a potential discontinuity of the form $V(x)=V_{A}+\theta(x-x_{0})(V_{B}-V_{A})$ where $\theta(x-x_{})$ is the Heaviside step function. The boundary conditions must ensure continuity of the wave function and of the first derivative of the wave function \footnote{This is true for any finite potential}. As in the previous sections, we consider a plane wave $\Psi_{A}=A^{+}e^{\jmath k_{A}x}+A^{-}e^{-\jmath k_{A}x}$ on the left of discontinuity and $\Psi_{B}=B^{+}e^{\jmath k_{B}x}+B^{-}e^{-\jmath k_{B}x}$ on the right. We define

\begin{equation}
k_{A,B}=\frac{\sqrt{2mV_{0}}}{\hbar}\sqrt{\tilde{\varepsilon} - \tilde{V}_{A,B}}
\end{equation}

where $\tilde{V}_{A,B}=V_{A,B}/V_{0}$ are two dimensionless parameters which fully define the discontinuity. The boundary conditions can then be written as

\begin{align}
A^{+}+A^{-}&=B^{+}+B^{-} \\
\jmath k_{A}(A^{+}-A^{-})&=\jmath k_{B}(B^{+}-B^{-})
\end{align}

The two expression can be easily written in a matrix form

\begin{equation}
\begin{bmatrix}
B^{+} \\
B^{-}
\end{bmatrix} =
\frac{1}{2}
\begin{bmatrix}
1+k_{A}/k_{B} & 1-k_{A}/k_{B} \\
1-k_{A}/k_{B} & 1+k_{A}/k_{B}
\end{bmatrix} 
\begin{bmatrix}
A^{+} \\ A^{-}
\end{bmatrix} =
\frac{1}{2}D(\tilde{V}_{A}, \tilde{V}_{B})\begin{bmatrix}
A^{+} \\ A^{-}
\end{bmatrix}
\end{equation}

\subsection{Constant Potential}

The constant potential region is simply a region of space of length $L=la_{0}$, with $l$ dimensionless and $a_{0}$ a reference unit length, and potential $\tilde{V}=V/V_{0}$.
The associated transfer matrix is simply given by 

\begin{equation}
P(\tilde{V}, L)=
\begin{bmatrix}
e^{\jmath kL} & 0 \\
0 & e^{-\jmath kL} \\
\end{bmatrix}
\end{equation}

Quite intuitively, the propagation in a region without any potential discontinuity is only affecting the complex phase. 

\subsection{Delta potential}

For a delta potential of the form $V(x)=\alpha \delta(x-x_{0})$ the first derivative of the wavefunction \footnote{You can demonstrate it by integrating the Schr\"{o}dinger equation from $-\epsilon$ to $+\epsilon$ and considering the limit $\epsilon \rightarrow 0$} has a discontinuity 

\begin{equation}
\od{\Psi(x)}{x} \rvert _{\epsilon^{+}} - \od{\Psi(x)}{x} \rvert _{\epsilon^{-}}=\frac{2m\alpha}{\hbar^{2}}\Psi(x_{0})
\end{equation}

Therefore the boundary conditions on the plane wave amplitude are

\begin{align}
A^{+}+A^{-}&=B^{+}+B^{-} \\
\jmath k_{A}(A^{+}-A^{-}) - \jmath k_{B}(B^{+}-B^{-})&=\frac{2m\alpha}{\hbar^{2}}(A^{+}-A^{-})
\end{align}

We assume the same potential $\tilde{V}=V/V_{0}$ on the left and right side of the barrier and define a parameter $\gamma$ which depends on $\tilde{V}$ and $\alpha$

\begin{equation}
\gamma = \frac{\alpha \sqrt{2mV_{0}}}{2 \hbar V_{0} \sqrt{\tilde{\varepsilon} - \tilde{V}}}
\end{equation}

th associated transfer matrix is 

\begin{equation}
del(\tilde{V}, \alpha)= 
\begin{bmatrix}
1-\jmath \gamma & - \jmath \gamma \\
\jmath \gamma & 1+ \jmath \gamma
\end{bmatrix}
\end{equation}

the corresponding transmission coefficient is 

\begin{equation}
T=
\left( 1+\gamma^{2} \right) ^{-1}=
\frac{\hbar ^{2}4(\varepsilon-V)}{\hbar ^{2}4(\varepsilon-V)+2m\alpha^{2}} = 
\frac{1}{1+m\alpha^{2}/2\hbar^{2}(\varepsilon-V)}
\end{equation}


\subsection{Rectangular barrier}

The discontinuity, constant and delta potential are the basic building blocks we need to calculate the transfer matrices of more complicated systems. To show how the composition works, we calculate a well known result, the single rectangular barrier. We consider a potential $V_{A}$ on the left and right side and a barrier with potential $V_{B}$ and length $L$. The total transfer matrix is given by

\begin{equation}
M=D(\tilde{V}_{B}, \tilde{V}_{A})P(\tilde{V}_{B}, l)D(\tilde{V}_{A}, \tilde{V}_{B})
\end{equation} 

By applying the formulas derived in the previous section, we find

\begin{equation}
M = \frac{1}{4}
\begin{bmatrix}
1+k_{B}/k_{A} & 1-k_{B}/k_{A} \\
1-k_{B}/k_{A} & 1+k_{B}/k_{A}
\end{bmatrix}
\begin{bmatrix}
e^{\jmath k_{B}L} & 0 \\
0 & e^{-\jmath k_{B}L} \\
\end{bmatrix}
\begin{bmatrix}
1+k_{A}/k_{B} & 1-k_{A}/k_{B} \\
1-k_{A}/k_{B} & 1+k_{A}/k_{B}
\end{bmatrix}
\end{equation}

In order to calculate the transmission we only need the matrix element $M_{22}$. After some algebra we get the result

\begin{equation}
M_{22}=\cos k_{B}L-\jmath \left(\frac{k_{B}^2 + k_{A}^2}{2k_{A}k_{B}} \sin k_{B}L \right)
\end{equation} 

The corresponding transmission coefficient is the well known expression

\begin{equation}
T=\frac{1}{|M_{22}|^{2}}= \left[ 1 + \left(\frac{k_{B}^2-k_{A}^2}{2k_{A}k_{B}}\right)^2 \sin ^2 k_{B}L  \right] ^{-1}
\end{equation}

Note that unit transmission is only possible when $k_{B}L=2\pi n$. These transmission peaks are called \emph{Ramsauer peaks}

\begin{ExerciseList}
\Exercise
Demonstrate that you can obtain unit transmission only if the potential on the left and right side of the barrier is the same.
\end{ExerciseList}


\subsection{Double delta barrier}

The double barrier has in general a more complicated analytical form. However, in the case of a double delta barrier, the solution can still be written in a meaningful form. We consider two identical barrier separated by a distance $L$. The potential is assumed to be the same all over the space, except for the barrier themselves. The transfer matrix is built as

\begin{equation}
M = del(\alpha, \tilde{V})P(\tilde{V}, L)del(\alpha, \tilde{V})
\end{equation}

The associated transfer matrix is 

\begin{equation}
M=\begin{bmatrix}
1-\jmath \gamma & - \jmath \gamma \\
\jmath \gamma & 1+ \jmath \gamma
\end{bmatrix}
\begin{bmatrix}
e^{\jmath kL} & 0 \\
0 & e^{-\jmath kL} \\
\end{bmatrix}
\begin{bmatrix}
1-\jmath \gamma & - \jmath \gamma \\
\jmath \gamma & 1+ \jmath \gamma
\end{bmatrix}
\end{equation}

The resulting transmission coefficient is 

\begin{equation}
T = \frac{1}{\gamma^{4}+(1+\gamma^{2})^2 + 2 \gamma^{2}(1-\gamma^{2}) \cos 2kL + 4\gamma^{3}\sin 2kL}
\end{equation}

Note that the above expression can be written in the form used for eq.~\ref{eq:coh15} where $T=\frac{1}{(1+\gamma^2)}$ is the single barrier transmission and $R=\frac{\gamma^2}{(1+\gamma)^2}$ the single barrier reflection. The trigonometric term has a complicated expression, as includes both the contribution $\theta=2kL$ due to propagation between the barrier and additional terms due to finite phase terms in the transmittance and reflectance.

\begin{ExerciseList}
\Exercise
Verify that in symmetric a double delta barrier the transmission is one when the resonance condition $\tan(kL)=-1/\gamma$ is verified (hint: use the tangent half angle formula).
\end{ExerciseList}


\subsection{N identical barriers}

For any given shape of the transmission barrier, it is possible to derive a closed expression for the transmission along $N$ identical barrier once the transfer matrix for a single one is known. The demonstration is based on the \emph{Chebyshev identity}.


As previously demonstrated, current conservation and TRS we must obey the condition $\text{det}[M]=1$ and $\Tr [M]$ must be a real number.
The determinant can be written as $\lambda_{1}\lambda_{2}=1$ where $\lambda_{1,2}$ are the eigenvalues of the transfer matrix. This condition is respected in two cases

\begin{eqnarray}
\lambda_{1} = e^{\jmath kx} \; \lambda_{2}=e^{-\jmath kx}
\\
\lambda_{1} = e^{\beta x} \; \lambda_{2}=e^{-\beta x}
\end{eqnarray} 

with $k, \beta \in \mathbf{R}$. The two solution correspond respectively to propagating states (complex eigenvalues) and evanescent states (real eigenvalues). It is left as an exercise that we can equivalently state that $\Tr [M] \leq 2$ corresponds to propagating states and $\Tr [M] > 2$ correspond to evanescent states.

If we imagine to begin with a representation where the transfer matrix is diagonal, the physical meaning of imaginary and real eigenvalues is clear. When the eigenvalues are imaginary the wave function can  propagate in the systems as a plane wave. On the other hand, real eigenvalues correspond to an evanescent exponentially decaying mode. A divergent solution corresponding to an exponentially diverging state exists as well, in order to preserve the current conservation condition. In other words, the transfer matrix would not be well defined without imposing the diverging state. Note that it is questionable to think in term of current conservation, as evanescent states do not carry current. In the next chapter we will focus on the physics associated with this kind of solution. Now, we will rather consider propagating states. 

We consider a generic transfer matrix of a barrier

\begin{equation}
M = \begin{bmatrix}
a & b \\ c & d
\end{bmatrix}
\end{equation}

and we assume that the eigenvalues are complex in the form $\lambda_{1,2}=e^{\pm \jmath kx}$. Then it can be demonstrated that the transfer matrix $M^{N}$ corresponding to $N$ barriers can be written as

\begin{equation}
M^{N}=
\begin{bmatrix}
aU_{N-1}-U_{N-2} & bU_{N-1} \\
cU_{N-1} & dU_{N-1}-U_{N-2}
\end{bmatrix}
\end{equation}

where 

\begin{equation}
U_{N}=\frac{\sin (N+1)kx}{\sin kx}
\end{equation}

is the $Nth$ coefficient of a Chebyshev polynomial of the second kind. The statement can be demonstrated by induction. The case $N=1$ is trivial. Then we demonstrate that if the thesis holds for $N$, then it holds also for $N+1$.
Therefore we consider

\begin{equation}
M^{N+1}=MM^{N}
\end{equation}

we will demonstrate the theorem for the matrix element $[M]_{11}$, the other elements can be derived similarly. We have

\begin{equation}
[M^{N+1}]_{11} = (a^{2}+bc)U_{N-1}-aU_{N-2}
\end{equation}

By invoking the conditions on determinant and trace, and remembering that determinant and trace are invariant with respect to linear transformations we have

\begin{align*}
a+d &= 2\cos kx \nonumber \\ 
ad-bc &=1
\end{align*}

By using these relations and deriving that $\sin(N+1)kx + \sin (N-1)kx = 2\sin (N)kx\cos kx$ we can easily demonstrate that $[M^{N+1}]_{11}=aU_{N}-U_{N-1}$.

The total transmission across the $N$ barrier can then be expressed as 

\begin{equation}
T_{N} = \frac{1}{1 + \frac{R}{T} \frac{\sin ^{2}Nkl}{\sin ^{2}kl} }
\end{equation}

where $R,T$ are the single barrier transmission and reflection coefficient and $l$ is the distance between barriers. In  \cref{fig:coh5,fig:coh7,fig:coh9b} some examples are calculated and shown. All the scripts used to generate the figures can be found in the git repository. It is important to note what happens when the barrier are several, i.e. when $N \rightarrow \infty$. More and more transmitting states will be then allowed in some specific energy window within the barrier. When the barriers are in infinite number, electronic bands of propagating states are generated, which is exactly what happens in a regular lattice in Bloch states theory, just from a different point of view.


\begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/doublebarrier}
 \caption{Transmission along single and double delta barrier.}
 \label{fig:coh5}
 \end{figure}

\begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/doublebarrier_rect}
 \caption{Transmission along single and double rectangular barrier.}
 \label{fig:coh7}
 \end{figure}

\begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/doublebarrier_rectn}
 \caption{Transmission along many rectangular barriers. Note energy gaps arising at $1$ and $2$ eV}
 \label{fig:coh9b}
 \end{figure}

\section{Tight binding and plane wave representation}

In the previous sections we defined the transfer and scattering matrix in terms of plane wave amplitudes. In the following paragraph which properties do not depend on a specific representation and how we may apply the proper transformation to change representation.

If we start from the definition of probability current density associated to plane wave functions $\Psi_{A(B)}= A^{+}(B^{+})e^{\jmath k_{A(B)}x}+A^{-}(B^{-})e^{-\jmath k_{A(B)}x} $, we defined the transmittance using the squared probability amplitudes

\begin{equation}
t = \frac{\sqrt{v_{B}}B^{+}}{\sqrt{v_{A}}A^{+}}
\end{equation}

the advantage of this definition is that the scattering matrix is well defined as a unitary operator, as the current conservation (in presence of TRS) is well defined by construction. In most cases $v_{A}=v_{B}$ because the typical nanostructures used in device applications have identical leads, and we can define the transmittance using the plane wave function amplitudes

\begin{equation}
t = \frac{B^{+}}{A^{+}}
\label{eq:coh19}
\end{equation}

This is probably the definition which is more often used in literature. Similarly, we can consider directly the ration between the forward and backward propagating wave function components

\begin{equation}
t = \frac{\Psi_{B}^{+}}{\Psi_{A}^{+}}
\label{eq:coh21}
\end{equation}

The definitions in \cref{eq:coh19,eq:coh21} are equivalent apart a phase factor defined in a linear transformation by noting that the wave function can be written as

\begin{equation}
\begin{bmatrix}
\Psi_{A}^{+} \\ \Psi_{A}^{-}
\end{bmatrix} = 
\begin{bmatrix}
e^{\jmath k_{A}x} & 0 \\ 0 & e^{-\jmath k_{A}x}
\end{bmatrix}
\begin{bmatrix}
A^{+} \\ A^{-}
\end{bmatrix}
\end{equation}

Therefore the different representation are equivalent under certain linear transformation. It is important to note that some properties of the transfer matrix do not depend on the representation. By invoking determinant and trace invariance with respect to linear transformations, current conservation and time reversal symmetry always imply $\text{det}[M]=1$ and $\Tr [M] \in \mathbf{R}$.

We now demonstrate that this is still true if we work in a tight binding (TB) representation, and in this case the transfer matrix can be written very easily. In a TB model the total wave function is represented as a linear combination of localized basis functions $\varphi_{i}(\rvec) = \Braket{\rvec|i}$

\begin{equation}
\Ket{\Psi} = \sum_{i}c_{i}\Ket{i}
\end{equation}

If many body interactions can be neglected, the Hamiltonian is written in the form

\begin{equation}
H =\sum_{i} \varepsilon_{i} \Ket{i}\Bra{i} + \sum_{i\neq j}t_{ij}\Ket{j}\Bra{i}
\end{equation}

If we consider a one dimensional chain with nearest neighbour coupling, the single particle time independent Schr\"{o}dinger equation can be conveniently written in the matrix form

\begin{equation}
\begin{bmatrix}
\ddots \\
0 & t_{n-2} & \varepsilon_{n-1} - \varepsilon & t_{n-1}^{*} & 0 & \cdots \\
\cdots & 0 & t_{n-1} & \varepsilon_{n} - \varepsilon & t_{n}^{*} & 0 & \cdots \\
\cdots & \cdots & 0 & t_{n} & \varepsilon_{n+1} - \varepsilon & t_{n+1}^{*} & 0 \\
 & & & & & & \ddots \\
\end{bmatrix}
\begin{bmatrix}
\vdots \\
c_{n-1} \\
c_{n} \\
c_{n+1} \\
\vdots
\end{bmatrix} = 0
\label{eq:coh23}
\end{equation}
   
It can be easily shown that we the real space representation of the Schr\"{o}dinger equation has the same identical form if we expand the differential operator with a finite difference methods, i.e. the effective mass discrete representation and the nearest neighbour TB representation are identical. A nice tratment of this equivalence can be found in the work of G. Klimeck and Boykin \cite{Boykin2004}. 
The equation ~\ref{eq:coh23} can be equivalently written as

\begin{equation}
\begin{bmatrix}
c_{n+1} \\ c_{n}
\end{bmatrix}
= \mathcal{M}
\begin{bmatrix}
c_{n} \\ c_{n-1}
\end{bmatrix}
\end{equation}

with

\begin{equation}
\mathcal{M}=
\begin{bmatrix}
(\varepsilon - \varepsilon_{n})/t^{*}_{n} & -t_{n-1}/t^{*}_{n} \\
1 & 0 
\end{bmatrix}
\end{equation}

In a simple homogeneous chain with onsite energy $\varepsilon_{n}=0$ and $t=t^{*}=1$ the matrix reduces to

\begin{equation}
\mathcal{M}=
\begin{bmatrix}
\varepsilon & -1 \\
1 & 0 
\end{bmatrix}
\end{equation}

If we consider the basis function $\Ket{i=n}$ to be located in the position $n a$ where $a$ is the spacing between the basis, the coefficients $c_{n}$ are related to the plane wave expression as 

\begin{equation}
\begin{bmatrix}
c_{n} \\ c_{n-1}
\end{bmatrix} =
\begin{bmatrix}
1 & 1 \\
e^{-\jmath k_{A}a } & e^{\jmath k_{A}a } \\
\end{bmatrix} 
\begin{bmatrix}
\Psi_{A}^{+}(na) \\ \Psi_{A}^{-}(na) 
\end{bmatrix} = Q\begin{bmatrix}
\Psi_{A}^{+}(na) \\ \Psi_{A}^{-}(na) 
\end{bmatrix}
\end{equation}

whre 

\begin{equation}
\begin{bmatrix}
\Psi_{A}^{+}(na) \\ \Psi_{A}^{-}(na) 
\end{bmatrix} =
\begin{bmatrix}
A^{+}(na)e^{\jmath k_{A}na} \\
A^{-}(na)e^{ - \jmath k_{A}na}
\end{bmatrix}
\end{equation}

Note that we are assuming that also the amplitude can be a function of position. The reason is that in the following it will be more convenient to drop the notation $A,B$ we used to distinguish a wave function on the left or on the right of a barrier but we rather explicitly to the position where the wave function is evaluated. 
Having defined the linear transformation $Q$ it easy to demonstrate (left as an exercise) that the transfer matrix in plane wave and TB representation are related by

\begin{equation}
\begin{bmatrix}
\Psi_{A}^{+}((n+1)a) \\ \Psi_{A}^{-}((n+1)a) 
\end{bmatrix}=Q^{-1}\mathcal{M}Q
\begin{bmatrix}
\Psi_{A}^{+}(na) \\ \Psi_{A}^{-}(na) 
\end{bmatrix}
\end{equation} 

As for the plane waves case, the TB transfer matrix for $n$ sites can be written by composition rules simply as 

\begin{equation}
\mathcal{M}_{n} = \prod _{i=0}^{n} \mathcal{M}_{i}
\end{equation}

This representation can be very advantageous because it can be written directly once the Hamiltonian, possibly including a local potential term, can be written in matrix form. In the next chapter, we will see that this will be extremely useful in the analysis of the Anderson disorder model.
