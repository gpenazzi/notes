\chapter{Ballistic transport}

In the previous chapter we introduced a number of approximation allowing for a semiclassical description of transport in the diffusive limit. We recall them briefly:

\begin{itemize}

\item A particle can be characterized in the phase space by its momentum and position (i.e., we can write a classical distribution function).

\item Particles are described semi-classically, i.e. they obey classical dynamics but their momentum and energy are described by a Bloch state dispersion relation.

\item The non equilibrium perturbation is small and can be characterized by a single characteristic time (relaxation time approximation).

\item Scattering events occur rarely, i.e. we can wait a time long enough between two events to assume that they will be uncorrelated.

\end{itemize}

Additionally we considered only elastic scattering events for simplicity and we could derive the generalized Ohm law and define the Drude conductance $\sigma$. 

Referring to characteristic length scales, this description can hold in confined systems, like bulk 2D or 1D materials, as we already saw that we can account for confinement correctly in the density of states, i.e. we have to take care of the correct dimensionality of the reciprocal space and possible sub-bands but the math does not change. We are implicitly assuming that the phase relaxation length is short because we are neglecting any phase coherent effect between two scattering events (uncorrelated scattering); we will relax this constrain in the next chapter. Furthermore, we are assuming that the characteristic length of our system is large compared to the mean free path, i.e. we assume that \emph{we will have scattering}, regardless the mean free path.

Within these assumption we can write the conductivity of a system as 

\begin{equation}
G=\frac{A}{L}\sigma
\label{eq:lan1}
\end{equation}

where $\sigma$, the conductance, is the bulk material property defined in an arbitrary renormalization volume and $A,L$ describe the surface and length of the system. In low dimensional systems, the expression above may still hold but obviously needs to be slightly modified in order to account for the lower dimensionality. The conductivity can be written in a generalized form

\begin{equation}
G=\frac{L^{d-1}}{L}\sigma = L^{d-2}\sigma
\label{eq:lan2}
\end{equation}

where the expression now refers to the edge of a generic hypercube in $d$ dimensions.

Recalling the expression for the conductivity $\sigma=nq\mu$, we would have a divergent conductivity in a system with no scattering ($\tau_m \rightarrow \infty$). Or, equivalently, if $L \rightarrow 0$  the conductivity diverges as well. In other words, if the system dimensionality is small compared to the characteristic lengths implicitly included in $\sigma$ as the mean free path, the conductivity will diverge. In the next chapter we will drop only one assumption, i.e. we consider a system shorter than the mean free path, where no or few scattering events will occur and we will question whether the conductivity will really diverge. 

The ideas here explained are the very basis of the so called Landauer-B\"uttiker theory.




\section{Heuristic derivation of Landauer formula}

Consider a one-dimensional or a quasi one-dimensional (i.e. a 1D system with a finite number of transverse modes) system shorter with length $L \ll l_{m}$ where $l_{m}$ is the elastic mean free path, connected to large metallic electrodes. We assume the contacts to act as an external bath, i.e. each contact is in thermodynamic equilibrium. The temperature and the chemical potential of the two contacts may differ and drive the central region out of equilibrium. A central region, called scattering region, is connected to the electrodes through a left and right lead. Even though we use different names for convenience, the leads and the scattering region could belong to the same material (for example, a nanowire). 

\begin{figure}[h]
\centering
 \includegraphics[width=0.5\textwidth,keepaspectratio]{graph/landauer1}
  \includegraphics[width=0.3\textwidth,keepaspectratio]{graph/qpcontact}
 \caption{Schematic picture of a quasi-ballistic system in the Landauer-B\"{u}ttiker picture (left) and an example of quantum point contact (right).}
 \label{fig:lan1}
 \end{figure}
 
Such an idealized picture can corresponds to very different experimental setups. Historically, the first realization of this setup has been realized by electrostatic gating of a 2DEG, as shown in fig~/ref{fig:lan1}. We will refer to this setup as Landauer-B\"uttiker system. We define the following physical hypothesis:  

\begin{itemize}
\item The contacts are locally in thermodynamic equilibrium, i.e. the charge distribution in each contact is well described by a Fermi-Dirac distribution. 
\item The interface between the contact and the leads is reflectionless, i.e. any electron can be transmitted from a lead to the corresponding contact with probability $1$. 
\item The lead are ballistic, i.e. scattering events may only occur in the scattering region. 
\end{itemize} 

The second assumption ensure that all the right-traveling electrons must be injected by the left contact and all the left-traveling electrons must be injected by the right contact. The third assumption, combined with the first, ensure that thermal relaxation only occurs at the contact. When a charge is traveling from a contact to the other, it will relax to the thermal equilibrium via inelastic scattering in the contact itself. This also mean that irreversibility is given by the contacts. 

Given this assumptions, we will now answer in a simple way the question: how much is the resistance if the scattering region is also ballistic, or, in other words, there are no possible scattering events?

We consider a single band 1D system oriented along the $z$ direction, all the electrons therefore may be in a state $k_{z}^{+}$ or $k_{z}^{+}$, corresponding to positive or negative values of the momentum or, equivalently, to left and right traveling electrons. As we consider a single band, there is no degeneracy and the $k_{z}$ is a good quantum number (we will initially neglect the spin number).

The current going from the left electrode to the right electrode is then given by 

\begin{equation}
I_{lr}= -e \int \dif \varepsilon v(\varepsilon) g(\varepsilon) f_{l}(\varepsilon, \mu_{l}) 
\label{eq:lan3}
\end{equation}

where $f_{l}$ is the distribution function in the left function, which we assumed to be a Fermi-Dirac. The density of states in energy depends on the specific system, therefore is convenient to use the density of states in reciprocal space with the identity $\od{n}{\varepsilon}=\od{n}{k}\od{k}{\varepsilon}\dif \varepsilon=\frac{1}{2\pi \hbar v}\dif \varepsilon$. Therefore eq.~\ref{eq:lan3} reduces simply to

\begin{equation}
I_{lr}= -e \int \dif \varepsilon v(\varepsilon) \frac{1}{2 \pi \hbar v} f_{l}(\varepsilon, \mu_{l}) = -\frac{e}{h} \int d\varepsilon f_{l}(\varepsilon, \mu_{l})
\end{equation}

The total current is given by the difference of the current flowing from left to right and the current flowing from right to left.

\begin{equation}
I=I_{lr}-I_{rl}=-\frac{e}{h} \int (f_{l}(\varepsilon, \mu_{l}) - f_{r}(\varepsilon, \mu_{r})) \dif \varepsilon
\end{equation}

When an infinitesimal bias is applied to the contacts, this will shift the chemical potential by an amount $\delta \mu = -e \delta V$, and we will have $\mu_{l}=\mu+\delta, \mu_{r}=\mu $ where $\mu$ is an initial equilibrium chemical potential. Therefore the differential resistance is given by

\begin{equation}
G=\lim_{V\rightarrow 0}\frac{\delta I}{\delta V}=\frac{e^{2}}{h}\int \pd{f(\varepsilon, \mu)}{\varepsilon} \dif \varepsilon
\label{eq:lan5}
\end{equation}
 
We already encountered the derivative of the Fermi-dirac and saw that in the low temperature limit it corresponds to a Dirac delta function. If we consider the limit $T\rightarrow 0$ and include the spin degeneracy, we obtain the expression of the \emph{quantum of conductance}

\begin{equation}
G_{0}=\frac{2e^2}{h}\approx \SI{77.48}{ \micro \siemens}
\end{equation}

The equation above states an important fact: \emph{a ballistic low dimensional system in a two terminal configuration can not have a conductance larger than the quantum of conductance $\frac{2e}{h}$.}

The integral in eq.~\ref{eq:lan5} goes to the bottom of the band to infinity and the equation holds for a single band. If we have a system with multiple bands, in particular a quasi-1D system with multiple sub-bands populated, this will only modify the degeneracy index. We can use an Heavyside step-function $\theta(\varepsilon - \varepsilon_{i})$ to count the number of populated sub-bands at a given Fermi energy and write a more general expression for the zero temperature multi-band case

\begin{equation}
G = \frac{2e^2}{h} \sum_{i}\theta(\varepsilon_{F} - \varepsilon_{i})
\end{equation}

The ballistic limit has been experimentally measured for the first tie in the $1988$ by van Wees and collaborators \cite{vanWees1988}, and is today routinely observed in a large number of organic and inorganic systems. The thermal broadening function in eq.~\ref{eq:lan5} tells us that in order to observe the conductance steps, the spacing between sub-bands must be small respect to the temperature. Being the first experiments available only on weakly confined systems, very low temperature were necessary. Nowadays, conductance steps can observed at room temperature in atomically thin systems. 

\begin{figure}[h]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/vanwees}
 \caption{Reprinted from ref.~\cite{vanWees1988}.}
 \label{fig:lan3}
 \end{figure}

Up to now we considered no possible scattering in the scattering region. Let's now include a scattering center such that the electron travelling from a contact to another have a probability $T(E)$ to be transmitted and $R(E)=1-T(E)$ to be reflected. We will define the transmission and reflection coefficients and their properties  more rigorously in the following sections and show that they are indeed well defined quantities.  

In the single band case, the argument we applied are still valid, but the assumption that all the right travelling charges come from the left lead (and viceversa) must be dropped. Rather, all the right travelling electrons in the left lead are populated by the left lead and viceversa. There could be left travelling charges in the left lead populated by the left lead, due to reflection. The situation is depicted in fig.~\ref{fig:lan5}


\begin{figure}[h]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/rynd}
 \caption{Figure from Dmitry Ryndyk lecture notes.}
 \label{fig:lan5}
 \end{figure}

As $T+R=1$, there is no net current flowing below the lowest chemical potential. Above the lowest chemical potential, we have in the left lead a total current $I_{l}=I_{l}^{+}-I_{l}^{-}$, where $I_{l}^{+}$ is the injected states and $I_{l}^{-}$ the reflected states. In the right lead the current is $I_{r}^{+}$ (we consider only states above the chemical potential, therefore there is no injected states from right contact). The following relations hold:

\begin{equation}
I_{l}^{+} = -\frac{2e}{h}\int (f_{l}-f_{r})\dif \varepsilon \\
I_{l}^{-} = -\frac{2e}{h}\int (1-T(E))(f_{l}-f_{r})\dif \varepsilon \\
I_{r}^{+} = -\frac{2e}{h}\int T(E)(f_{l}-f_{r})\dif \varepsilon
\end{equation}

Therefore the net current is 

\begin{equation}
I=I_{l}^{+}-I_{l}^{-}=I_{r}^{+}=-\frac{2e}{h}\int T(E)(f_{l}-f_{r})\dif \varepsilon
\end{equation}

In the limit $T\rightarrow 0$ (where $T$ here is the temperature), we have that the total conductance is given by 

\begin{equation}
G=G_{0}T(\varepsilon_{F})
\end{equation}

where $\varepsilon_{F}$ is the Fermi energy of the electrodes at equilibrium. If the scattering events do not cause interband scattering, i.e. the sub-bands can be seen as independent transport channels, the multi-band conductance is written as

\begin{equation}
G=G_{0}\sum_{i}\theta(\varepsilon_{F}-\varepsilon_{i})T_{i}(\varepsilon_{F})
\end{equation}

Therefore the calculation of the resistance is directly related, in linear response, to the calculation of the transmission coefficient. Some of the next sections will be indeed devoted to describe the mathematical and computational tools which allow to accomplish this task and the physical consequences of the transmission picture.   


\section{Four probe resistance: where is the voltage drop?}

In the previous section we only defined a distribution function for the electrodes and we provide a formula for the conductance in the case when both the current and the potential are measured at the electrodes. In this case we define the setup a \emph{two probe measurement}. We will now consider a slightly more complicated case, where we inject current through the electrodes but we measure the potential inside the leads. Such a setup is called a \emph{four probe measurement}. 

From fig.~\ref{fig:lan5} we can notice that inside the leads the states population is less trivial than inside the contacts, due to the fact that the leads are driven out of equilibrium by applied bias. The number of populated states is in general given by  

\begin{equation}
n=\int g(\varepsilon)(\varepsilon, \mu_{l})\dif \varepsilon
\end{equation}

In a lead, for example the left one, the density of states is populated according to distribution in the left and right contact. We will assume for simplicity an homogeneous system, with the same density of states in both the leads.Within this assumption we canwrite the number of states injected from the left electrode in the left lead

\begin{equation}
  n_l=\int g(\varepsilon)(\varepsilon, \mu_{l})\dif \varepsilon
\end{equation}

and the number of states injected from the right electrode in the right lead as

\begin{equation}
    n_l=\int g(\varepsilon)(\varepsilon, \mu_{l})\dif \varepsilon
\end{equation}

We also assume the time reversal symmetry is preserved, i.e. $T_{lr}=T_{rl}$. States up to the $\mu_{l}$ will be completely populated by the left and right contact as $T+r=1$.  However, in the energy interval $\mu_{r}, \mu_{l}$ only some states can propagate in the left and right lead. If we consider the left lead, the right electrode will provide all the $+k$ states, while the $-k$ states will be occupied by transmitted states from the right lead and reflected states from the left lead. Naming the left and right lead $ll, rr$, we can write

\begin{equation}
  n_{ll}= \frac{n_l}{2}(1+R)+\frac{n_r}{2}T = \frac{n_l}{2}(2-T)+\frac{n_r}{2}T
\label{eq:coh7}
\end{equation}

We can see that in the limit $T\rightarrow 0$, $n_{ll}=n_{l}$ and in the limit $T\rightarrow 1$, $n_{ll}=(n_l + n_r)/2$, which is quite intuitive. Eq.~\ref{eq:coh7} can be rewritten as

\begin{equation}
\int g(\varepsilon)f_{ll}(\varepsilon) \dif \varepsilon=\frac{2-T}{2}\int g(\varepsilon)f_{l}(\varepsilon, \mu_{l}) \dif \varepsilon + \frac{T}{2} \int g(\varepsilon) f_{r}(\varepsilon, \mu_{r}) \dif \varepsilon
\end{equation}

which is solved by the distribution

\begin{equation}
f_{ll}(\varepsilon)=\frac{2-T}{2}f_{l}(\varepsilon, \mu_{l}) + \frac{T}{2}f_{r}(\varepsilon, \mu_{r})
\end{equation}

$f_{ll}$ is in general a non-equilibrium distribution, but in linear response we expand it in the same way of a Fermi-Dirac and therefore I will sometimes improperly refer to a chemical potential defined in the lead region. If the applied bias is sufficiently small, then $(\mu_{l}-\mu_{r}) \rightarrow 0$. The first order expansion of Fermi distribution in energy is $f(\varepsilon, \mu) \approx \varepsilon - \mu$, therefore

\begin{equation}
f_{ll}(\varepsilon) = \frac{2-T}{2}(\varepsilon - \mu_{l}) + \frac{T}{2}(\varepsilon - \mu_{r})= \varepsilon - \mu_{ll}
\end{equation}

where 

\begin{equation}
\mu_{ll} = \mu_{l} + \frac{T}{2}(\mu_{r} - \mu_{l})
\label{eq:coh9}
\end{equation}

We can obtain a similar expression for the right lead

\begin{equation}
\mu_{rr} = \mu_{r} + \frac{T}{2}(\mu_{l}-\mu_{r})
\label{eq:coh11}
\end{equation}

The difference between the chemical potential in right and left lead is

\begin{equation}
\mu_{ll} - \mu_{rr} = (\mu_{l} - \mu_{r})(1-T)=(\mu_{l}-\mu_{r})R
\end{equation}

In a four probe measurement, the measured voltage is exactly $\mu_{ll} - \mu_{rr}$ instead of $\mu_{l}-\mu_{r}$. It follows that the four probe conductance (indicated with 4p opposite to 2p for a two probe measurement) is

\begin{equation}
G^{4p}=\frac{G^{2p}}{R}=G_{0}\frac{T}{R}
\end{equation}

Therefore if $R \rightarrow 0$ then $G^{4p}\rightarrow \infty$ as we could expect from the classical expression. This indicates that the ballistic resistance is indeed a \emph{contact resistance}, and it arises from the injection of states from a dissipative system to a low-dimensional system with a single electronic channel. 

Moreover, eq. ~\ref{eq:coh9} and ~\ref{eq:coh11} state then in a ballistic system (T=1), the chemical potential in the system is $\mu_{ll} = \mu_{rr} = \frac{\mu_{rr} + \mu_{ll}}{2}$. Therefore all the voltage drop occurs at the electrode interface. Note that up to now we the treatment is still semi-classical, meaning that $T$ and $R$ may be defined in terms of backscattering and forward scattering probabilities and quantum phase information never play a role. In fact, ballistic transport can be also described by the Boltzmann equation. The transmission is anyway calculated by QM methods, in the following we will see that if we preserve phase information and we calculate the transmission beyond first order perturbation theory, some novel physics arise. But first, we define more strictly the transmission coefficient and show that it can rigorously describe charge current through the derivation of probability density current. Note also that this demonstration is only valid in absence of magnetic field, the expression in presence of magnetic field looks slightly different.

\begin{ExerciseList}
\Exercise The ballistic contact resistance is not quantitatively relevant in a 2DEG. Consider a 2DEG with a lateral width of \SI{10}{\micro \meter} and a Fermi wavelength of \SI{20}{\nano\meter}. How much is the ballistic resistance in this case?
\Exercise Demonstrate that in a 4-probe measurement at zero temperature where several states are injected in the leads the conductance is given by (see \cite{Ferry2009} pag. 143)

\begin{equation}
  G=G_{0}\frac{\sum_{i=1}^N T_{i}2\sum_{i=1}^{N}v_{i}^{-1}}{\sum_{i=1}^N (1+R_{i}-T_{i})v_{i}^{-1}}
\end{equation}

where $v_{i}$ is the group velocity of a mode $i$, $N$ the total number of modes and $T_{i}, R_{i}$ the transmission and reflection coefficients for the given mode.

\end{ExerciseList}
