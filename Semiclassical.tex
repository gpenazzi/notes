\chapter{Semiclassical Transport}
\label{sec:semiclassical}

In this chapter I will give an overview of the fundamental concepts of semiclassical transport. We will review the solution of the Boltzmann equation in linear response and hence the definition of conductance and transport momentum relaxation time, and see how it can be calculated analytically within first order perturbation theory.

\section{Boltzmann Equation}

Long before the introduction of quantum mechanics, Boltzmann had the intuition that a many particle system can be described in a precise and yet mathematically advantageous way if we define a \emph{distribution function} $f(\rvec, \pvec, t)$ which describes the probability to find a particle in a given infinitesimal region of phase space, such that

\begin{equation}
dN=f(\rvec, \pvec, t)d^3 \rvec d^3 \pvec
\end{equation}  

is the number of particles within the volume $dr_{x}dr_{y}dr_{z}dp_x dp_y dp_z$ around the phase space coordinate $\rvec, \pvec$. The Boltzmann equation, in the most general form, is the kinetic equation for $f(\rvec, \pvec, t)$:

\begin{equation}
\frac{df}{dt}=\left( \frac{\partial f}{\partial t}\right) _{drift} +
\left( \frac{\partial f}{\partial t}\right)  _{diff}+
\left( \frac{\partial f}{\partial t}\right)  _{coll} 
\label{eq2:bol1}
\end{equation} 

where \emph{diff} stands for diffusion and \emph{coll} for collision. The student familiar with many body statistical mechanics will be familiar with the fact that the Boltzmann equation does not provide an exact solution to the many body problem, but rather arises from truncation of the full Liouville equation for a many body system. The study of complex many body phenomena is beyond the scope of this lectures, therefore we will assume that many body interactions can be mapped in the collision term.  The eq.~\ref{eq2:bol1} can be derived in two different ways: by carrier conservation arguments or by trajectory arguments (see ref. \cite{Lundstrom2009}), here we follow the latter. We know from classical mechanics that the distribution function of a free particle along a phase space trajectory which obeys Hamilton dynamics is conserved, i.e.

\begin{equation}
f\left( \rvec, \pvec, t\right) = f\left( \rvec + d\rvec, \pvec + d\pvec, t+dt \right) = f\left( \rvec + \frac{\pvec}{m} dt, \pvec + \Fvec dt, t+dt \right)
\label{eq2:bol2}
\end{equation}

The expression above can be simply written as $\frac{df}{dt}=0$. If we add collisions, eq.~\ref{eq2:bol2} is not valid any more: a particle can be instantly scattered from a trajectory in $(\rvec, \pvec)$ to the trajectory in $(\rvec, \pvec ')$. As a consequence, the total derivative is non zero and is commonly written as:

\begin{equation}
\frac{\partial f}{\partial t}=\left(\frac{\partial f}{\partial t} \right)_{coll} + G
\label{eq2:bol3}
\end{equation}

where $\left(\frac{\partial f}{\partial t} \right)_{coll}$ is called collision operator. The additional term $G$ takes into account the possibility of creating or annihilating electron-hole pairs, called generation/recombination factor.  In the following we will not consider light adsorption/emission, therefore we assume $G=0$. 

The total differential $df$ is:

\begin{equation}
df = \frac{\partial f}{\partial t}dt + \gradr f \cdot d\rvec + \gradp f \cdot d\pvec = 
\frac{\partial f}{\partial t}dt + \gradr f \cdot \frac{\pvec}{m}dt + \gradp f \cdot \Fvec dt
\label{eq2:bol4}
\end{equation}

Combining eq.~\ref{eq2:bol3} and eq.~\ref{eq2:bol4} we obtain:

\begin{equation}
\frac{\partial f}{\partial t} + \frac{\pvec}{m}\gradr f + \Fvec \gradp f = \collisionop
\end{equation}

In the case of charge transport we are clearly interested in electrodynamic forces $\Fvec=q\left( \Evec + \vvec \times \Bvec \right)$ where $\vvec$ is the group velocity $\vvec=\frac{\partial \varepsilon}{\partial \pvec}$. The term with $\gradr$ is called \emph{diffusion} term and goes to zero when the distribution is homogeneous in space; the term with $\Fvec$ is called \emph{drift} term and is driven by an external force. 

The \emph{semiclassical} nature of Boltzmann equation is usually related to two distinct facts: the first one is that the group velocity is related to the crystal momentum, so that we can map crystal properties in the mass tensor. Furthermore we have to pay some attention when we sum up (or integrate) on the available states at a given time and position and possibly include the correct degeneracies. If we write the distribution as a function of wavevector, the total number of states (normalized per unit volume) at a given time and position is

\begin{equation}
n(\rvec)=\frac{g_{c}}{(2\pi)^d}\int d^d\kvec f(\rvec, \kvec, t)
\end{equation}

where $g_{c}$ is the degeneracy factor for a given carrier type (spin+band) and $d$ the dimensionality of the system. If we want to keep consistency between the classical and semi-classical notation, we can always use momentum and apply the substitution $d^d\pvec \longrightarrow \frac{d^d \pvec}{(2\pi\hbar)^d}$.

The additional "bit" of quantum theory is hidden in the collision integral, we'll see in the following how to derive it from the Schr\"{o}dinger equation. Up to now, we will see how to define it in terms of scattering probabilities. 

\subsection{Collision integral}

In presence of collision, there is a finite probability that a particle with a given momentum $\pvec$ will be scattered to a different momentum state $\kvec '$. We describe this process with a \emph{scattering probability function} $S(\kvec, \kvec ', \rvec, t)$ \footnote{In the following I may drop the space and time variables for a cleaner notation, with the implicit assumption that all the quantities are calculated as a function of $\rvec, t$. I will also write $d\kvec = d^{d}\kvec$. We'll put the variables  back when needed. We assume also degeneracy $1$, and put the right factor back at the end.}. Taking into account Pauli's exclusion principle, such a transition is only allowed if the state $\kvec^{'}$ is unoccupied. Therefore the transition probability is given by 

\begin{equation}
P_{\kvec \rightarrow \kvec '} = S(\kvec, \kvec ' )[1-f(\kvec ')]
\end{equation}

Similarly, particles will be scattered from states $\kvec '$ to $\kvec$. We can define the collision integral as 

\begin{equation}
\collisionop = \collisionin + \collisionout
\end{equation}

with 

\begin{align}
\collisionout = -\frac{1}{(2\pi)^{d}}f(\kvec)\int S(\kvec, \kvec ')[1-f(\kvec ')] d\kvec '\\
\collisionin = \frac{1}{(2\pi)^{d}}[1-f(\kvec)]\int S(\kvec ', \kvec)f(\kvec ')d\kvec ' 
\end{align}

It follows that the collision operator can be expressed as:

\begin{equation}
\collisionop = -\frac{1}{(2\pi)^{d}} \int [ S(\kvec, \kvec ')f(\kvec)[1-f(\kvec ')] -   S(\kvec ', \kvec)f(\kvec ')[1-f(\kvec)]] d\kvec '
\label{eq2:bol5}
\end{equation}

\begin{ExerciseList}
\Exercise Detailed Balance: in an homogeneous system of Fermions at equilibrium with no applied electric field  $\left(\frac{df}{dt}=0  \right)$ the solution to the Boltzmann equation is given by the Fermi-Dirac distribution. Derive a relation between $S(\kvec, \kvec ')$ and $S(\kvec ', \kvec)$. Under which condition the two quantities are identical?
\label{ex:detailedbalance}
\end{ExerciseList}



\subsection{Linear response: Drude formula}

We solve now the Boltzmann equation in a bulk material under the assumption of small electric field (linear response) and assuming that $S(\kvec, \kvec ')=S(\kvec ', \kvec)$ (see Ex. ~\ref{ex:detailedbalance}). We will also assume that the system is homogeneous, i.e. there is no particle density gradient and $f$ depends only on $\pvec, t$, such that the diffusion term is zero.

As the applied field is small, we can expand the distribution $f$ around the equilibrium value:

\begin{equation}
f(\kvec, t) \approx f^{0}(\kvec, t) + f^{1}(\kvec, t)
\end{equation}

and assume $|f^{1}| \ll f^{0}$. We consider a stationary condition, such that distribution and scattering time do not depend on time any more. At equilibrium we must have $\left( \frac{\partial f^{0}}{\partial t} \right)_{coll}=0$, therefore the perturbation $f^{1}$ obeys the equation:

\begin{equation}
\collisionop = \left( \frac{\partial f^{1}}{\partial t} \right)_{coll} = - q\Evec \frac{\partial f^{0}}{\partial \pvec} = - q\Evec\vvec \frac{\partial f^{0}}{\partial \varepsilon({\kvec})}
\label{eq2:bol10}
\end{equation} 

where $\varepsilon({\kvec})$ is the energy of the state $\kvec$. Note that we used only $f^{0}$ on the rhs, justified by the assumption that the perturbation correction is small (first Born approximation). We invoke the detailed balance principle and assume $S(\kvec, \kvec ')=S(\kvec ', \kvec)$. The product terms in eq.~\ref{eq2:bol5} cancel out and we get the simple expression:

\begin{equation}
\left( \frac{\partial f^{1}}{\partial t} \right)_{coll} = -\frac{1}{(2\pi)^{d}} \int  S(\kvec, \kvec ')[f(\kvec)-f(\kvec ')]d\kvec '
\label{eq2:bol6}
\end{equation}

To make some progress we need some simplifying assumption about $f^{1}$. 
Given the total distribution, the current density can be calculated as 

\begin{equation}
\Jvec (\rvec, t)= q\frac{g_{c}}{(2\pi)^{d}} \int d\kvec \vvec f(\rvec, \kvec, t)
\end{equation}

It follows that the current is zero when the integrand is odd in $\kvec$, i.e. $f$ is even (symmetric) in $\kvec$. Therefore we expect the perturbation term $f^{1}$ to be odd in $\kvec$ along the direction of the applied force. To satisfy this requirement we assume the simple distribution

\begin{equation}
f^{1} \propto \left(\kvec \cdot \Evec / E \right)
\end{equation}

The equation~\ref{eq2:bol6} is more conveniently expressed as

\begin{equation}
\left( \frac{\partial f^{1}}{\partial t} \right)_{coll} = - \frac{1}{(2\pi)^{d}} f^{1}({\kvec}) \int d\kvec ' S(\kvec, \kvec ')\frac{ f^{1}(\kvec) - f^{1}(\kvec ')}{f^{1}(\kvec)}= -  \frac{f^{1}(\kvec)}{\tau}
\label{eq2:bol7}
\end{equation}

with

\begin{equation}
\frac{1}{\tau} = \frac{1}{(2\pi)^{d}} \int d\kvec ' S(\kvec, \kvec ')\left(1 - \frac{\kvec  '\cdot \ever}{\kvec \cdot \ever} \right)
\label{eq2:bol8}
\end{equation}

where $\ever$ is the unit vector in the direction of the applied electric field. Eq.~\ref{eq2:bol7} is usually referred as \emph{relaxation time ansatz} (see \cite{Piroth2008} p.388ff) and $\tau$ is the \emph{transport relaxation time} (or \emph{transport momentum relaxation time} or just \emph{relaxation time} for some authors).

We assume that the scattering probability $S(\kvec, \kvec ')$ only depends on the relative angle between $\kvec$ and $\kvec '$ and can be written as $S(\kvec, \theta)$.  Given the state $\kvec$, $\kvec '$ can be determined in radial coordinates by the polar angle $\theta$ and the azimuthal angle $\phi$. If we call the angle between $\kvec$ and $\ever$ $\eta$ and the angle between $\kvec '$ and $\ever$ $\eta ' $, by geometrical construction we can verify that

\begin{equation}
\frac{\kvec '\cdot \ever}{\kvec \cdot \ever}=\frac{\cos \eta^{'}}{\cos \eta}=\sin \theta \sin \phi \tan \eta + \cos \theta
\end{equation}

The integral of the first term respect to the azimuthal angle is zero  (as it depends on $\sin \phi$), therefore the transport relaxation time reduces to the expression:

\begin{equation}
\frac{1}{\tau_{m}} = \frac{1}{(2\pi)^d} \int d\kvec ' S(\kvec, \theta)(1-\cos \theta)
\label{eq2:bol9}
\end{equation}

Note that if we consider the relaxation time as the generic rate determining how often the particle with momentum $\kvec$ get scattered in any other state, the equation~\ref{eq2:bol9} would be simply $\frac{1}{\tau} = \frac{1}{(2\pi)^d}\int d\kvec ' S(\kvec, \theta)$. 

The term $(1-\cos \theta)$ arise because we are specifically interested in the \emph{relaxation time of the particle along the applied field direction}, therefore the term with $\theta$ is a weight on all the possible orientation of final states and takes into account possible anisotropy in the scattering potential. The most efficient scattering mechanism is backscattering ($\theta=\pi$), while forward scattering $\theta=0$ does not affect the momentum at all, therefore the corresponding weight is zero. The transport relaxation time is also a measure of the time-scale needed until the system can relax to equilibrium. In fact, if we start from the perturbed condition and switch off the electric field, the distribution evolves according to

\begin{equation}
f(\kvec, \rvec, t)=f^{0}(\kvec, \rvec, t) + f^{1}(\kvec, \rvec, t )e^{- \frac{t}{\tau_{m}}}
\end{equation} 

Now we simply plug the transport relaxation time ansatz in eq.~\ref{eq2:bol10} and we obtain the first order distribution perturbation

\begin{equation}
f^{1}(\kvec)=\tau q \Evec \vvec \frac{\partial f^{0}(\kvec)}{\partial \varepsilon(\kvec)}
\end{equation}

We recall that only $f^{1}$ contributes to a finite current density, therefore

\begin{equation}
\Jvec(\rvec)=\frac{1}{(2\pi)^d} q^{2} \int d\kvec \vvec (\Evec \cdot \vvec)\tau(\varepsilon) \frac{\partial f^{0}(\kvec)}{\partial \varepsilon(\kvec)}
\end{equation}

Note that $\tau$ in general is also a function of energy, being defined for a given momentum $\pvec$. The integral can be written in energy (the factor $\frac{1}{2\pi}$ and degeneracy are implicit in the density of states) as

\begin{equation}
\Jvec(\rvec)= q^{2} \int (d\varepsilon) g(\varepsilon) \langle v_{\Evec}^{2} \tau_{m}(\varepsilon) \rangle _{\varepsilon} \frac{\partial f^{0}(\kvec)}{\partial \varepsilon(\kvec)}
\label{eq2:bol11}
\end{equation}

where $v^{2}_{\Evec}$ is the magnitude of the velocity projected on the direction of the electric field, $\langle \rangle_{\varepsilon}$ is the average on the isosurface with energy $\varepsilon$ and $g(\varepsilon)$ the density of states. If we assume a free electron like particle with a spherical Fermi surface, this term is simply given by

\begin{equation}
D=\langle v_{\Evec}(\varepsilon)^{2} \tau(\varepsilon) \rangle _{\varepsilon} =
\frac{1}{d}v^{2}\tau_{m} = \frac{1}{d}v l_{m}
\end{equation}

where $l_{m}$ is the \emph{mean free path} and $d$ is the dimensionality of the system \footnote{The $d$ factor is simply due to the fact that $\langle v^{2}_{x} \rangle = \langle v^{2} \rangle /d$ upon integration on a d-dimensional spherical surface}.

The derivative of the equilibrium distribution which appears in eq.~\ref{eq2:bol11} is referred, in the case of Fermi-Dirac distribution, as \emph{thermal broadening function}. The reason for this definition is clear if we look at the plot of the function in Fig.~\ref{fig2:thbroad} \footnote{In figure it is actually shown $- \frac{\partial f^{0}(\kvec)}{\partial \varepsilon(\kvec)}$ as you can easily verify. We worked with positive charges, therefore the equilibrium distribution is $1-f$ with $f$ Fermi-Dirac, and the derivative is positive. The derivation is identical for electrons and holes as the minus sign is canceled by the charge sign in eq. ~\ref{eq2:bol7}}


\begin{figure}[h]
\centering
 \includegraphics[width=0.6\textwidth,keepaspectratio]{graph/thbroad}
 \caption{Thermal broadening function as a function of temperature.}
 \label{fig2:thbroad}
 \end{figure}

When $T\rightarrow 0$, the broadening function approaches a delta function $\delta(\varepsilon-\varepsilon ^{'})$. Therefore at low temperature (or alternatively, given that $g(\varepsilon)$ and $\tau(\varepsilon)$ are much smoother in energy than the thermal broadening function around the Fermi energy, we can approximate $\frac{\partial f^{0}(\kvec)}{\partial \varepsilon(\kvec)}$ with a delta function and write the conductivity as

\begin{equation}
\sigma = q^{2}g(\varepsilon_{F})D(\varepsilon_{F})
\end{equation}


\vspace{1cm}

\begin{ExerciseList}
\Exercise Demonstrate that in a free electron gas the conductivity can be written as $\sigma = n q\mu$ where $n$ is the charge density and $\mu$ is the mobility defined as $\mu =\frac{q\tau_{m} }{m}$.
\Exercise Demonstrate that at high temperature the relation between mobility and diffusion coefficient is $\mu=\frac{qD}{k_{B}T}$ (hint: at high temperature the Fermi-Dirac distribution can be approximated by a Boltzmann distribution).
\end{ExerciseList}

\vspace{1cm}

The conductivity is more often written as a function of \emph{mobility} $\mu$, where $\mu$ obey the general relation $\vvec _{d} = \mu \Evec$ and $\vvec _{d}$ is the \emph{drift velocity}. In general $\mu$ is a tensor, in the simple isotropic case we considered it can be reduced to a scalar.
After solving the two exercises, you derived two limits of the generalized \emph{Einstein relation}, relating mobility and diffusion coefficient as follows:

\begin{equation}
nq\mu=\frac{\partial n}{\partial \varepsilon _{F}}q^2 D
\end{equation}

In addition to drift forces due to a finite electric field, the current should also include a diffusion term due to the \emph{Fick's first law of diffusion}, which we neglected as we assumed an homogeneous charge density. The total steady state current is given by the \emph{drift diffusion equation}

\begin{equation}
\Jvec(\rvec) = q \mu \Evec (\rvec) - qD\gradr n(\rvec)
\end{equation}

It can be demonstrated that the drift-diffusion equation is rigorously derived within the Moment method by solving the first order moment of the Boltzmann equation respect to velocity (for example, ref.\cite{Jacoboni2010}). Within the same technique, the solution of the zero order moment is the well known continuity equation

\begin{equation}
- \gradr \Jvec(\rvec, t) = \frac{\partial n}{\partial t}n(\rvec, t)
\end{equation}

The drift diffusion equation is, in line of principle, valid at low electric field where the linear response treatment is physically justified. Nevertheless, despite its apparent simplicity, this equation is still widely used for device simulation. Non-parabolic bands and non-linear transport effects are usually semi-empirically included in a more complicated expression of mobility which can depend on several parameters (charge density, electric field, temperature). 

\subsection{Matthiessen's rule}

If there are more scattering sources within the system (for example, lattice vacancy and doping), the expression for the total mobility can be easily calculated only in the limit of uncorrelated scattering events. In fact, in this case the total scattering rate is simply given by 

\begin{equation}
S(\kvec, \kvec ') = \sum_{\alpha} S^{\alpha}(\kvec, \kvec ')
\label{eq2:bol12}
\end{equation}

where $\alpha$ is an index on all the scattering sources \footnote{Remember that the probability of the union for the events $A,B$ is, more in general, $P(A\cup B)=P(A)+P(B)-P(A\cap B)$. This means that if the scattering events are correlated, which will happen for large scattering (but not only), $A$ and $B$ cannot be disjoint and eq.~\ref{eq2:bol12} does not apply}. It follows that the total transport relaxation time can be calculated by the equation known as \emph{Matthiessen's rule}

\begin{equation}
\frac{1}{\tau_{m}} = \sum_{\alpha} \frac{1}{\tau_{m,\alpha}}
\label{eq2:bol13}
\end{equation}

The total transport relaxation time can then be used to calculate the mobility. From eq.~\ref{eq2:bol11} it is also evident that the average of $\tau(\varepsilon)$ with respect to energy is linear in $\tau$, therefore the Matthiessen's rule is not exact even for uncorrelated scattering events, nevertheless it is often routinely applied to give a numerical estimation of total mobility.


\section{Evaluation of scattering rates}

In order to evaluate the transport relaxation time, end hence the mobility, we have to evaluate the scattering rate $S(\kvec, \kvec^{'})$ and then apply eq.~\ref{eq2:bol9}. 

When the perturbation is small, the scattering rate can be calculated in general by means of time-dependent perturbation theory in the interaction picture. The rigorous derivation originally by P. Dirac  can be found in any quantum mechanics textbook (my preference goes for Modern Quantum Mechanics by J.J. Sakurai \cite{Sakurai1993}). Fermi golden rule is equivalent to the first order expansion term of perturbation theory. 

In the following I will provide a less rigorous but more intuitive derivation usually seen in contexts where time-dependent perturbation theory .


\subsection{Fermi Golden Rule}

Consider a system described at equilibrium by the Hamiltonian $\opham ^{0}$. We now apply a perturbation such that the final hamiltonian is given by 

\begin{equation}
\opham = \opham^{0} + \opham^{1}
\label{eq:fgr1}
\end{equation}

where $\opham ^{1}$ is a generic perturbation term which can describe a local potential or an interaction. We also assume that the solutions of the unperturbed system $\Ket{\varphi_{n}; t}=\Ket{\varphi}e^{-\jmath\varepsilon_{n}t/\hbar}$ form a complete orthogonal basis. We assume that the solution to the time dependent Schr\"{o}dinger equation $\opham \Ket{\Psi; t}=\jmath\hbar \frac{\partial}{\partial t}\Ket{\Psi; t}$ can then be expressed as a linear combination of the basis kets 

\begin{equation}
\Ket{\Psi; t}=\sum_{n}c_{n}(t)\Ket{\varphi_{n}; t}=\sum_{n}c_{n}(t)\Ket{\varphi_{n}}e^{-\jmath\varepsilon_{n}t/\hbar}
\label{eq:fgr2}
\end{equation}

It follows that if $\opham^{1}=0$ then the coefficients $c_{n}(t)$ are constant in time. 
 
 We consider now $\Ket{\Psi; t}$ to be initially in a defined state $\Ket{\varphi_{i}}$, i.e. $\Ket{\Psi; t=0}=\Ket{\varphi_{i}}$. After the interaction with the perturbing potential, we may have scattering from the state $\Ket{\varphi_{i}; t}$ to a state $\Ket{\varphi_{f}; t}$. Note that as the basis is orthogonal, we must also have $\Braket{\varphi_{f}; t=0 | \Psi; t=0} = 0$. The transition probability is then defined as 

\begin{equation}
P_{if}=\lim _{t\rightarrow \infty} \lvert \Braket{\varphi_{f}; t |\Psi; t } \rvert^{2}= \lim _{t\rightarrow \infty} \lvert c_{f}(t) \rvert ^{2}
\end{equation}

The scattering rate is defined as the average number of transitions per unit time, therefore the quantity we are interested in is defined as

\begin{equation}
W_{if}=\lim _{t\rightarrow \infty} \frac{\lvert c_{f}(t) \rvert ^{2}}{t}
\end{equation}

In  this apparently harmless passage we actually introduced the first approximation. In fact, we are considering that only direct transition $\varphi_{i} \rightarrow \varphi_{f}$ contribute to the transition rate, even when we consider the limit $t \rightarrow \infty$. Therefore we neglect any higher order process with transition mediated by intermediate states, of the kind $\varphi_{i} \rightarrow \varphi_{m} \rightarrow \varphi_{f}$. Clearly, if the scattering probability is small, higher order processes are negligible and this assumption is reasonable. 

We combine the expansion in eq.~\ref{eq:fgr2} with the time-dependent Schr\"{o}dinger equation

\begin{equation}
\left[\opham^{0}+\opham^{1} \right]\sum_{n}c_{n}(t)\Ket{\varphi_{n}; t} = \jmath\hbar\frac{\partial}{\partial t} \sum_{n}c_{n}(t)\Ket{\varphi_{n}; t}
\end{equation}

the unperturbed contribution cancels out. We multiply both sides for the Bra $\Bra{\varphi_{f}; t}$

\begin{equation}
\sum_{n}c_{n}(t) \Braket{\varphi_{f}; t | \opham^{1} | \varphi_{n}; t} = \jmath\hbar\frac{\partial}{\partial t} \sum_{n}c_{n}(t)\Braket{\varphi_{f}; t | \varphi_{n}; t}
\label{eq:fgr4}
\end{equation}

Here we need a further approximation. If the scattering potential is weak enough, we can assume that the coefficient $c_{i}(t)$ is much larger then any other, i.e. we assume that on the left hand side of the equation above $\Ket{\Psi; t}\approx \Ket{\Psi, t=0}=\Ket{\varphi_{i}; t=0}$. This assumption is called \emph{First Born Approximation}, and states the the final wavefunction is close to the initial state, which is a valid statement then the final states is weakly perturbed \footnote{A typical situation where the FBA fail is a scattering potential which introduces bond states, i.e. a resonant tunneling configuration.}. Under this assumption eq.~\ref{eq:fgr4} becomes

\begin{equation}
\jmath\hbar\frac{\partial }{\partial t} c_{f}(t) = \Braket{\varphi_{f}; t | \opham^{1}|\varphi_{i}; t}
\label{eq:fgr5}
\end{equation}

If $\opham^{1}$ is a local potential, i.e. $\Braket{x | U | x^{'}}=U(x, t)\delta(x-x^{'})$. We consider as a general form of perturbing potential time dependent oscillatory potential $H^{1}(t)=H^{1}e^{\pm \jmath \omega t}$ (we may invoke potential superposition if we have more spectral components). The r.h.s of eq.~\ref{eq:fgr5} is then

\begin{equation}
\Braket{\varphi_{f}; t | \opham^{1}|\varphi_{i}; t} = H^{1}_{fi}  e^{\jmath \left(\varepsilon_{f} - \varepsilon_{i} \pm \hbar \omega \right)t/\hbar} 
\end{equation}

Then eq.~\ref{eq:fgr5} can be easily integrated and the result is 

\begin{equation}
c_{f}(t) = \frac{1}{\jmath \hbar} H^{1}_{fi}\frac{e^{\jmath \left(\varepsilon_{f} - \varepsilon_{i} \pm \hbar \omega \right)t/\hbar}  -1}{\jmath \left(\varepsilon_{f} - \varepsilon_{i} \pm \hbar \omega \right)/\hbar}
\end{equation}

we define $\beta = \left(\varepsilon_{f} - \varepsilon_{i} \pm \hbar \omega \right)/\hbar$. Substituting and rearranging we obtain

\begin{equation}
c_{f}(t) = \frac{1}{\jmath \hbar}H^{1}_{fi}e^{\jmath \beta t}\frac{\sin \left(\beta t/2 \right)}{\beta t/2}t
\end{equation}

From the definition of the scattering rate

\begin{equation}
W_{if}= \lim_{t\rightarrow\infty}\frac{\lvert H^{1}_{fi} \rvert ^2}{\hbar^{2}}\left[ \frac{\sin \left(\beta t/2 \right)}{\beta t/2}\right]^2 t^2
\end{equation}

The $(\sin(x)/x)^{2}$ is a direct consequence of the time-energy uncertainty principle. If the time $t$ is sufficiently large, it can be approximated by a delta function and energy conservation is assured. However for a short time, comparable with $\hbar/( \varepsilon_{f}-\varepsilon_{i}\pm\hbar\omega)$, energy conservation may not be verified. In fig~\ref{fig:fgr1} I show a plot of this function. 

\begin{figure}[h]
\centering
 \includegraphics[width=0.6\textwidth,keepaspectratio]{graph/fgr1}
 \caption{Energy selection term of Fermi Golden Rule}
 \label{fig:fgr1}
 \end{figure}
 
Reminding that $\int_{-\infty}^{+\infty} \frac{\sin^2 x}{x^2}=\pi$, we can substitute for large enough $t$:

\begin{equation}
\lim_{t\rightarrow \infty}= \left[\frac{\sin \left(\beta t / 2 \right)}{\beta t / 2} \right]^2 = \frac{2\pi}{t}\delta(\beta) = \frac{2\pi}{t}\hbar\delta(\varepsilon_{f}-\varepsilon_{i}\pm \hbar \omega)
\end{equation}

We are finally ready to write the \emph{Fermi Golden Rule}:

\begin{equation}
W_{if}=\frac{2\pi}{\hbar} \lvert H^{1}_{fi} \rvert ^2 \delta(\varepsilon_{f}-\varepsilon_{i}\pm \hbar \omega)
\end{equation}

The term $\pm$ indicates emission ($+$) and adsorption ($-$) processes due an external time-dependent potential (e.g. phonon or photon adsorption/emission processes). If the external potential is not time dependent the energy conservation term will reduce to $\delta(\varepsilon_{f}-\varepsilon_{i})$.


\subsection{Delta potential}

Let's consider the simplest potential functional, given by a delta potential, and use the Bloch wavefunctions as a basis (\kvec can be used as a quantum number). We have 

\begin{equation}
H_{\kvec \kvec^{'}}=\frac{1}{V}\int_{V} e^{j\kvec^{'}\rvec}U(\rvec)e^{j\kvec \rvec}
\end{equation} 

with $U(\rvec)=A\delta(\rvec - \rvec_{0})$ and $V=1$ a normalization volume. In this limiting case we have simply $H_{\kvec^{'}\kvec}=A$ and the scattering rate is given by

\begin{equation}
S(\kvec,\kvec^{'})=\frac{2\pi}{\hbar}A^{2}\delta(\varepsilon_{\kvec^{'}}-\varepsilon_{\kvec})
\end{equation}

In this particular case $W(\kvec, \kvec^{'})$ does not depend on the angle between the initial and final state and we can demonstrate that the transport relaxation time is equivalent to the total scattering time, i.e. the factor $(1-\cos \theta )$ doesn't matter. In fact

\begin{equation}
\frac{1}{\tau_{m}} = \frac{1}{(2\pi)^d}\int d\kvec ' S(\kvec ', \kvec)(1-\cos \theta)
\end{equation}

if we assume transform to spherical coordinates $d\kvec ' = k'^2\sin\phi d\phi d\theta$ where $k'$ the term in $\cos\theta$ cancels out. It follows that if $S$ does not depend on the angle $\theta$ between $\kvec ', \kvec$, then $\frac{1}{\tau _{m}}$ = $\frac{1}{\tau}$ where $\tau$ is the scattering time:

\begin{equation}
\frac{1}{\tau} = \frac{1}{(2\pi)^d}\int d\kvec ' S(\kvec ', \kvec)
\end{equation}

i.e., the transport relaxation time and the scattering time are identical if the potential completely randomize the momentum. 

We can write the relaxation time as: 

\begin{align*}
\frac{1}{\tau _{m}}& = \frac{2\pi \lvert H^{1}_{fi} \rvert ^2}{\hbar(2\pi)^d} \int d\kvec ' \delta (\varepsilon(\kvec ') - \varepsilon(\kvec )) \nonumber \\ 
& = \frac{2\pi \lvert H^{1}_{fi} \rvert ^2}{\hbar} \int \delta(\varepsilon_{\kvec^{'}}-\varepsilon_{\kvec})g(\varepsilon)\dif \varepsilon \nonumber \\ 
 &= \frac{2\pi \lvert H^{1}_{fi} \rvert ^2}{\hbar} g(\varepsilon)
\end{align*}

The simple derivation above shows that for a potential which completely randomize momentum along all directions the transport momentum relaxation time and the total momentum relaxation time are the same quantity.

For the delta potential we can integrate in energy by applying $\dif (\pvec^{'}) \rightarrow \dif \varepsilon g(\varepsilon)$ and obtain

\begin{equation}
\frac{1}{\tau_{m}}=\frac{1}{\tau}=\frac{2\pi}{\hbar}A^{2}\int \delta(\varepsilon_{\kvec^{'}}-\varepsilon_{\kvec})g(\varepsilon)\dif \varepsilon = \frac{2\pi}{\hbar}A^{2} g(\varepsilon_{\kvec})  
\end{equation}

This simple but important result states that the transport relaxation time is lower when the density of states is higher, i.e. if a state has more final states available for scattering, then the average scattering time is lower.


\subsection{Neutral impurity in a nanowire}

Let's now consider a one dimensional system as a nanowire, and calculate the elastic scattering due to a neutral impurity. We will consider the impurity modelled by a constant potential defined within a sphere of radius $r_{0}$:

\begin{equation}
U(\rvec) = U_{0} r^{3} \delta(\rvec - \rvec_{0})
\end{equation}

This simple model will allow us to appreciate some differences between scattering phenomena in a bulk crystal and in a confined system. The relaxation time is given by the equation ~\ref{eq2:bol9}. In a 1D system the only two possible scattering phenomena, neglecting interband scattering between different localized modes, are forward ($\theta=0$) and backward ($theta=\pi$) scattering. Therefore the anisotropy correction is $(1-\cos\theta)=2$.

We write the scattering rates as derived from Fermi Golden Rule, considering the initial and final states $k$ and $k^{'}$ (we consider a wavefunction in the form $(1/\sqrt{L})\varphi(x,y)e^{jkz}$, where $\varphi(x,y)$ is an envelope function on the cross-section of the nanowire). The transport relaxation time is

\begin{equation}
\frac{1}{\tau_{m}^{i}}=\frac{2L}{\hbar}\int |U_{k'k}|^{2}\delta (\varepsilon(k')-\varepsilon(k))dk'
\label{eq:imp1}
\end{equation}


The interaction matrix element can be expanded as

\begin{equation}
U_{k'k}=\braket{k^{'} | U | k}  = \frac{1}{L}\int d^{3}\rvec e^{j(k-k^{'})z}\varphi^{*}(x,y)U_{0}r^{3}\varphi(x,y)
\end{equation}

We substitute $k'$ with $\varepsilon$ in the integral, remembering that $\frac{dk'}{d\varepsilon}=\frac{1}{\hbar v_{k'}}$ \footnote{or equivalently you use the expression for $\frac{1}{\tau}$ as a function of density of states we derived for the delta potential}.
We substitute this expression in eq.~\ref{eq:imp1} and obtain

\begin{equation}
\frac{1}{\tau_{m}^{i}}=\frac{2}{\hbar ^2 v_{k}}\int_{k'} \frac{1}{L} \lvert \left[ \int d^{3}\rvec e^{j(k-k^{'})z}\varphi^{*}(x,y)U_{0}r^{3}\delta(\rvec-\rvec_{0})\varphi(x,y) \right] \rvert^{2} \delta(k'-k)
\end{equation}

The delta function drop the dependence on z. A single scatterer in $\rvec_{0}=(x_{0}, y_{0}, z_{0})$ is therefore giving a contribution

\begin{equation}
\frac{1}{\tau_{m}^{i}}=\frac{2U_{0}^2 r^{6}}{\hbar^2 L |v_{k}|} \lvert \varphi(x_{0},y_{0})\rvert^{4}
\label{eq:imp2}
\end{equation} 

The exact form of the envelop function can be calculated in the case of cylindrical and squared nanowires. In order to keep the solution as simple as possible, we introduce the constant envelope approximation, i.e. $\varphi(x,y) \approx 1/\sqrt{S}$, where $S$ is the surface of the nanowire cross section. Now we have to consider that the relaxation time must be averaged on all the possible impurity position by means of Matthiessen's rule. If we call $\tau_{m}^{i}(\rvec_{0})$ the expression in eq.~\ref{eq:imp2} and we consider that the impurities are randomly placed, we may substitute the Matthiessen's sum with the integral

\begin{equation}
\frac{1}{\tau_{m}}=\sum_{i}\frac{1}{\tau_{m}^{i}(\rvec_{0})} \rightarrow 
\frac{n_{i}}{S}\int \dif x_{0} \dif y_{0} \dif z_{0}\frac{1}{\tau_{m}^{i}(\rvec_{0})} 
\end{equation}

where $n_{i}$ is the linear impurity density. We consider a state on the Fermi surface, such that $|v_{k}|=v_{F}$. Therefore the average momentum relaxation time is given by

\begin{equation}
\frac{1}{\tau_{m}}= \frac{2n_{i}U_{0}^2 r^6}{\hbar ^2 v_{F} S^{2}}
\end{equation}

The resulting low-field mobility is then

\begin{equation}
\mu = \frac{q \hbar ^2 v_{F} S^2}{2m n_{i} U_{0}^2 r^6}
\end{equation}

\begin{ExerciseList}
\Exercise evaluate the transport relaxation time for the neutral impurity, as a function of impurity concentration. Assume a Fermi velocity of $10^7 \text{cm/s}$, a cross section of $5  \text{nm} \times 5 \text{nm}$,  an impurity radius of $3 \AA$ and an impurity potential of $1 \text{eV}$
\end{ExerciseList}

