\chapter{Localization theory}


After having introduced the basic concepts of semi-classical and quantum transport we will now try to understand the transport properties of infinite systems. From the point of view of a linear-response semi-classical theory it doesn't make any difference if we considered a system with a very small or very large length. In all cases, the transport properties of the system can be separated between bulk transport properties (i.e. the conductance tensor) and geometrical information on the sample. The conductance can be written, in fact, as 

\begin{equation}
G = \sigma L^{d-2}
\end{equation}

where $\sigma$ is the conductance, $L$ a characteristic length and $d$ the dimensionality of the system. From a quantum-mechanical point of view, we showed that in a one-dimensional system diffusive transport is recovered when we phase interference is not relevant, i.e. when the length of the system is large compared to phase relaxation length. In fact, we can sum up 4-probe resistances and Ohm's law is recovered. On the other hand, when we include phase information, we have two possible cases: propagating states which form bands or evanescent non-propagating states. In the following section we will try to understand this second case and its physical consequences more in depth. 


The whole theoretical apparatus called \emph{Localization theory} is much wider and deeper than shown here. The student interested in a deeper overview may refer to this ref. \cite{Abrahams2010}. In this section I will focus on disorder-induced localization always in a limit of single-particle hamiltonian, we will therefore avoid phase transition due by many body effects, as these are usually treated in advanced quantum many body physics courses. 
The reason why I find useful to have a dedicated section about localization is that the transfer matrix approach offers a very easy access point to random matrix theory and allows to derive the fundamentals of Anderson localization. I find also the main ideas of this chapter to be useful in order to appreciate the fine differences between semi-classical and quantum transport regimes.  

Part of these chapter ($\beta$ function subsection) is based on a more comprehensive review by M\"{u}ller and Delande (arXiv:1005.0915v2).


\section{Asymptotic behaviour of disordered systems}

Consider a one-dimensional tight binding chain. As demonstrated in chapter 3, the transfer matrix treatment is consistent with Bloch theory and in such system propagating band states will arise. Impurities and other disorder sources which break the lattice translational symmetry introduce a finite reflection probability which we saw to be the quantum-mechanical origin for resistance. But what's the behavior of an infinite system where phase coherence is preserved? Clearly the diffusive limit cannot be invoked, as it only holds in the limit of no phase interference. A theorem help us to ensure that the question is well formulated and that such a limit exists: the \emph{Osedelec}.

The \emph{Osedelec} theorem states that given the product of $n$ random matrices

\begin{equation}
M_{n} = \prod_{i=1}^{n} M_{i}
\end{equation}

 where for product of random matrices we mean an infinite product of matrices $M_{i}$ chosen from a given set according to a given probability distribution,  the asymptotic limit

\begin{equation}
\lim _{n\rightarrow \infty} M_{n}^{\frac{1}{n}}
\end{equation}

always exists and is unique. Given the eigenspace $u_{1}, ...u_{m}$ also the limit 

\begin{equation}
\lim _{n\rightarrow \infty} \frac{1}{n} \log \Vert M_{n} u_{i} \Vert 
\end{equation} 

exists for any given $u_{i}$. Additionally, the \emph{Furstenberg theorem}, states that the limit above exists for any $u\neq 0$. This implies that in a disordered system we will always have real, hence evanescent states. These theorems are easily formulated for a real matrix space, while the generalization to different spaces can be tricky. We won't bother about this problem, because if we write the transfer matrix from the TB Hamiltonian we can always express it in as a real matrix, derive its asymptotic behavior and if needed transform in another representation.  

The Furstenberg theorem is equivalently saying that for any random matrix infinite product, the \emph{Ljapunov exponent} of $M_{n}$ relative to the vector $u$ exists

\begin{equation}
\gamma(u) = \lim _{n \rightarrow \infty} \frac{1}{n} \log \frac{\Vert M_{n}u\Vert}{\Vert u \Vert} 
\end{equation}

The \emph{Ljapunov exponent} is a quantity widely used in non-linear systems dynamics and express the growth rate of a linear transformation $M$ along a given direction $M$.

This set of theorems ensure that the asymptotic properties of a random matrix system do not change depending on the specific realization, therefore some meaningful quantities can be defined, similarly to what we do in a semi-classical limit, where we do not care about the specific position of scatterers to calculate the conductance. 

The physical meaning of the Ljapunov exponent is clearer if we consider a TB system 

\begin{equation}
\begin{bmatrix}
c_{n+1} \\ c_{n}  
\end{bmatrix}=
M_{n}
\begin{bmatrix}
c_{0} \\ c_{-1}  
\end{bmatrix}
\end{equation}

By the principle of \emph{dynamic filtering}, as the limit of $M_{n}$ is uniquely defined, then the state vector defined by $c_{n+1}, c_{n}$ must converge to an eigenvector of $M_{n}$. In fact, if we represent $[c_{i+1}, c_{i}]$ as a superposition of the eigenvectors of $M_{n}$ $u_{1}$ and $u_2$, any subsequent rotation $M_{i}$ on the starting vector will enhance the dominant component by an amount $\lambda_{1}$ where $\lambda_{1}$ is the largest eigenvalue. After a large amount of iterations, the non-dominant component will be negligible and the result will converge to the dominant eigenvector. 

If we want to calculate a different eigenvector, the we choose an initial $[c_{0}, c_{-1}]$ orthogonal to $u_{1}$, such that we will necessarily converge to the second dominant eigenvector $u_{2}$ (and so on in linear systems with more than 2 dimension).

It follows that 

\begin{equation}
\Vert \begin{bmatrix}
c_{n+1} \\ c_{n}  
\end{bmatrix}
\Vert \propto
\lambda^{n}
\end{equation}

where $\lambda = e^{\gamma}$ and $\lambda^{n}$ is the asymptotic eigenvalue of $M_{n}$. In general, if we write the boundary condition as a superposition of the asymptotic transfer matrix eigenvectors, the solution after $n$ step can be written as

\begin{equation}
\begin{bmatrix}
c_{n+1} \\ c_{n}  
\end{bmatrix}\propto
\begin{bmatrix}
\lambda^{n} & 0 \\
0 & 1/\lambda^{n}
\end{bmatrix}
\begin{bmatrix}
c_{0} \\ c_{-1}  
\end{bmatrix}
\end{equation}

and \emph{the inverse of the dominant eigenvalue is such that the wave function envelope and the transmission (hence the conductance) are exponentially decaying with the length of the system}. This statement is the kernel of localization theory, demonstrating that disordered one-dimensional systems are asymptotically insulators and states in one-dimensional disordered systems are always localized. 


\section{Numerical calculation of Ljapunov exponents: Anderson disorder model}

It is clearly of large interest to calculate the Ljapunov exponents for a given set of random transfer matrices, as all the physical properties of interest for the charge transport are therein contained. In the following we will consider a very popular model of disorder, the \emph{Anderson disorder model}. According to this model, given a TB chain every on-site energy contains a perturbation term chosen randomly from a given interval

\begin{equation}
\varepsilon = \varepsilon_{0} + \delta  \mbox{ with } \delta \in [-\Delta /2, \Delta /2]
\end{equation}

How can we calculate the Ljapunov exponents for this model? Naively, one could think to calculate directly $M_{n}$ by iteration and then calculate the eigenvalues. This procedure is highly numerically unstable. In fact, by the principle of dynamical filtering the columns of $M_{i}$ are calculated as a a linear combinations of $\lambda u_{1}, 1/\lambda u_{2}$, i.e. they are given by the sum of exponentially growing and exponentially decaying components. Summing up very large and very small numbers is pretty much the worst think you can do with a computer. The loss of numerical precision will quickly result in numerical instability, and this is probably the main problem of transfer matrix formalism. To prove it, you can find a quick script attempting a brute force solution. The (meaningless) result is shown in fig ~\ref{fig:loc1}

\begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/ljapunov_explicit}
 \caption{Attempt to calculate Ljapunov exponent by direct solution results in numerical noise.}
 \label{fig:loc1}
 \end{figure}
 
A converging procedure takes advantage of the principle of dynamical filtering and follows the following steps, starting from a set of random initial vector $u_{1}, ...u_{m}$ where $m$ is the dimensionality of the space (2 for single mode 1D chain)

\begin{enumerate}
\item Rotation of the vector $n$ times according to the transfer matrix, where $n$ is small enough to avoid numerical instability (usually $n \approx 10$ to $100$ depending on the amount of disorder)

\begin{equation}
 u_{1}^l, ...u_{m}^l = \mathcal{M}_{l+n}...\mathcal{M}_{l} u_{1}^{l-1},...u_{m}^{l-1}
\end{equation}

\item Cumulate temporary values for $d_{1} = 1/n \log \lambda^{n}$

\begin{equation}
d_{1} = d_{1} + \log \frac{\Vert u_{1}^{l}\Vert}{l}
\end{equation}

\item Orthogonalize $u_{2}$ with respect to $u_{1}$

\item As point 2, but for $d_{2}$

\item Repeat $2$ to $3$ for the whole subspace $u_{1}...u_{m}$

\item Normalize the whole subspace

\item $l=l+1$ till convergence
\end{enumerate}

Steps $3$ to $6$ can be performed all together by Grahm Schmidt orthogonalization. The result for a linear chain with disorder $[-t/2, t/2]$, where $t$ is the TB hopping element, is shown in \cref{fig:loc3, fig:loc5}

 \begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/ljapunov_iterative1}
 \caption{Iterative solution of Anderson 1D disorder model.}
 \label{fig:loc3}
 \end{figure}
 
  \begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/ljapunov_iterative2}
 \caption{Iterative solution of Anderson 1D disorder model (magnified).}
 \label{fig:loc5}
 \end{figure}
 
 Here below I report the code used for the calculations above (which you can find in the repository as well) 
 
 \small
 \begin{verbatim}
import numpy as np
import matplotlib.pyplot as plt

def rndm(w=0.1):
    """Return a random transfer matrix in the form
    |e  -1  |
    |1   0  |
    with e random from an interval [-w/2, w/2]"""

    e = np.random.random() * w - w/2.0
    mat = np.ndarray(shape=(2,2))
    mat[0,0] = e
    mat[0,1] = -1.0
    mat[1,0] = 1.0
    mat[1,1] = 0.0
    return mat

def iterativesubspace(w=1.0, ntot=500000, reort=10):
    """Calculate both Lyapunov exponents of the system by direct investigation
    of the full subspace"""
    n = int(ntot/reort)
    d1 = np.zeros(n)
    d2 = np.zeros(n)

    #Set the starting subspace guess
    sub = np.zeros((2,2))
    sub[0,0] = 1.0
    sub[1,1] = 1.0

    for i in range(n):
        tranmat = np.eye(2, dtype=float)
        for j in range(reort):
            tranmat = np.dot(tranmat,rndm(w))
        sub = np.dot(tranmat, sub)
        sub, unsub = GS(sub)
        d1[i] = d1[i-1] + np.log(np.linalg.norm(unsub[:,0]))/reort
        d2[i] = d2[i-1] + np.log(np.linalg.norm(unsub[:,1]))/reort

    lexp1 = d1/np.array(range(1, len(d1)+1))
    lexp2 = d2/np.array(range(1, len(d2)+1))
    runningaverage = lexp1.copy()
    for ind, val in enumerate(lexp1):
        runningaverage[ind] = np.sum(lexp1[:ind])/ind
    print('average exponent',np.mean(lexp1, dtype=np.float64))
    plt.plot(lexp1)
    plt.plot(lexp2)
    plt.plot(runningaverage)
    plt.show()
    
    return 

def proj(u, v):
    # notice: this algrithm assume denominator isn't zero
    return u * np.dot(v,u) / np.dot(u,u)  
 
def GS(V):
    U = np.copy(V)
    for i in xrange(1, V.shape[1]):
        for j in xrange(i):
            U[:,i] -= proj(U[:,j], V[:,i])
    # normalize column
    den=(U**2).sum(axis=0) **0.5
    E = U/den
    # Return normalized and not normalized columns
    return E, U
    
def main(w=1.0, ntot=1000000, reort=50):
    print('Calculating ljapunov exponent')
    direct()
    iterativesubspace(w=w, ntot=ntot, reort=reort)

if __name__ == '__main__':
    main()


\end{verbatim} 
 \normalsize
 
 
The Ljapunov exponent can be estimated analytically for a small perturbation disorder $W$ in a 1D linear chain with hopping matrix elements $t$ as

\begin{equation}
\gamma = \frac{W^2}{24(4t-\varepsilon^{2})}
\end{equation}

The same procedure can be applied to quasi-1D systems, it can be easily demonstrated that the transfer matrix looks the same but scalar values for $\varepsilon_{n}$ and $t=1$ are substituted by block matrices $H_{n}$, $I$  representing the Hamiltonian of a single cross-section and the coupling between different cross-sections. In this case we will still find $N$ positive and $N$ negative Ljapunov exponents, where $N$ is the dimensionality of the cross-section. The localization length is given by the slowest decaying component of the wavefunction, i.e. the longest localization length or, equivalently, the smallest positive Ljapunov exponent. 

\section{Localization length}

The wave function localization is directly related to the transmission probability and hence to the conductance.
In phase-coherent one-dimensional disordered systems the conductance is experimentally observed to decay exponentially with the sample length according to a simple equation

\begin{equation}
T(L) \propto e^{\frac{-L}{l_{loc}}}
\end{equation}

Where $l_{loc}$ is the localization length. The smallest eigenvalue of the corresponding transfer matrix will converge to the asymptotic value $\frac{1}{\lambda^{n}}$, whre $\lambda$ is the smallest eigenvalue, therefore the transmission will be proportional to $\lambda^{2n}=e\gamma n$ with $\gamma$ the smallest Ljapunov exponent. The localization length may then be written simply as 

\begin{equation}
l_{loc} = \frac{1}{2\gamma}
\end{equation}


You see that the Ljapunov exponent contains two important information: it tells how strictly localized in space is the wavefunction and it describes the asymptotic behaviour of the conductance. The localization length will depend on the details of disorder and electronic band structure such as the group velocity, and in one-dimensional system it is of the same order of the elastic mean free path. In \cref{fig:loc7, fig:loc9}
the solution of the linear chain for different strength disorder is shown. Intuitively, the larger the disorder, the largest the Ljapunov exponent and the shortest the localization length. Not also that the localization length is decreasing as a power of the disorder potential, consistently with the mean free path evaluation of a delta potential calculated in the Fermi golden rule approximation. 


 \begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/ljexp1}
 \caption{Iterative solution of Anderson 1D disorder model for different disorder strength.}
 \label{fig:loc7}
 \end{figure}
 
  \begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/ljexp2}
 \caption{Ljapunov exponent trend depending on disorder strength.}
 \label{fig:loc9}
 \end{figure}
 
We should keep in mind that these result are valid in the asymptotic limit. If the system is short enough, for small perturbation which are correctly described at the level of Fermi golden rule (therefore no bound or resonant states), the semi-classical diffusive limit should be recovered. When $T \propto e^{-L/l}$, the proper dimensionless conductance, given by the 4 probe conductance on the ballistic conductance $g=T/R$, is defined as

\begin{equation}
g(L) \propto \frac{1}{e^{L/l}-1}
\label{eq:loc11}
\end{equation} 

We didn't explicitely set $l=l_{loc}$ or $l=l_{m}$ because even though the two quantities are in the same order, they are formally distinct. Therefore here $l$ is a sort of average characteristic decay length. It can be however proofed that $2l=l_{m}$ and therefore $l_{loc}=2l=l_{m}/2$ by consideration on the diffusive regime similar to the one in chapter 3.

Note that this function recovers correctly the qualitative limit for the diffusive and localized behaviour

\begin{eqnarray}
g(L) \propto \frac{l}{L} \; \; \; L \ll l \\
g(L) \propto e^{\frac{-L}{l}} \; \; \; L \gg l 
\end{eqnarray}


\subsection{Thouless number}

A quantity analogous to the dimensionless conductance can be defined starting from the semi-classical conductance expression, offering an important physical interpretation of confinement systems. This quantity is the \emph{Thouless number}, defined as

\begin{equation}
g(L) = G(L)\frac{\hbar}{2e^2}=\sigma L^{d-2}\frac{\hbar}{2e^2}
\end{equation}

where $\sigma$ is the conductance, $L$ the characteristic system length, $d$ the dimensionality and $e$ the electron charge. Note that the Thouless number and the dimensionaless conductance are equivalent (except for a $\pi$ factor). If we consider a material with diffusion coefficient $D=L^{2}/\tau$, $\tau$ may be seen as a measure of uncertainty for the presence of a charge within the hypercube $L^{d}$. In other words, if a charge is allowed to diffuse with a characteristic time $\tau$, then we cannot define the energy of the associated state with arbitrary precision, but we will rather have

\begin{equation}
\delta \varepsilon \tau \approx \frac{\hbar}{2}
\end{equation} 

where $\delta \varepsilon$ is an intrinsic energy broadening of the diffuse state. 
Then we calculate the spacing between distinct energy levels in the hypercube. If we assume a constant density of states $n$ then 

\begin{equation}
\Delta \varepsilon = \frac{1}{L^{d} n}
\end{equation}

where $L^{d}$ is the volume of the hypercube. By applying the semi-classical expression of conductance which we previously calculated in linear response, we can easily verify that 

\begin{equation}
g = \frac{\delta \varepsilon}{\Delta \varepsilon}
\end{equation}

The Thouless number is the ration between the intrinsic broadening of a state due to finite lifetime and the spacing between different energy levels. It follows that an insulating system must be characterized by well distinct energy states, while an increased broadening of energy states is characteristic of a larger conductance. We will derive more precise quantitative expression for the broadening by Green's function techniques in the next chapter. 


\subsection{The $\beta$ function}

By deriving the properties of 1D disordered systems, we relied on the existence of asymptotic limits defined for Ljapunov exponents, or equivalently for the logarithm of the transfer matrix eigenvalues. Clearly if we consider an ensemble of disordered systems, the average conductance is an ill defined quantity because the conductance \emph{is not a self-averaged quantity}. The localization length, in this sense, is a scaling parameters which does not depend on the length and allows us to determine the properties of the systems as a function of additional parameters like the disorder strength.

To achieve a similar result, Abrahams first introduced a scaling parameter called 
universal scaling $\beta$ function (borrowing the idea from renormalization group theory). The ultimate goal of scaling theory is to find a solid scheme to determine physical properties of  macroscopic systems starting from microscopic information. 

The beta function is defined as

\begin{equation}
\beta(L) = L \od{\log g}{L} = \od{\ln g}{\ln( L/l)}
\end{equation}

Where $g$ is the channel intrinsic conductance (what we called four probe conductance previously), $L$ the system length and $l$ a scaling length parameter.  As the logarithm is a monotonous function, the $\beta$ function has the following properties

\begin{itemize}
\item If $\beta < 0$ then the conductance $g$ must decrease with system size, i.e. a system with a finite short length conductance will turn to an ideal insulator for an infinite system size. 
\item If $\beta > 0 $ then $g$ increases with system size, i.e. a system which exhibit finite conductance for small lengths, will show finite conductance for large lengths.   
\end{itemize}

We can calculate the $\beta$ function as a function of $g$ for the one-dimensional case by using the expression in eq.~\ref{eq:loc11}

\begin{equation}
\beta(g)= -(g+1)\ln \left(1+\frac{1}{g} \right) 
\end{equation}

Of course $\beta$ is negative, as we would expect because we already found that one-dimensional systems behave asymptotically as insulators for any finite amount of disorder. 
For a small amount of disorder, or short lenght, the resistance should go to zero, hence the conductance should diverge. We have 

\begin{equation}
\lim _{g \rightarrow \infty} \beta(g) = -g \left[\frac{1}{g}+\frac{1}{2g^2}+ \mathcal{O}\left( \frac{1}{g^3} \right)\right] = -1 -\frac{1}{2g} + \mathcal{O}\left(g^{-2} \right)
\end{equation}

The term $\frac{1}{2g}$ represent a 2nd order contribution called \emph{weak localization correction} and is due to self-interference between scattering paths. It basically contains the quantum-corrections to the semi-classical diffusive picture which we already encountered. 
In the opposite limit, when localization kicks in, we obtain

\begin{equation}
\lim _{x\rightarrow 0} \beta(g) \approx \ln g
\end{equation}

The resulting qualitative plot of the $\beta$ function in 1D is shown in the figure below

  \begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/loc1}
 \caption{$\beta$ function in a 1D system.}
 \label{fig:loc11}
 \end{figure}
 
The 1D behaviour was quite clear even before introducing the $\beta$ function, but its full power  is exploited for higher dimensionality. The procedure is similar: if we assume $g$ to be a smooth monotonic function of disorder, so is $\beta$. Therefore we can derive the asymptotic value in the classical and localized limit and define a qualitative behaviour. In the classical limit we have, as we derived from Boltzmann equation, $g \propto L^{d-2}$. Therefore we must have 

\begin{equation}
\beta (g) = d-2- \frac{c}{g} + \mathcal{O}(\frac{1}{g^2}) 
\label{eq:loc11b}
\end{equation}

where $c$ is an additional expansion term which we left to account for weak localization correction, consistently with the 1D case. In the limit of large disorder, we may still expect localization to occur on short scale in the direction transverse to transport direction. Therefore any change in system size $L \rightarrow aL$ where $L$ is already larger than the localization length will only add a finite number of nearly independent transport channel, as the wave function of adjacent channel do not overlap. We can therefore still expect a power decay behaviour and 

\begin{equation}
\beta \approx \ln g
\end{equation}

According to this asymptotic behaviour, we can draw the $\beta$ function in 2 and 3 dimensions as well, as shown in figure below.

  \begin{figure}[H]
\centering
 \includegraphics[width=0.7\textwidth,keepaspectratio]{graph/loc2.png}
 \caption{$\beta$ function in a 1D, 2D and 3D system.}
 \label{fig:loc11b}
 \end{figure}
 
 The consequences of this simple treatment, possible thanks to the introduction of the scaling function, are 
 terribly important. First of all, we see that also in two-dimensional system asymptotic localization should always occur, as $\beta$ can is asymptotically going to zero from below. However, the localization length in two dimensional system with small amount of disorder is so large to have no practical impact,  it can easily be larger than the sample itself. On the other hand in 3D system a specific value of $g$ where $\beta$ crosses the zero exists. This is called \emph{critical value}, as it's an instability point with respect to the flow of the $\beta$ function. If at given disorder $\ln g > \ln g_{c}$, then the system will stay metallic and will still have a finite conductance for any scaling $L \rightarrow aL$. If on the opposite $\ln g < \ln g_{c}$, then the carriers will be asymptotically localized and a large sample behaves as an insulator. Therefore the system undergoes a metal to insulato (MIT) phase transition at the critical point $g_{c}$. It can be demonstrated that the point $g_{c}$ corresponds to the Ioffe-Regel localization criterion:
 
  \begin{equation}
  kl \approx 1
  \end{equation}
 
When the wavelength of a state is larger then the localization length, then the state is strongly localized. It follows that in an amorphous 3D bulk material it can always exists a critical energy, such that all the states below that energy are localized while all the states above that energy are propagating (or delocalized) states. This energy is called \emph{mobility edge}, and is a very common concept used in electronic transport theory of amorphous materials. As a final remark, we overlooked the weak localization term which may play a crucial role in 2D systems. In fact in some rare cases the weak localization coefficient may be negative (\emph{weak anti-localization}) and a metal-insulator transition may be observed in two-dimensional materials as well.   

