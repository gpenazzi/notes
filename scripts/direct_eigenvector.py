import numpy as np
import grahmschmidt
import matplotlib.pyplot as plt
from scipy import linalg

def rndh(w=0.1, size=1):
    """Return a random nearest neighbour hamiltonian 
    with e random onsite from an interval [-w/2, w/2]"""
    mat = np.zeros(shape=(size,size), dtype=complex)
    for i in range(size):
        mat[i,i] =  w*(np.random.random() - 0.5)
    # Hopping elements
    t = -1.0
    for i in range(size-1):
        mat[i, i+1] = t
        mat[i+1, i] = t
    # Periodic boundary
    mat[0, size-1] = t
    mat[size-1, 0] = t
    return mat



def main(w=1.0, ntot=1000000, reort=50):
    print('Calculating ljapunov exponent')
    size=500
    ham = rndh(w=w,size=size)
    #Solve eigenvalue problem
    eigs, eigvecs = linalg.eigh(ham)
    index = 10
    density = np.multiply(np.conj(eigvecs[:,index]), eigvecs[:,index])
    plt.plot(density)
    plt.show()

if __name__ == '__main__':
    main()


