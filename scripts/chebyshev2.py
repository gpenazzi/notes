import numpy as np
import matplotlib.pyplot as plt


m_0 = 9.10e-31
h = 1.58e-16
e = 1.602e-19
sqrt_2m_h = np.sqrt(2*m_0)/(h*np.sqrt(e)) #In 1/m * sqrt(eV)
m_2hsq = m_0/(2.0*(h**2))


def main():
    R = 0.7
    #Parameter double and single barrier
    L = 3e-10
    alpha = 2.0
    E = np.linspace(0.2, 10.0, num=1000)
    #Parameter N barriers
    L = 1e-10
    alpha = 2.0
    E = np.linspace(0.2, 5.0, num=1000)

    N_array = [8, 50]
    for N in N_array:
        T = cheb(alpha, N, L, E)
        labelstring = 'N = '+str(N)
        plt.plot(E,T, label = labelstring)
        plt.legend()
    plt.ylabel('Transmission')
    plt.xlabel('Energy [eV]')
    plt.show()



def cheb(alpha, N, L, E):
    """Calculate the transmission in N identical barriers
    delta barriers with height alpha separated by a distance L
    for states with energy E (E is an array)"""

    k1 =  sqrt_2m_h * np.sqrt(E)
    k2 =  sqrt_2m_h * np.lib.scimath.sqrt(E - alpha)
    T_single = 1 / (1.0 + (((k1**2-k2**2)/(2*k1*k2))**2)*(np.sin(k2*L/2.0)**2))
    R = 1.0 - T_single
    T = 1.0 / (1 + (R/(1-R))*np.power((np.sin(N*k1*L))/(np.sin(k1*L)),2) )
    return T

if __name__ == "__main__":
    main()
