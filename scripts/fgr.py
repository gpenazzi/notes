import numpy
import matplotlib.pyplot as plt
from matplotlib import rc

#Plot of sin(at/2)/(at/2) useful for Fermi Golden Rule demonstration
rc('font', size=14)

#Number of coordinate steps
n = 500
#range
x_min = -10.0
x_max = 10.0
x = numpy.linspace(x_min, x_max, n)
t_array = numpy.array([1.0, 10.0])

for t in t_array:
    y = numpy.square(numpy.sin(x*t/2)/(x*t/2))
    plt.plot(x, y, label='t = '+str(t))
plt.legend()
plt.xlabel(r'$\beta$', fontsize=18)
plt.title(r'$\left[\sin(\beta t/2)/ \left(\beta t/2\right) \right]^{2}$',
        fontsize=18)
plt.show()



#plt.xlabel(r'Center coordinate [nm]')
#plt.ylabel(r'Energy [eV]')
#plt.title(r'$\omega_{0}=10.0\omega_{c}$')
