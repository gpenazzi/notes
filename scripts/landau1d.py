import numpy
import matplotlib.pyplot as plt
from matplotlib import rc

#SI constants
me = 9.110e-31
hbar = 1.055e-34
e = 1.602e-19

#Define the physical parameters
#IEffective mass (electron mass units, isotropic)
m=0.07
#Magnetic field (Tesla)
B=5.0

#Confinement potential (in unit of cyclotron frequency omegac)
omega0=10.0

#Center coordinate range (in nm)
x0_range = 50.0
#Number of coordinate steps
x0_n = 500
#Number of sub-bands
subbands = 5

#Calculation
x0 = 1e-9*numpy.linspace(-x0_range, x0_range, x0_n)
omegac = e*B/(me*m)
bigm = me*m*(1.0+numpy.power(1.0/omega0,2.0))

energy=numpy.zeros((subbands,x0.size))
for nn in range(subbands):
    energy[nn,:] = ((nn+0.5)*hbar*omegac +\
            (numpy.power(x0*e*B,2.0)/(2.0*bigm)))/e

for nn in range(subbands):
    plt.plot(x0*1e9, energy[nn,:])
plt.xlabel(r'Center coordinate [nm]')
plt.ylabel(r'Energy [eV]')
plt.title(r'$\omega_{0}=10.0\omega_{c}$')
plt.show()
