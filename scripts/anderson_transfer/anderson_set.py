import numpy as np
import ljapunov
import matplotlib.pyplot as plt


def main():
    weights = np.linspace(0.1, 1.0, num = 10)
    n = 1000000
    reort = 10
    onsite = 3.0
    print('weights',weights)
    averages = np.zeros(10)

    for ind, w in enumerate(weights):
        print('doing w = ',w)
        lexp, running_average, avg = ljapunov.iterativevector(w=w,
                ntot=n, reort=reort, onsite = onsite)
        averages[ind] = avg
        labelstring = 'w = ' + str(w)
        plt.plot(lexp, label = labelstring)
        #plt.plot(running_average)
        plt.legend()
    plt.show()
    print('averages', averages)
    plt.plot(weights, averages)
    plt.xlabel('w')
    plt.ylabel('Ljapunov Exponent')
    plt.show()
    return



if __name__ == '__main__':
    main()
