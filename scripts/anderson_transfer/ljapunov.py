import numpy as np
import grahmschmidt
import matplotlib.pyplot as plt

def rndm(w=0.1, onsite = 0.0):
    """Return a random transfer matrix in the form
    |e  -1  |
    |1   0  |
    with e random from an interval [-w/2, w/2]"""

    e = np.random.random() * w - w/2.0 + onsite
    mat = np.ndarray(shape=(2,2))
    mat[0,0] = e
    mat[0,1] = -1.0
    mat[1,0] = 1.0
    mat[1,1] = 0.0
    return mat


def iterativevector(w=1.0, ntot=2000000, reort=10, onsite = 0.0):
    """Calculate the Lyapunov exponent by direct investigation of dominant
    eigenvector"""

    #Calculate by direct investigation of Mu
    u = np.transpose(np.array([1.0, 0.0]))
    n = int(ntot/reort)
    d1 = np.zeros(n)
    for i in range(n):
        #Normalize
        u = u/np.linalg.norm(u)
        tranmat = np.eye(2, dtype=float)
        for j in range(reort):
            tranmat = np.dot(tranmat,rndm(w = w, onsite = onsite))
        u = np.dot(tranmat,u)
        d1[i] = d1[i-1] + np.log(np.linalg.norm(u))/reort
    lexp = d1/np.array(range(1, len(d1)+1))
    runningaverage = lexp.copy()
    for ind, val in enumerate(lexp):
        runningaverage[ind] = np.sum(lexp[:ind])/ind
    avg = np.mean(lexp, dtype=np.float64)
    print('average exponent',np.mean(lexp, dtype=np.float64))
    
    return (lexp, runningaverage, avg)


def iterativesubspace(w=1.0, ntot=500000, reort=10):
    """Calculate both Lyapunov exponents of the system by direct investigation
    of the full subspace"""
    n = int(ntot/reort)
    d1 = np.zeros(n)
    d2 = np.zeros(n)

    #Set the starting subspace guess
    sub = np.zeros((2,2))
    sub[0,0] = 1.0
    sub[1,1] = 1.0

    for i in range(n):
        tranmat = np.eye(2, dtype=float)
        for j in range(reort):
            tranmat = np.dot(tranmat,rndm(w))
        sub = np.dot(tranmat, sub)
        sub, unsub = grahmschmidt.GS(sub)
        d1[i] = d1[i-1] + np.log(np.linalg.norm(unsub[:,0]))/reort
        d2[i] = d2[i-1] + np.log(np.linalg.norm(unsub[:,1]))/reort

    lexp1 = d1/np.array(range(1, len(d1)+1))
    lexp2 = d2/np.array(range(1, len(d2)+1))
    runningaverage = lexp1.copy()
    for ind, val in enumerate(lexp1):
        runningaverage[ind] = np.sum(lexp1[:ind])/ind
    print('average exponent',np.mean(lexp1, dtype=np.float64))
    plt.plot(lexp1)
    plt.plot(lexp2)
    plt.plot(runningaverage)
    plt.show()
    
    return        


def direct(w=1.0, n=100):
    """Try to calculate the eigenvalues by directly calculation Mn. This is just
    meant to show the numerical instability associated with the brute force
    method"""
    tranmat = np.eye(2, dtype=float)
    eig1 = np.zeros(n)
    eig2 = np.zeros(n)
    for i in range(n):
        tranmat = np.dot(tranmat,rndm(w))
        eig1[i], eig2[i] = np.linalg.eig(np.dot(np.transpose(tranmat), tranmat))[0]
        print('eig', np.dot(np.transpose(tranmat), tranmat), eig1[i], eig2[i])
    plt.plot(eig1)
    plt.plot(eig2)
    plt.show()
    return

def main(w=1.0, ntot=1000000, reort=10):
    print('Calculating ljapunov exponent')
    direct()
    iterativesubspace()
    #iterativevector()

if __name__ == '__main__':
    main()


