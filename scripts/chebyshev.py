import numpy as np
import matplotlib.pyplot as plt


m_0 = 9.10e-31
h = 1.58e-16
e = 1.602e-19
sqrt_2m_h = np.sqrt(2*m_0)/(h*np.sqrt(e)) #In 1/m * sqrt(eV)
m_2hsq = m_0/(2.0*(h**2))


def main():
    R = 0.7
    L = 3e-10
    E = np.linspace(0.1, 10.0, num=1000)
    alpha = 1.0
    N_array = [500]
    for N in N_array:
        T = cheb(alpha, N, L, E)
        labelstring = 'N = '+str(N)
        plt.plot(E,T, label = labelstring)
        plt.legend()
    plt.ylabel('Transmission')
    plt.xlabel('Energy [eV]')
    plt.show()



def cheb(alpha, N, L, E):
    """Calculate the transmission in N identical barriers
    delta barriers with height alpha separated by a distance L
    for states with energy E (E is an array)"""

    T_single = 1.0 / (1.0 + m_2hsq * (alpha**2)/E)
    R = 1.0 - T_single
    k =  sqrt_2m_h * np.sqrt(E)
    T = 1.0 / (1 + (R/(1-R))*np.power((np.sin(N*k*L))/(np.sin(k*L)),2) )
    return T

if __name__ == "__main__":
    main()
